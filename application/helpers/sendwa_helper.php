<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// header('Content-Type: application/json');
//save datalog
if ( ! function_exists('kirimWa'))
{
    function kirimWa($nohp,$pesan)
    {
        $CI =& get_instance();
      //   $nano = '{
      //     "to":"'.$nohp.'",
      //     "account":"ALBIRUNIMART",
      //     "content":"'.trim($pesan).'"
      // }';
      $data_content = json_encode( 
        array( 'to' => $nohp,
              'account' => 'ALBIRUNIMART',
              'content' => trim($pesan)
        ), JSON_PRETTY_PRINT);
      // echo $nano;
      // exit;
        // $url = 'http://nusawapi.demodantest.in/Sendtext/getDataSendOrder/?nomor='.$nohp.'&pesan='. urlencode($pesan);
        // file_get_contents($url);
        // echo $url;
        // exit;
        $curl = curl_init();
        // $content = urlencode($pesan);
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://wamobi.teras.camp:3031/v1/send/text',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_VERBOSE => true,
            CURLOPT_POSTFIELDS =>$data_content,
            CURLOPT_HTTPHEADER => array(
              'x-secret-key: rian-d3v3L0p3r',
              'Content-Type: application/json'
            ),
          ));
          
          $response = curl_exec($curl);
          
          curl_close($curl);
        //  echo json_encode($response);
        //  exit;
        return true;
    }
}