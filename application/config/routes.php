<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

$route['404_override'] = 'home/notfound';

$route['translate_uri_dashes'] = FALSE;

$route['404'] = 'home/notfound';

$route['paymentgateway'] = 'VirtualAccount/index';
$route['paymentgateway/submit'] = 'VirtualAccount/submit';

$route['pginvoice'] = 'Invoice/index';
$route['pginvoice/submit'] = 'Invoice/submit';
$route['getcbxendit'] = 'Invoice/getEventCallback';

$route['gudang'] = 'gudang/index';
$route['subscribe-email'] = 'home/subscribe_email';
$route['unsubscribe-email'] = 'home/unsubscribe_email';
$route['dapur'] = 'home/login';
$route['snap'] = 'snap';
$route['transaction'] = 'transaction';
$route['notification'] = 'notification';
$route['c/(:any)'] = 'categories/index/$1';
$route['promo'] = 'promo/index';
$route['flashsale'] = 'flashsale/index';
$route['p/(:any)'] = 'products/detail_product/$1 ';
$route['p_detail/(:any)'] = 'products/detail_product_landing/$1 ';
$route['products'] = 'products/index';
$route['products/getgrosir'] = 'products/getgrosir';
$route['testimoni'] = 'testimoni/index';
$route['cart'] = 'cart/index';
$route['payment'] = 'payment/index';
$route['register'] = 'auth/register';
$route['login'] = 'auth/login';
$route['logout'] = 'home/logout';
$route['profile'] = 'profile';
$route['search'] = 'page/search';
$route['reset-password'] = 'auth/reset_password';
$route['reset'] = 'auth/reset';
$route['new-password'] = 'auth/new_password';
$route['profile/invoice/(:num)'] = 'profile/xendit_invoice/$1';
$route['profile/transaction/(:num)'] = 'profile/detail_order/$1';
$route['profile/edit-profile'] = 'profile/edit_profile';
$route['profile/change-password'] = 'profile/change_password';
$route['administrator'] = 'administrator/index';
$route['administrator/users/(:num)'] = 'administrator/detail_users/$1';
$route['administrator/order/(:num)'] = 'administrator/detail_order/$1';
$route['administrator/order/(:num)/process'] = 'administrator/process_order/$1';
$route['administrator/order/(:num)/sending'] = 'administrator/sending_order/$1';
$route['administrator/order/(:num)/set_diskon'] = 'administrator/set_diskon/$1';
$route['administrator/order/(:num)/ubah_alamat'] = 'administrator/ubah_alamat/$1';
$route['administrator/order/(:num)/delete'] = 'administrator/delete_order/$1';
$route['administrator/products/search'] = 'administrator/search_products';
$route['administrator/product/import'] = 'administrator/import_produk';
$route['administrator/product/hadiah'] = 'administrator/hadiah_product';
$route['administrator/product/add'] = 'administrator/add_product';
$route['administrator/product/add-landing/(:num)'] = 'administrator/add_landing/$1';
$route['administrator/product/add-img/(:num)'] = 'administrator/add_img_product/$1';
$route['administrator/product/grosir/(:num)'] = 'administrator/add_grosir_product/$1';
$route['administrator/product/(:num)/edit'] = 'administrator/edit_product/$1';
$route['administrator/email/send'] = 'administrator/send_mail';
$route['administrator/email/(:num)'] = 'administrator/detail_email/$1';
$route['administrator/page/add'] = 'administrator/add_page';
$route['administrator/page/(:num)/edit'] = 'administrator/edit_page/$1';
$route['administrator/setting/home'] = 'administrator/home_setting';
$route['administrator/setting/typewriter_note'] = 'administrator/typewriter_note';
$route['administrator/setting/slide_preview'] = 'administrator/slide_preview';
$route['administrator/setting/slide_preview/add'] = 'administrator/add_slide_preview_setting';
$route['administrator/setting/small_thumbnail_book'] = 'administrator/small_thumbnail_book';
$route['administrator/setting/small_thumbnail_book/(:num)'] = 'administrator/edit_small_thumbnail_book/$1';
$route['administrator/setting/banner'] = 'administrator/banner_setting';
$route['administrator/setting/banner/add'] = 'administrator/add_banner_setting';
$route['administrator/setting/description'] = 'administrator/description_setting';
$route['administrator/setting/rekening'] = 'administrator/rekening_setting';
$route['administrator/setting/rekening/add'] = 'administrator/add_rekening_setting';
$route['administrator/setting/rekening/(:num)'] = 'administrator/edit_rekening_setting/$1';
$route['administrator/setting/sosmed'] = 'administrator/sosmed_setting';
$route['administrator/setting/sosmed/add'] = 'administrator/add_sosmed_setting';
$route['administrator/setting/sosmed/(:num)'] = 'administrator/edit_sosmed_setting/$1';
$route['administrator/setting/address'] = 'administrator/address_setting';
$route['administrator/setting/delivery'] = 'administrator/delivery_setting';
$route['administrator/setting/delivery/add'] = 'administrator/add_delivery_setting';
$route['administrator/setting/delivery/(:num)'] = 'administrator/edit_delivery_setting/$1';
$route['administrator/setting/cod'] = 'administrator/cod_setting';
$route['administrator/setting/cod/add'] = 'administrator/add_cod_setting';
$route['administrator/setting/footer'] = 'administrator/footer_setting';
$route['(:any)'] = 'page/index/$1';
