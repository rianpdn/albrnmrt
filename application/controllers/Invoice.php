<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
use Xendit\Xendit;

class Invoice extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('invoice');
	}

	public function submit()
	{
		$extId = $this->input->post("external_id");
		$email = $this->input->post("payer_email");
		$description = $this->input->post("description");
		$amount = $this->input->post("amount");

		// https://github.com/xendit/xendit-php/blob/master/examples/InvoiceExample.php
		Xendit::setApiKey($this->config->item('xendit_key'));

		$params = [
			"external_id" => $extId,
			"payer_email" => $email,
			"description" => $description,
			"amount" => $amount,

			// redirect url if the payment is successful
			"success_redirect_url"=> base_url()."invoice/success",

			// redirect url if the payment is failed
			"failure_redirect_url"=> base_url()."invoice/failure",
		];

		$invoice = \Xendit\Invoice::create($params);

		// this will automatically redirect to invoice url
		header("Location: ".$invoice["invoice_url"]);		
	}

	public function success()
	{
		echo("success");
	}

	public function failure()
	{
		echo("failure");
	}
	public function getEventCallback()
	{

		// if ($_SERVER["REQUEST_METHOD"] === "POST") {
		// $data = file_get_contents("php://input");
		// } else {
		// print_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);
		// }
		$dt = json_decode(file_get_contents('php://input'),true); 

		$external_id = $dt['external_id'];
		$status = $dt['status'];
		$id = $dt['id'];

		$getInvoice = $this->db->where('external_id',$external_id)
                                        ->get('xendit_invoice');
		if ($getInvoice->num_rows() != 0) {
			$data = $getInvoice->row_array();
			// echo json_encode($data['status']);
			// exit;
			if (@$data['status'] == 'PENDING') {
				$d = [
					'status' => $status
				];
				$update = $this->db->where('xendit_invoice_id',$id)
							->update('xendit_invoice',$d);
				if ($update) {
					$res=[
						'msg' => 'Data Invoice Xendit Berhasil diperbaharui'
					];
					
					$this->db->set('status', 1);
					$this->db->where('invoice_code', $external_id);
					$this->db->update('invoice');

				}else{
					$res=[
						'msg' => 'Data Invoice Xendit Gagal diperbaharui'
					];

				}
				echo json_encode($res);
			}
		}else{
			$res=[
				'msg' => 'Data Invoice Xendit tidak ada'
			];
			echo json_encode($res);
		}

	}
}
