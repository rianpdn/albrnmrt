<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Xendit\Xendit;
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('cookie');
        $this->load->library('form_validation');
        $this->load->model('Categories_model');
        $this->load->model('Products_model');
        $this->load->model('Settings_model');
        $this->load->model('Promo_model');
        $this->load->model('Testi_model');
        $this->load->model('Order_model');
        $this->load->model('Flashsale_model');
        $this->load->model('Voucher_model');
        $this->load->model('Payment_model');
        $this->load->model('Reseller_model');

        if(!$this->session->userdata('admin')){
            $cookie = get_cookie('djehbicd');
            if($cookie == NULL){
                redirect(base_url());
            }else{
                $getCookie = $this->db->get_where('admin', ['cookie' => $cookie])->row_array();
                if($getCookie){
                    $this->session->set_userdata('admin', true);
                }else{
                    redirect(base_url());
                }
            }
        }
    }
    public function upload_resi(){
       
        $this->form_validation->set_rules('help', 'help', 'required');

        if($this->form_validation->run() == false){
            $data['title'] = 'Bulk Upload Resi - Dapur Panel';
            $data['proof'] = $this->db->get_where('payment_proof', ['status' => 0]);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/upload_resi', $data);
            $this->load->view('templates/footer_admin');
        }else{
                $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
                $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload
                $config['upload_path'] = './uploads/';                                // Buat folder dengan nama "fileExcel" di root folder
                $config['file_name'] = $fileName;
                $config['allowed_types'] = 'xls|xlsx|csv';
                $config['max_size'] = 10000;
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('file'))
                    $this->upload->display_errors();
                $media = $this->upload->data('file');
                $inputFileName = './uploads/' . $fileName;
                try {
                    $inputFileType = IOFactory::identify($inputFileName);
                    $objReader = IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    $res = returnResultCustomTransfer(false,"Failed Import Log","canceled");
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                    $getData = $this->Order_model->getDataInvoiceId($rowData[0][1]);
                                  
                    $kodeOrder = $getData->row_array();
                    if($getData->num_rows()>0){
                        $data = $kodeOrder['invoice_code'];

                            if($rowData[0][1] == $data){

                                $this->db->set('status', 3);
                                $this->db->where('invoice_code', $data);
                                $this->db->update('invoice');
                                $this->db->set('resi',  $rowData[0][0]);
                                $this->db->where('invoice_code', $data);
                                $this->db->update('invoice');
                
                            } 
                    }

                    
                    delete_files($inputFileName);                                 
                }
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Excel Resi berhasil diproses',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/orders_by_status/3');
        }
    }

    public function importBulkResi()
    {
        
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload
        $config['upload_path'] = './uploads/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();
        $media = $this->upload->data('file');
        $inputFileName = './uploads/' . $fileName;
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());

            $res = returnResultCustomTransfer(false,"Failed Import Log","canceled");
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $getTotalBayar = $this->Order_model->getDataInvoice($rowData[0][1]);
                            // ->result()
            if($getTotalBayar->num_rows()>0){
                $data = $getTotalBayar['invoice_code'];

                    if($rowData[0][1] == $data){

                        $this->db->set('status', 3);
                        $this->db->where('invoice_code', $data);
                        $this->db->update('invoice');
                        $this->db->set('resi',  $rowData[0][0]);
                        $this->db->where('invoice_code', $data);
                        $this->db->update('invoice');
        
                    } 
            }

            delete_files($inputFileName);                                 
        }
        $res = returnResultCustomTransfer(true,"Success","Default");
        echo json_encode($res);
    //    redirect('peserta/manage');
 
    }
    public function index(){
        $data['title'] = 'Dashboard - Dapur Panel';
        $data['bruto'] = $this->db->query("select sum(total_realprice) as bruto from invoice where date(date_input) = '".date('Y-m-d')."' and status != 0")->row_array();
        $data['uang_masuk'] = $this->db->query("select sum(total_price) as uang_masuk from invoice where status != 0 and date(date_input) = '".date('Y-m-d')."'")->row_array();
        $data['saldo_xendit'] = $this->getSaldoXendit()['balance'];
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/index');
        $this->load->view('templates/footer_admin');
    }
    public function getSaldoXendit()
    {

        Xendit::setApiKey($this->config->item('xendit_key'));

        $getBalance = \Xendit\Balance::getBalance('CASH');
        // var_dump($getBalance);
        return $getBalance;
    }
    public function filterUangDash()
    {
       $start = $this->input->post('start');
       $end = $this->input->post('end');
       $data['bruto'] = $this->db->query("select sum(total_realprice) as bruto from invoice where date(date_input) BETWEEN '".$start."' and '".$end."' and status != 0")->row()->bruto;
        $data['uang_masuk'] = $this->db->query("select sum(total_all) as uang_masuk from invoice where date(date_input) BETWEEN '".$start."' and '".$end."' and status != 0")->row()->uang_masuk;
        $res = array(
            'success' => true,
            'data' => $data
        );
        echo json_encode($res);
    }
    public function add_order_manual(){
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
        $this->form_validation->set_rules('reseller', 'reseller', 'required');
        if($this->form_validation->run() == false){
            $data['title'] = 'Buat Pesanan Manual - Dapur Panel';
            $data['product'] = $this->Products_model->getProductsAll()->result_array();
            $data['provinces'] = $this->Payment_model->getProvinces();
            $data['reseller'] = $this->Reseller_model->getPembeli()->result_array();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_order_manual',$data);
            $this->load->view('templates/footer_admin');

        }else{
            $data = array();
            $upload = $this->Reseller_model->prosesOrder();
            if($upload['result'] == 'success'){
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Order berhasil dibuat',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/orders_by_status/0');
            }
        }
    }
    public function add_order_reseller(){
        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
        $this->form_validation->set_rules('reseller', 'reseller', 'required');
        if($this->form_validation->run() == false){
            $data['title'] = 'Reseller / Penulis - Dapur Panel';
            $data['product'] = $this->Products_model->getProductsAll()->result_array();
            $data['provinces'] = $this->Payment_model->getProvinces();
            $data['reseller'] = $this->Reseller_model->getReseller()->result_array();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_order_reseller',$data);
            $this->load->view('templates/footer_admin');

        }else{
            $data = array();
            $upload = $this->Reseller_model->prosesOrder();
            if($upload['result'] == 'success'){
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Order berhasil dibuat',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/orders_by_status/0');
            }
        }
    }
    public function textToSlug($text='') {
        $text = trim($text);
        if (empty($text)) return '';
        $text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
        $text = strtolower(trim($text));
        $text = str_replace(' ', '-', $text);
        $text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
        return $text;
    }
    public function add_user_pembeli()
    {
        $token = sha1(rand());
        $name = $this->input->post('nama',true);
        $email = $this->input->post('email',true);
        $telp = $this->input->post('telp',true);
        $usertype = $this->input->post('tipe',true);
        $password = 'user';
        $username = $this->textToSlug($name);

       $data = [
        'name' => $name,
        'username' => $username,
        'email' => $email,
        'password' => password_hash($password, PASSWORD_DEFAULT),
        'date_register' => date('Y-m-d H:i:s'),
        'token' => $token,
        'photo_profile' => 'default.png',
        'no_telp' => $telp,
        'user_type' => $usertype,
        'is_activate' => 1
        ];
        // echo json_encode($data);
        // exit;
        $insert = $this->db->insert('user', $data);

        if($insert){
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'User berhasil dibuat',
                icon: 'success'
                });
                </script>");
                redirect(base_url() . 'administrator/add_order_manual');
        }


    }
    public function add_user_reseller()
    {
        $token = sha1(rand());
        $name = $this->input->post('nama',true);
        $email = $this->input->post('email',true);
        $telp = $this->input->post('telp',true);
        $usertype = $this->input->post('tipe',true);
        $password = 'user';
        $username = $this->textToSlug($name);

       $data = [
        'name' => $name,
        'username' => $username,
        'email' => $email,
        'password' => password_hash($password, PASSWORD_DEFAULT),
        'date_register' => date('Y-m-d H:i:s'),
        'token' => $token,
        'photo_profile' => 'default.png',
        'no_telp' => $telp,
        'user_type' => $usertype,
        'is_activate' => 1
        ];
        // echo json_encode($data);
        // exit;
        $insert = $this->db->insert('user', $data);

        if($insert){
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'User berhasil dibuat',
                icon: 'success'
                });
                </script>");
                redirect(base_url() . 'administrator/add_order_reseller');
        }


    }
    public function getProductbyId($id)
    {
       $data = $this->Products_model->getProductById($id);
       $res = array(
           'success' => true,
           'data' => $data
       );
       echo json_encode($res);
    }
    public function users(){
        $data['title'] = 'Pengguna - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/users/';
        $config['total_rows'] = $this->User_model->getUsers("","")->num_rows();
        $config['per_page'] = 1000;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['users'] = $this->User_model->getUsers($config['per_page'], $from);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/users', $data);
        $this->load->view('templates/footer_admin');
    }
    // orders
    public function detail_users(){
        $data['title'] = 'Pesanan User - Dapur Panel';
        $id = $this->uri->segment(3);
        $data['orders'] = $this->User_model->getOrderByIdUser($id);
        $data['user'] = $this->User_model->getProfileById($id);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/detail_users', $data);
        $this->load->view('templates/footer_admin');
    }
    public function active_user($id){
        $this->db->set('is_activate', 1);
        $this->db->where('id', $id);
        $this->db->update('user');
        redirect(base_url() . 'administrator/users');
    }

    public function nonactive_user($id){
        $this->db->set('is_activate', 0);
        $this->db->where('id', $id);
        $this->db->update('user');
        redirect(base_url() . 'administrator/users');
    }
    public function add_landing($id){
       
        $this->form_validation->set_rules('help', 'help', 'required');
        if($this->form_validation->run() == false){
            $data['title'] = 'Upload Landing - Dapur Panel';
            $data['product'] = $this->Products_model->getProductById($id);
            $data['landing'] = $this->db->get_where('landing_product', ['id_product' => $id]);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_landing', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Products_model->uploadLanding();
            if($upload['result'] == 'success'){
                $this->Products_model->insertLanding($upload, $id);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Landing berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/product/add-landing/'.$id);
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah Landing, pastikan gambar landing berukuran maksimal 10mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/product/add-landing/'.$id);
            }
        }
    }
    public function verif_proof_excel(){
       
        $this->form_validation->set_rules('help', 'help', 'required');

        if($this->form_validation->run() == false){
            $data['title'] = 'Verifikasi Bukti Pembayaran Excel - Dapur Panel';
            $data['proof'] = $this->db->get_where('payment_proof', ['status' => 0]);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/verif_proof_excel', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Products_model->uploadImg();
            if($upload['result'] == 'success'){
                $this->Products_model->insertImg($upload, $id);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Excel berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/product/proof');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah gambar, pastikan gambar berukuran maksimal 10mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/product/proof');
            }
        }
    }
    public function proof(){
        $data['title'] = 'Bukti Pembayaran - Dapur Panel';
        $data['proof'] = $this->db->get_where('payment_proof', ['status' => 0]);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/proof', $data);
        $this->load->view('templates/footer_admin');
    }
    public function proof_history(){
        $data['title'] = 'Riwayat Bukti Pembayaran - Dapur Panel';
        $data['proof'] = $this->db->get_where('payment_proof');
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/proof_history', $data);
        $this->load->view('templates/footer_admin');
    }
    public function confirm_proof($id){
        $type = $_GET['type'];
        $this->db->set('status', 1);
        $this->db->where('invoice', $id);
        $this->db->update('payment_proof');
        $this->db->set('status', 1);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
        $get = $this->db->get_where('payment_proof', ['invoice' => $id])->row_array();
        unlink("./assets/images/confirmation/".$get['file']);
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Bukti pembayaran terverifikasi',
            icon: 'success'
            });
        </script>");

        $querygetInv = $this->db->where('invoice_code',$id)
                            ->get('invoice')->row_array();
        $telp = $querygetInv['telp'];
        $nama = $querygetInv['name'];
        // $textWa = "Dear".$nama.""."\xA"."
        // Segera kami konfirmasi atas info pembayaran pada jam kerja aktif."."\xA"."
        // Terimakasih."."\xA"."
        // ";   
        $textWa = "Dear".$nama.""."\xA"."
        Pembayaran anda sudah kami terima, segera kami akan memproses pesanan anda."."\xA"."
        Terimakasih."."\xA"."
        ";  
        kirimWa($telp,$textWa);
        if($type == "order"){
            redirect(base_url() . 'administrator/order/' . $id);
        }else{
            redirect(base_url() . 'administrator/proof/');
        }
    }

    // orders
    public function orders(){
        $data['title'] = 'Pesanan - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/orders/';
        $config['total_rows'] = $this->Order_model->getOrders("","")->num_rows();
        $config['per_page'] = 10;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['orders'] = $this->Order_model->getOrders($config['per_page'], $from);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/orders', $data);
        $this->load->view('templates/footer_admin');
    }
    
    public function orders_by_status(){
        $status = $this->uri->segment(3);
        $icon_wa = 0;
        $icon_print = 0;
        if($status == 0){
            $label = 'Belum Dibayar';
           
            $icon_wa = 1;
        }else if($status == 1){ 
            $label = 'Belum Diproses';
            $icon_print = 1;
        }else if($status == 2){ 
            $label = 'Sedang diproses';

        }else if($status == 3){ 
            $label = 'Sedang dikirim';

        }else if($status == 4){ 
            $label = 'Selesai';

        }
        $data['icon_wa'] = $icon_wa ;
        $data['icon_print'] = $icon_print ;
        
      
        $data['title'] = 'Pesanan '.$label.' - Dapur Panel';
        $data['label'] = $label;
        $data['orders'] = $this->Order_model->getOrdersByStatus($status);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/orders_by_status', $data);
        $this->load->view('templates/footer_admin');
    }
    public function sendNotifWa()
    {
        $nomor = $this->input->get('nomor');
        $text = 'Assalammualaikum kak, maaf kak untuk ordernya sudah ada bukti transfernya kak?';
        $data = array('nomor' => $nomor,
                        'text' =>  $text,
                        'createdAt' => date('Y-m-d H:i:s'));

        $insert = $this->db->insert('send_notif_wa',$data);
        kirimWa($nomor,$text);
        if ($insert) {
            // redirect(base_url() . 'administrator/orders_by_status/0');
            $res = array(
                "sukses" => true
            );
            echo json_encode($res);
        }
    }
    public function arsip_pesanan()
    {
        $start = $this->input->post('tgl_awal');
        $end = $this->input->post('tgl_akhir');
       
        // echo  var_dump($_POST);
        // exit;
        $qArsipPesanan = $this->db->query("update invoice set status = '5' where date(date_input) BETWEEN '".$start."' and '".$end."' ");
        if ($qArsipPesanan) {
            $res = array(
                'status' => 'oke',
                'log' => $this->db->last_query()
            );
            echo json_encode($res);
        }
        

    }
    public function export_data_orders()
    {
        $start = $this->input->post('tgl_awal');
        $end = $this->input->post('tgl_akhir');
        $status = $this->input->post('status');

        $awal = date("d-F-Y", strtotime($start));
        $akhir = date("d-F-Y", strtotime($end));
        if ($status == '-') {
            $qgetPesanan = $this->db->query("select *,case when status=0 then 'Belum Dibayar' when status=1 then 'Belum Diproses' when status=2 then 'Sedang diproses' when status=3 then 'Sedang dikirim' when status=4 then 'Selesai' else 'Arsip' end as status from invoice where date(date_input) BETWEEN '".$start."' and '".$end."'  ")->result();
        }else{
            $qgetPesanan = $this->db->query("select *,case when status=0 then 'Belum Dibayar' when status=1 then 'Belum Diproses' when status=2 then 'Sedang diproses' when status=3 then 'Sedang dikirim' when status=4 then 'Selesai' else 'Arsip' end as status from invoice where status='".$status."' and date(date_input) BETWEEN '".$start."' and '".$end."'  ")->result();
        }
       

        // $q = $this->Order_model->getOrders(1000, 0)->result();


        $object = new Spreadsheet();
        $object->setActiveSheetIndex(0);

        $table_columns = array("No","Produk","Jml Produk","Kode Order", "Nama","Email","Handphone","Provinsi","Kota","Kecamatan","Kelurahan/Desa","Kodepos","Alamat Lengkap","Kurir","Tipe Kurir","Status Pesanan","Biaya Ongkir","Total Belanja","Total Keseluruhan","Tanggal Order");

        $column = 0;

        foreach($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $no = 0;
        foreach($qgetPesanan as $v)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no++);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $this->getProductTransaction($v->invoice_code));
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$this->getQtyProductTransaction($v->invoice_code));
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row,$v->invoice_code);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row,$v->name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row,$v->email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row,$v->telp);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row,$this->getProvByfile($v->province));
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,$this->getCityByfile($v->regency));
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row,$v->district);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,$v->village);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$v->zipcode);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row,$v->address);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,$v->courier);
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,$v->courier_service);
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row,$v->status);
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row,$v->ongkir);
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row,$v->total_price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row,$v->total_all);
            $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, date('j F Y H:i:s', strtotime($v->date_input)));
            $excel_row++;
        }

        $object_writer = new Xlsx($object);
        // $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data-Pesanan-'.$awal.' sampai '.$akhir.'.xlsx"');
        header('Cache-Control: max-age=0');
        $object_writer->save('php://output');
    }
    public function getProvByfile($id)
    {
        $url = base_url().'prov.json';
        $data = file_get_contents($url);
        $json = json_decode($data, true);
        $result = $json['rajaongkir']['results'];

        foreach ($result as $key => $res) {
           if ($res['province_id'] == $id) {
            $prov = $res['province'];
           }
        }
        return $prov;
    }
    public function getCityByfile($id)
    {
        $url = base_url().'kota.json';
        $data = file_get_contents($url);
        $json = json_decode($data, true);
        $result = $json['rajaongkir']['results'];

        foreach ($result as $key => $res) {
           if ($res['city_id'] == $id) {
            $city = $res['type'].' '.$res['city_name'];
           }
        }
        return $city;
    }
    public function getProductTransaction($kode_order)
    {
       $this->db->where('id_invoice',$kode_order);
       $q = $this->db->get('transaction')->result();
       $prod_array = []; 
       foreach ($q as $s) {
            $prod_array[] = $s->product_name;
       }
       $produk = implode(" ; ",$prod_array);
       return $produk;
    }
    public function getQtyProductTransaction($kode_order)
    {
       $this->db->where('id_invoice',$kode_order);
       $q = $this->db->get('transaction')->result();
       $qty_prod_array = []; 
       foreach ($q as $s) {
            $qty_prod_array[] = $s->qty;
       }
       $qty = implode(" ; ",$qty_prod_array);
       return $qty;
    }
    public function detail_order($id){
        if($this->Order_model->getDataInvoice($id)){
            $data['title'] = 'Detail Pesanan - Dapur Panel';
            $data['orders'] = $this->Order_model->getOrderByInvoice($id);
            $data['invoice'] = $this->Order_model->getDataInvoice($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/detail_order', $data);
            $this->load->view('templates/footer_admin');
        }else{
            redirect(base_url() . 'administrator/orders');
        }
    }

    public function print_detail_order($id){
        if($this->Order_model->getDataInvoice($id)){
            $data['title'] = 'Detail Pesanan - Dapur Panel';
            $data['orders'] = $this->Order_model->getOrderByInvoice($id);
            $data['invoice'] = $this->Order_model->getDataInvoice($id);
            $this->load->view('administrator/order_invoice', $data);
        }else{
            redirect(base_url() . 'administrator/orders');
        }
    }

    public function process_order($id){
        $buyer = $this->db->get_where('invoice', ['invoice_code' => $id])->row_array();
        $this->db->set('status', 2);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
        $transaction = $this->db->get_where('transaction', ['id_invoice' => $id]);
        foreach($transaction->result_array() as $t){
            $this->db->set('transaction', 'transaction+'.$t['qty'].'', FALSE);
            $this->db->set('stock', 'stock-'.$t['qty'].'', FALSE);
            $this->db->where('slug', $t['slug']);
            $this->db->update('products');
        }
        $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['useragent'] = $this->config->item('app_name');
        $config['smtp_crypto'] = $this->config->item('smtp_crypto');
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = $this->config->item('host_mail');
        $config['smtp_port'] = $this->config->item('port_mail');
        $config['smtp_timeout'] = '5';
        $config['smtp_user'] = $this->config->item('mail_account');
        $config['smtp_pass'] = $this->config->item('pass_mail');
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
        $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
        $this->email->to($buyer['email']);
        $this->email->subject('Pesanan Sedang Diproses '.$id);
        $this->email->message(
            '<p><strong>Halo '.$buyer['name'].'</strong><br>
            Pembayaranmu sudah kami terima dan pesanan sedang kami proses. Jika ada pertanyaan silakan hubungi Whatsapp '.$this->config->item('whatsapp').' atau <a href="https://wa.me/'.$this->config->item('whatsappv2').'">klik disini</a>.</p>
            <p>Terima kasih atas kepercayaannya kepada kami.<br/>
            Terima Kasih</p>
            ');
        $this->email->send();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Status berhasil diubah menjadi Sedang Diproses',
            icon: 'success'
            });
        </script>");
        redirect(base_url() . 'administrator/order/'.$id);
    }

    public function finish_order_cod($id){
        $this->db->set('status', 4);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
        $transaction = $this->db->get_where('transaction', ['id_invoice' => $id]);
        foreach($transaction->result_array() as $t){
            $this->db->set('transaction', 'transaction+1', FALSE);
            $this->db->set('stock', 'stock-1', FALSE);
            $this->db->where('slug', $t['slug']);
            $this->db->update('products');
        }
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Pesanan selesai',
            icon: 'success'
            });
        </script>");
        redirect(base_url() . 'administrator/order/'.$id);
    }
    public function ubah_alamat($id)
    {
        $alamat = $this->input->post('alamat', true); 

        $this->db->set('address', $alamat);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
// echo $this->db->last_query();
// exit;
        $this->session->set_flashdata('upload', "<script>
        swal({
        text: 'Berhasil Ubah Alamat Pembeli',
        icon: 'success'
        });
        </script>");
        redirect(base_url() . 'administrator/order/'.$id);
    }
    public function set_diskon($id)
    {
        $diskon = $this->input->post('diskon', true); 



        $this->session->set_flashdata('upload', "<script>
        swal({
        text: 'Berhasil Set Diskon Reseller',
        icon: 'success'
        });
        </script>");
        redirect(base_url() . 'administrator/order/'.$id);
    }
    public function sending_order($id){
        $resi = $this->input->post('resi', true);
        if($resi == NULL){
            redirect(base_url() . 'administrator/orders');
        }
        $buyer = $this->db->get_where('invoice', ['invoice_code' => $id])->row_array();
        $this->db->set('status', 3);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
        $this->db->set('resi', $resi);
        $this->db->where('invoice_code', $id);
        $this->db->update('invoice');
        $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['useragent'] = $this->config->item('app_name');
        $config['smtp_crypto'] = $this->config->item('smtp_crypto');
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = $this->config->item('host_mail');
        $config['smtp_port'] = $this->config->item('port_mail');
        $config['smtp_timeout'] = '5';
        $config['smtp_user'] = $this->config->item('mail_account');
        $config['smtp_pass'] = $this->config->item('pass_mail');
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
        $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
        $this->email->to($buyer['email']);
        $this->email->subject('Pemesanan Telah Dikirim '.$id);
        $this->email->message(
            '<p><strong>Halo '.$buyer['name'].'</strong><br>
            Pesananmu telah kami kirim. <br/> Nomor Resi: <strong>'.$resi.'</strong> <br/> Jika ada pertanyaan silakan bisa menghubungi kami melalui Whatsapp'.$this->config->item('whatsapp').' atau <a href="https://wa.me/'.$this->config->item('whatsappv2').'">klik disini</a>.</p>
            ');
        $this->email->send();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Nomor Resi telah dikirim kepada pembeli melalui email',
            icon: 'success'
            });
        </script>");
        redirect(base_url() . 'administrator/order/'.$id);
    }

    public function delete_order($id){
        $buyer = $this->db->get_where('invoice', ['invoice_code' => $id])->row_array();
        $this->db->where('invoice_code', $id);
        $this->db->delete('invoice');
        $this->db->where('id_invoice', $id);
        $this->db->delete('transaction');
        $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['useragent'] = $this->config->item('app_name');
        $config['smtp_crypto'] = $this->config->item('smtp_crypto');
        $config['protocol'] = 'smtp';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = $this->config->item('host_mail');
        $config['smtp_port'] = $this->config->item('port_mail');
        $config['smtp_timeout'] = '5';
        $config['smtp_user'] = $this->config->item('mail_account');
        $config['smtp_pass'] = $this->config->item('pass_mail');
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
        $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
        $this->email->to($buyer['email']);
        $this->email->subject('Pemesanan Anda Ditolak '.$id);
        $this->email->message(
            '<p><strong>Halo '.$buyer['name'].'</strong><br>
            Pesanan Anda kami tolak. <br/> Silakan hubungi kami melalui Whatsapp'.$this->config->item('whatsapp').' atau <a href="https://wa.me/'.$this->config->item('whatsappv2').'">klik disini</a>.</p>
            ');
        $this->email->send();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Pesanan ditolak dan telah dihapus',
            icon: 'success'
            });
        </script>");
        redirect(base_url() . 'administrator/orders');
    }

    // email
    public function email(){
        $data['title'] = 'Kirim Email - Dapur Panel';
        $data['email'] = '';
        $this->db->select("*, email_send.id AS sendId");
        $this->db->from("email_send");
        $this->db->join("subscriber", "email_send.mail_to=subscriber.id");
        $this->db->order_by('email_send.id', 'desc');
        $data['email'] = $this->db->get();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/email', $data);
        $this->load->view('templates/footer_admin');
    }

    public function detail_email($id){
        $data['title'] = 'Detail Email - Dapur Panel';
        $data['email'] = '';
        $this->db->select("*, email_send.id AS sendId");
        $this->db->from("email_send");
        $this->db->join("subscriber", "email_send.mail_to=subscriber.id");
        $this->db->where('email_send.id', $id);
        $data['email'] = $this->db->get()->row_array();
        if(!$data['email']){
            redirect(base_url() . 'administrator/email');
        }
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/detail_email', $data);
        $this->load->view('templates/footer_admin');
    }

    public function send_mail(){
        $this->form_validation->set_rules('sendMailTo', 'sendMailTo', 'required', ['required' => 'Ke wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Kirim Email - Dapur Panel';
            $data['email'] = $this->Settings_model->getEmailAccount();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/send_mail', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $to = $this->input->post('sendMailTo');
            $subjet = $this->input->post('subject');
            $message = $this->input->post('description');
            $data = [
                'mail_to' => $to,
                'subject' => $subjet,
                'message' => $message
            ];
            $this->db->insert('email_send', $data);

            if($to == 0){
                $data = $this->db->get('subscriber');
                foreach($data->result_array() as $d){
                    $this->load->library('email');
                    $config['charset'] = 'utf-8';
                    $config['useragent'] = $this->config->item('app_name');
                    $config['smtp_crypto'] = $this->config->item('smtp_crypto');
                    $config['protocol'] = 'smtp';
                    $config['mailtype'] = 'html';
                    $config['smtp_host'] = $this->config->item('host_mail');
                    $config['smtp_port'] = $this->config->item('port_mail');
                    $config['smtp_timeout'] = '5';
                    $config['smtp_user'] = $this->config->item('mail_account');
                    $config['smtp_pass'] = $this->config->item('pass_mail');
                    $config['crlf'] = "\r\n";
                    $config['newline'] = "\r\n";
                    $config['wordwrap'] = TRUE;

                    $message .= '<br/><br/><a href="'.base_url().'unsubscribe-email?email='.$d['email'].'&code='.$d['code'].'">Berhenti berlangganan</a>';

                    $this->email->initialize($config);
                    $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
                    $this->email->to($d['email']);
                    $this->email->subject($subjet);
                    $this->email->message(nl2br($message));
                    $this->email->send();
                }
            }else{
                $this->load->library('email');
                $config['charset'] = 'utf-8';
                $config['useragent'] = $this->config->item('app_name');
                $config['smtp_crypto'] = $this->config->item('smtp_crypto');
                $config['protocol'] = 'smtp';
                $config['mailtype'] = 'html';
                $config['smtp_host'] = $this->config->item('host_mail');
                $config['smtp_port'] = $this->config->item('port_mail');
                $config['smtp_timeout'] = '5';
                $config['smtp_user'] = $this->config->item('mail_account');
                $config['smtp_pass'] = $this->config->item('pass_mail');
                $config['crlf'] = "\r\n";
                $config['newline'] = "\r\n";
                $config['wordwrap'] = TRUE;

                $dataEmail = $this->db->get_where('subscriber', ['id' => $to])->row_array();
                $message .= '<br/><br/><a href="'.base_url().'unsubscribe-email?email='.$dataEmail['email'].'&code='.$dataEmail['code'].'">Berhenti berlangganan</a>';
                $this->email->initialize($config);
                $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
                $this->email->to($dataEmail['email']);
                $this->email->subject($subjet);
                $this->email->message(nl2br($message));
                $this->email->send();
            }
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Email berhasil dikirim',
                icon: 'success'
                });
            </script>");
            redirect(base_url() . 'administrator/email');
        }
    }

    public function detele_email($id){
        $this->db->where('id', $id);
        $this->db->delete('email_send');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Email berhasil dihapus',
            icon: 'success'
            });
        </script>");
        redirect(base_url() . 'administrator/email');
    }
    //sub categories

    // categories
    public function sub_categories(){
        $this->form_validation->set_rules('name', 'Name', 'required', ['required' => 'Nama kategori wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Sub Kategori - Dapur Panel';
            $data['getCategories'] = $this->Categories_model->getCategories();
            $data['getSubCategories'] = $this->Categories_model->getSubCategories();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/sub_categories', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $insert = $this->Categories_model->insertSubCategory();

            if($insert){
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Sub Kategori berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/sub_categories');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah kategori. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/sub_categories');
            }
        }
    }
    public function categories(){
        $this->form_validation->set_rules('name', 'Name', 'required', ['required' => 'Nama kategori wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Kategori - Dapur Panel';
            $data['getCategories'] = $this->Categories_model->getCategories();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/categories', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Categories_model->uploadIcon();

            if($upload['result'] == 'success'){
                $this->Categories_model->insertCategory($upload);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Kategori berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/categories');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah kategori, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/categories');
            }
        }
    }

    public function category($id){
        $this->form_validation->set_rules('name', 'Name', 'required', ['required' => 'Nama kategori wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Edit Kategori - Dapur Panel';
            $data['category'] = $this->Categories_model->getCategoryById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_category', $data);
            $this->load->view('templates/footer_admin');
        }else{
            if($_FILES['icon']['name'] != ""){
                $data = array();
                $upload = $this->Categories_model->uploadIcon();
                if($upload['result'] == 'success'){
                    $this->Categories_model->updateCategory($upload['file']['file_name'], $id);
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Kategori berhasil diubah',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/categories');
                }else{
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal mengubah kategori, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
                  </div>");
                    redirect(base_url() . 'administrator/category/' . $id);
                }
            }else{
                $oldIcon = $this->input->post('oldIcon');
                $this->Categories_model->updateCategory($oldIcon, $id);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Kategori berhasil diubah',
                    icon: 'success'
                    });
                    </script>");
                redirect(base_url() . 'administrator/categories');
            }
        }
    }

    public function deleteCategory($id){
        $this->db->where('id', $id);
        $this->db->delete('categories');
        $this->db->where('category', $id);
        $this->db->delete('products');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Kategori berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/categories');
    }
    public function lap_jual(){
        $data['title'] = 'Laporan - Laporan Penjualan';
        $config['base_url'] = base_url() . 'administrator/products/';

        $start = $this->input->post('tgl_awal');
        $end = $this->input->post('tgl_akhir');
        $this->form_validation->set_rules('help', 'help', 'required');
        if($this->form_validation->run() == false){
            $data['getLapJual'] = $this->Order_model->getLapJualan();
        }else{
            $data['getLapJual'] = $this->Order_model->getLapJualanPeriodic($start,$end);
        //     echo $this->db->last_query();
        // exit;
        }
        
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/lap_jual', $data);
        $this->load->view('templates/footer_admin');
    }
    public function download_lap_jual(){
        $start = $this->input->get('tgl_awal');
        $end = $this->input->get('tgl_akhir');
        $getData = $this->Order_model->getAllLapJualanPeriodic($start,$end);
        
        $object = new Spreadsheet();
        $object->setActiveSheetIndex(0);
       
        $table_columns = array("No","Nama Pembeli","Daerah","Kode Buku","Judul Buku","Harga Buku","Jumlah Buku","Bruto","Diskon","RP Diskon","Netto","Ongkir","Total","Nominal Transfer","Selisih","Tanggal Uang masuk","Insentif","Keterangan");

        $column = 0;

        foreach($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $no = 0;
        foreach($getData->result() as $v)
        {
            $rpdiskon = $v->bruto * ($v->diskon/100);
            $netto = $v->bruto - $rpdiskon;
            $total = $netto + $v->ongkir;
            $insentif = $v->bruto*(1/100);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no++);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row,$v->namapembeli);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$v->daerah);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row,$v->kodebuku);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row,$v->judulbuku);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row,$v->hargabuku);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row,$v->jumlahbuku);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row,$v->bruto);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,$v->diskon.'%');
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row,$rpdiskon);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,$netto);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$v->ongkir);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row,$total);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,'-');
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,'-');
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row,'-');
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row,$insentif);
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row,'-');
            $excel_row++;
        }

        $object_writer = new Xlsx($object);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Lap-Penjualan-Albiruni-'.date('jFy').'.xlsx"');
        header('Cache-Control: max-age=0');
        $object_writer->save('php://output');
    }
    // products
    public function products(){
        $data['title'] = 'Produk - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/products/';
        $config['total_rows'] = $this->Products_model->getProducts("","")->num_rows();
        $config['per_page'] = 10;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['getProducts'] = $this->Products_model->getProducts($config['per_page'], $from);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/products', $data);
        $this->load->view('templates/footer_admin');
    }

    public function search_products(){
        $key = $_GET['q'];
        $data['title'] = 'Produk - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/products/';
        $config['total_rows'] = $this->Products_model->getSearchProducts($key,"","")->num_rows();
        $config['per_page'] = 10;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['getProducts'] = $this->Products_model->getSearchProducts($key,$config['per_page'], $from);
        $data['search'] = $key;
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/products', $data);
        $this->load->view('templates/footer_admin');
    }
    public function import_produk(Type $var = null)
    {
            $this->form_validation->set_rules('help', 'help', 'required');
    
            if($this->form_validation->run() == false){
                $data['title'] = 'Bulk Upload Produk - Dapur Panel';
                $this->load->view('templates/header_admin', $data);
                $this->load->view('administrator/upload_produk', $data);
                $this->load->view('templates/footer_admin');
            }else{
                $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
                $tipe = $this->input->post('tipe');

                if ($tipe == 0) {
                    $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload
                    $config['upload_path'] = './uploads/';                                // Buat folder dengan nama "fileExcel" di root folder
                    $config['file_name'] = $fileName;
                    $config['allowed_types'] = 'xls|xlsx|csv';
                    $config['max_size'] = 10000;
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('file'))
                        $this->upload->display_errors();
                    $media = $this->upload->data('file');
                    $inputFileName = './uploads/' . $fileName;
                    try {
                        $inputFileType = IOFactory::identify($inputFileName);
                        $objReader = IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
    
                        $res = returnResultCustomTransfer(false,"Failed Import Log","canceled");
                    }
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();
                    for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $data = [
                        "title" => $rowData[0][0],
                        "price" => $rowData[0][1],
                        "stock" => $rowData[0][2],
                        "category" => $rowData[0][3],
                        "condit" => $rowData[0][4],
                        "weight" => $rowData[0][5],
                        "kode_buku" => $rowData[0][6],
                        "penerbit" => $rowData[0][7],
                        "ukuran" => $rowData[0][8],
                        "jml_halaman" => $rowData[0][9],
                        "isbn" => $rowData[0][10],
                        "img" => $rowData[0][11],
                        "description" => $rowData[0][12],
                        "date_submit" => date('Y-m-d H:i:s'),
                        "publish" => $rowData[0][14],
                        "slug" => $rowData[0][15]
                    ];
                        $this->db->insert('products', $data);
                        
                        delete_files($inputFileName);                                 
                    }
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Excel Resi berhasil diproses',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/products');
                }else if($tipe == 1){
                    $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload
                    $config['upload_path'] = './uploads/';                                // Buat folder dengan nama "fileExcel" di root folder
                    $config['file_name'] = $fileName;
                    $config['allowed_types'] = 'xls|xlsx|csv';
                    $config['max_size'] = 10000;
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('file'))
                        $this->upload->display_errors();
                    $media = $this->upload->data('file');
                    $inputFileName = './uploads/' . $fileName;
                    try {
                        $inputFileType = IOFactory::identify($inputFileName);
                        $objReader = IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());

                        $res = returnResultCustomTransfer(false,"Failed Import Log","canceled");
                    }
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();
                    for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        
                    $data = [
                        "title" => $rowData[0][1],
                        "price" => $rowData[0][2],
                        "stock" => $rowData[0][3],
                        "category" => $rowData[0][4],
                        "condit" => $rowData[0][5],
                        "weight" => $rowData[0][6],
                        "kode_buku" => $rowData[0][7],
                        "penerbit" => $rowData[0][8],
                        "ukuran" => $rowData[0][9],
                        "jml_halaman" => $rowData[0][10],
                        "isbn" => $rowData[0][11],
                        "img" => $rowData[0][12],
                        "description" => $rowData[0][13],
                        "date_submit" => $rowData[0][14],
                        "publish" => $rowData[0][15],
                        "slug" => $rowData[0][16],
                        "promo_price" => $rowData[0][18]
                    ];
                    if ($rowData[0][0] != 0) {
				        $this->db->where('id',$rowData[0][0]);
                        $this->db->update('products', $data);
                    }
                        delete_files($inputFileName);                                 
                    }
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Excel Resi berhasil diproses',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/products');
                }else{
                    $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload
                    $config['upload_path'] = './uploads/';                                // Buat folder dengan nama "fileExcel" di root folder
                    $config['file_name'] = $fileName;
                    $config['allowed_types'] = 'xls|xlsx|csv';
                    $config['max_size'] = 10000;
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('file'))
                        $this->upload->display_errors();
                    $media = $this->upload->data('file');
                    $inputFileName = './uploads/' . $fileName;
                    try {
                        $inputFileType = IOFactory::identify($inputFileName);
                        $objReader = IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());

                        $res = returnResultCustomTransfer(false,"Failed Import Flashsale Product","canceled");
                    }
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();
                    for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        
                    $data = [
                        "flashsale_price" => $rowData[0][1],
                    ];
                    // $qIdBuku = $this->db->where('kode_buku',$rowData[0][0])->get('products')->row()->id;
                    // $data_in = [
                    //     'id_product' => $qIdBuku
                    // ];
                    // $in = $this->db->insert('flashsale', $data_in);
                    // if ($rowData[0][0] != 0) {  
                            $this->db->where('kode_buku',$rowData[0][0]);
                            $this->db->update('products', $data);
                    // }
                        delete_files($inputFileName);                                 
                    }
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Excel Flashsale berhasil di import',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/products');
                }
                
            }
    }
    public function download_data_produk()
    {
        $qgetProduk = $this->db->query("select * from products")->result();
        

        $object = new Spreadsheet();
        $object->setActiveSheetIndex(0);
       
        $table_columns = array("No","id","title","price","stock","category","condit","weight","kode_buku","penerbit","ukuran","jml_halaman","isbn","img","description","date_submit","publish","slug","transaction","promo_price","viewer","featured","landing_status");

        $column = 0;

        foreach($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $no = 0;
        foreach($qgetProduk as $v)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no++);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row,$v->id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$v->title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row,$v->price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row,$v->stock);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row,$v->category);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row,$v->condit);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row,$v->weight);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,$v->kode_buku);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row,$v->penerbit);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,$v->ukuran);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$v->jml_halaman);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row,$v->isbn);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,$v->img);
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,$v->description);
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row,$v->date_submit);
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row,$v->publish);
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row,$v->slug);
            $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row,$v->transaction);
            $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $v->promo_price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row,$v->viewer);
            $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row,$v->featured);
            $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row,$v->landing_status);
            $excel_row++;
        }

        $object_writer = new Xlsx($object);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data-Produk-'.date('jFy').'.xlsx"');
        header('Cache-Control: max-age=0');
        $object_writer->save('php://output');
    }
    public function delete_hadiah($id){

        
        $get = $this->db->get_where('gift_product', ['id' => $id])->row_array();
        unlink("./assets/images/product_hadiah/".$get['img_gift']);

        $this->db->where('id', $id);
        $this->db->delete('gift_product');

        

        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Hadiah Produk berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/product/hadiah');
    }
    public function hadiah_product()
    {
        $this->form_validation->set_rules('title', 'title', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Hadiah Produk - Dapur Panel';
            $data['getProducts'] =  $this->Products_model->getAlltitleproduct();
            $data['getHadiahProducts'] =  $this->Products_model->getHadiahProduct();
            // echo $this->db->last_query();
            // exit;
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/hadiah_product', $data);
            $this->load->view('templates/footer_admin');
        }else{
                $data = array();
                $upload = $this->Products_model->uploadImgHadiah();

                if($upload['result'] == 'success'){
                    $this->Products_model->insertHadiah($upload);
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Hadiah Produk berhasil ditambah',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/product/hadiah');
                }else{
                    // echo json_encode($upload);
                    // exit;
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal mengubah produk, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
                </div>");
                    redirect(base_url() . 'administrator/product/hadiah');
                }
        }
    }
    public function voucher()
    {
        $this->form_validation->set_rules('judul', 'judul', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Voucher - Dapur Panel';
            $data['getVouchers'] =  $this->Voucher_model->getVoucher();
            // echo $this->db->last_query();
            // exit;
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/voucher', $data);
            $this->load->view('templates/footer_admin');
        }else{
                $data = array();
                $insert = $this->Voucher_model->tambah();

                if($insert){
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Voucher berhasil ditambah',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/voucher');
                }else{
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal menambah voucher. Silakan ulangi lagi.
                </div>");
                    redirect(base_url() . 'administrator/voucher');
                }
        }
    }
    public function delete_voucher($id){
        $this->db->where('id', $id);
        $this->db->delete('voucher');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Voucher  berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/voucher');
    }
    public function add_product(){
        $this->form_validation->set_rules('title', 'title', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Tambah Produk - Dapur Panel';
            $data['categories'] = $this->Categories_model->getCategorieswithParent();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_product', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Products_model->uploadImg();

            if($upload['result'] == 'success'){
                $this->Products_model->insertProduct($upload);
                if($this->input->post('sendemail') == 1){
                    $mail = $this->db->get('subscriber');
                    $title = $this->input->post('title');
                    function toSlugFromText($text='') {
                        $text = trim($text);
                        if (empty($text)) return '';
                        $text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
                        $text = strtolower(trim($text));
                        $text = str_replace(' ', '-', $text);
                        $text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
                        return $text;
                    }
                    foreach($mail->result_array() as $d){
                        // $this->load->library('email');
                        // $config['charset'] = 'utf-8';
                        // $config['useragent'] = $this->config->item('app_name');
                        // $config['smtp_crypto'] = $this->config->item('smtp_crypto');
                        // $config['protocol'] = 'smtp';
                        // $config['mailtype'] = 'html';
                        // $config['smtp_host'] = $this->config->item('host_mail');
                        // $config['smtp_port'] = $this->config->item('port_mail');
                        // $config['smtp_timeout'] = '5';
                        // $config['smtp_user'] = $this->config->item('mail_account');
                        // $config['smtp_pass'] = $this->config->item('pass_mail');
                        // $config['crlf'] = "\r\n";
                        // $config['newline'] = "\r\n";
                        // $config['wordwrap'] = TRUE;
    
                        // $message = '<p>
                        // Hai
                        // Gimana kabarnya? Kami punya produk terbaru nih. Ayo buruan dapatkan sebelum kehabisan stok.
                        // <strong>'.$this->input->post('title').'</strong>
                        // <stong>Rp '.number_format($this->input->post('price'),0,",",".").'</stong>
                        // <a href="'.base_url().'p/'.toSlugFromText($title).'">Lihat Produk</a></p>
                        // <br/><br/>
                        // <a href="'.base_url().'unsubscribe-email?email='.$d['email'].'&code='.$d['code'].'">Berhenti berlangganan</a>
                        // ';
                        // $this->email->initialize($config);
                        // $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
                        // $this->email->to($d['email']);
                        // $this->email->subject($this->input->post('title'));
                        // $this->email->message(nl2br($message));
                        // $this->email->send();
                    }
                }
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Produk berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/products');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah produk, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/product/add');
            }
        }
    }

    public function add_img_product($id){
        $this->form_validation->set_rules('help', 'help', 'required');
        if($this->form_validation->run() == false){
            $data['title'] = 'Tambah Gambar - Dapur Panel';
            $data['product'] = $this->Products_model->getProductById($id);
            $data['img'] = $this->db->get_where('img_product', ['id_product' => $id]);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_img_product', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Products_model->uploadImg();
            if($upload['result'] == 'success'){
                $this->Products_model->insertImg($upload, $id);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Gambar berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/product/add-img/'.$id);
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah gambar, pastikan gambar berukuran maksimal 10mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
              </div>");
                redirect(base_url() . 'administrator/product/add-img/'.$id);
            }
        }
    }

    public function add_grosir_product($id){
        $this->form_validation->set_rules('min', 'min', 'required', ['required' => 'Jumlah min. harus diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Tambah Harga Grosir - Dapur Panel';
            $data['product'] = $this->Products_model->getProductById($id);
            $this->db->order_by('id', 'desc');
            $data['grosir'] = $this->db->get_where('grosir', ['product' => $id]);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_grosir_product', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->db->order_by('id', 'desc');
            $check = $this->db->get_where('grosir', ['product' => $id]);
            $min = $this->input->post('min');
            $price = $this->input->post('price');
            $product = $this->Products_model->getProductById($id);
            if($check->num_rows() > 0){
                $jmlsebelumnya = $check->row_array()['min'] + 1;
                if($min < $jmlsebelumnya){
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal menambahkan harga grosir, pastikan jumlah minimal adalah $jmlsebelumnya
                  </div>");
                    redirect(base_url() . 'administrator/product/grosir/'.$id);
                }else{
                    $data = [
                        'min' => $min,
                        'price' => $price,
                        'product' => $id
                    ];
                    $this->db->insert('grosir', $data);
                    $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Harga grosir berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/product/grosir/'.$id);
                }
            }else{
                if($min < 2){
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal menambahkan harga grosir, pastikan jumlah minimal adalah 2
                  </div>");
                    redirect(base_url() . 'administrator/product/grosir/'.$id);
                }else{
                    $data = [
                        'min' => $min,
                        'price' => $price,
                        'product' => $id
                    ];
                    $this->db->insert('grosir', $data);
                    $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Harga grosir berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                    redirect(base_url() . 'administrator/product/grosir/'.$id);
                }
            }
        }
    }

    public function delete_grosir(){
        $id = $_GET['id'];
        $product = $_GET['product'];
        $this->db->where('id', $id);
        $this->db->delete('grosir');
        $this->session->set_flashdata('upload', "<script>
        swal({
        text: 'Grosir berhasil dihapus',
        icon: 'success'
        });
        </script>");
        redirect(base_url() . 'administrator/product/grosir/'.$product);
    }

    public function delete_img_other_product($id, $idp){
        $this->db->where('id', $id);
        $this->db->delete('img_product');
        $this->session->set_flashdata('upload', "<script>
        swal({
        text: 'Gambar berhasil dihapus',
        icon: 'success'
        });
        </script>");
        redirect(base_url() . 'administrator/product/add-img/'.$idp);
    }

    public function edit_product($id){
        $this->form_validation->set_rules('title', 'title', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Edit Produk - Dapur Panel';
            $data['categories'] = $this->Categories_model->getCategorieswithParent();
            $data['product'] = $this->Products_model->getProductById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_product', $data);
            $this->load->view('templates/footer_admin');
        }else{
            if($_FILES['img']['name'] != ""){
                $data = array();
                $upload = $this->Products_model->uploadImg();

                if($upload['result'] == 'success'){
                    $this->Products_model->updateProduct($upload['file']['file_name'], $id);
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Produk berhasil diubah',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/products');
                }else{
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal mengubah produk, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
                </div>");
                    redirect(base_url() . 'administrator/product/' . $id . '/edit');
                }
            }else{
                $oldImg = $this->input->post('oldImg');
                $this->Products_model->updateProduct($oldImg, $id);
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Produk berhasil diubah',
                    icon: 'success'
                    });
                    </script>");
                redirect(base_url() . 'administrator/products');
            }
        }
    }

    public function product($id){
        $data['title'] = 'Detail Produk - Dapur Panel';
        $data['product'] = $this->Products_model->getProductById($id);
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/detail_product', $data);
        $this->load->view('templates/footer_admin');
    }

    public function delete_product($id){
        $this->db->where('id', $id);
        $this->db->delete('products');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Produk berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/products');
    }

    // promo
    public function flashsale(){
        $data['title'] = 'Flashsale Produk - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/flashsale/';
        $config['total_rows'] = $this->Flashsale_model->getProducts("","")->num_rows();
        $config['per_page'] = 10;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['getProducts'] = $this->Flashsale_model->getProducts();
        $data['setting'] = $this->Settings_model->getSetting();
        $data['promo'] = $this->Flashsale_model->getPromo();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/flashsale', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_flashsale(){
        $this->Flashsale_model->insertPromo();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Produk berhasil dijadikan flashsale',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/flashsale');
    }

    public function delete_flashsale($id){
        $this->Flashsale_model->deletePromo($id);
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Produk untuk flashsale berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/flashsale');
    }

    // promo
    public function promo(){
        $data['title'] = 'Promo Produk - Dapur Panel';
        $config['base_url'] = base_url() . 'administrator/promo/';
        $config['total_rows'] = $this->Promo_model->getProducts("","")->num_rows();
        $config['per_page'] = 10;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['getProducts'] = $this->Promo_model->getProducts($config['per_page'], $from);
        $data['setting'] = $this->Settings_model->getSetting();
        $data['promo'] = $this->Promo_model->getPromo();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/promo', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_promo(){
        $this->Promo_model->insertPromo();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Produk berhasil dijadikan promo',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/promo');
    }

    public function delete_promo($id){
        $this->Promo_model->deletePromo($id);
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Produk untuk promo berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/promo');
    }

    // testimonials
    public function testimonials(){
        $this->form_validation->set_rules('name', 'Name', 'required', ['required' => 'Nama kategori wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Testimosi - Dapur Panel';
            $data['testi'] = $this->Testi_model->getTesti();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/testi', $data);
            $this->load->view('templates/footer_admin');
        }else{

            $data = array();
                $upload = $this->Testi_model->uploadImgTesti();

                if($upload['result'] == 'success'){
                    $this->Testi_model->insertTesti($upload);
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Testimoni berhasil ditambahkan',
                        icon: 'success'
                        });
                        </script>");
                    redirect(base_url() . 'administrator/testimonials');
                }else{
                    // echo json_encode($upload);
                    // exit;
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal menambah testimonial produk, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
                </div>");
                redirect(base_url() . 'administrator/testimonials');
                }
        }
    }

    public function testimonial($id){
        $this->form_validation->set_rules('name', 'Name', 'required', ['required' => 'Nama kategori wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Edit Testimosi - Dapur Panel';
            $data['testi'] = $this->Testi_model->getTestiById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_testi', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Testi_model->updateTesti($id);
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Testimoni berhasil diubah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/testimonials');
        }
    }

    public function delete_testimonial($id){
        $get = $this->db->get_where('testimonial', ['id' => $id])->row_array();
        unlink("./assets/images/testimonial/".$get['photo']);
        
        $this->db->where('id', $id);
        $this->db->delete('testimonial');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Testimoni berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/testimonials');
    }

    public function pages(){
        $data['title'] = 'Halaman - Dapur Panel';
        $data['pages'] = $this->Settings_model->getPages();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/pages', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_page(){
        $this->form_validation->set_rules('title', 'Judul', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Tambah Halaman - Dapur Panel';
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_page', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->insertPage();
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Halaman berhasil ditambahkan',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/pages');
        }
    }

    public function edit_page($id){
        $this->form_validation->set_rules('title', 'Judul', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Edit Halaman - Dapur Panel';
            $data['page'] = $this->Settings_model->getPageById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_page', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->updatePage($id);
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Halaman berhasil diubah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/pages');
        }
    }

    public function delete_page($id){
        $this->db->where('id', $id);
        $this->db->delete('pages');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Halaman Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/pages');
    }

    // settings
    public function settings(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/settings', $data);
        $this->load->view('templates/footer_admin');
    }
    public function delete_typewriter($id)
    {
        $get = $this->db->get_where('typewriter_note', ['id' => $id])->row_array();
        unlink("./assets/images/typewriter_note/".$get['img']);

        $this->db->where('id', $id);
        $this->db->delete('typewriter_note');

        

        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Typewriter Note berhasil dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/typewriter_note');
       
    }
    public function typewriter_note()
    {
        $this->form_validation->set_rules('title', 'title', 'required', ['required' => 'Judul wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Typewriter Note Request - Dapur Panel';
            $data['getTypewriter'] =  $this->Settings_model->getAllTypewriter();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/typewriter_note', $data);
            $this->load->view('templates/footer_admin');
        }else{
                $data = array();
                $upload = $this->Settings_model->uploadImgTypewriter();

                if($upload['result'] == 'success'){
                    $this->Settings_model->insertTypewriter($upload);
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Typewriter berhasil ditambah',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/setting/typewriter_note');
                }else{
                    // echo json_encode($upload);
                    // exit;
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal Tambah Typewriter, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.
                </div>");
                    redirect(base_url() . 'administrator/setting/typewriter_note');
                }
        }
    }
    public function home_setting(){
        $this->form_validation->set_rules('category_first', 'Kategori', 'required', ['required' => 'Kategori Section Top wajib diisi']);
        if($this->form_validation->run() == false){
          $data['title'] = 'Pengaturan - Dapur Panel';

          $data['address'] = $this->Settings_model->getSetting();


          $getHomeSettings = $this->db->get('home_setting')->row_array();
          $data['categories'] = $this->Settings_model->getCategory();
          $data['selectCategoryTop'] = $this->Settings_model->getCategoryById($getHomeSettings['section_category_first']);
          $data['selectCategoryTwo'] = $this->Settings_model->getCategoryById($getHomeSettings['section_category_two']);
          $data['selectCategoryThree'] = $this->Settings_model->getCategoryById($getHomeSettings['section_category_three']);
          $data['getHomesettings'] = $getHomeSettings;
          $this->load->view('templates/header_admin', $data);
          $this->load->view('administrator/setting_home', $data);
          $this->load->view('templates/footer_admin');
        }else{
          $this->Settings_model->editHome();
          $this->session->set_flashdata('upload', "<script>
              swal({
              text: 'Home Berhasil Diubah',
              icon: 'success'
              });
              </script>");
          redirect(base_url() . 'administrator/setting/home');
        }
    }
    public function trafic()
    {
        $data['title'] = 'Report - Trafic';

        
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/trafic', $data);
        $this->load->view('templates/footer_admin');
    }
    public function datatrafic()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $list = $this->Visitor_model->visit_Dash($start,$end);
        // echo $this->db->last_query();
        // exit;
        $visit = [];
        foreach($list as $row) {
                $visit[] = ['date' => date('d F H:i',strtotime($row['time'])), 
                            'count' =>$row['total'],
                            'count_order' => $this->Visitor_model->getCountoRderByDate($row['date']),
                            'log' => $this->db->last_query()];
            }
        echo json_encode($visit);
    }
    public function slide_preview(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['slider'] = $this->Settings_model->getSlidePreview();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_slide_preview', $data);
        $this->load->view('templates/footer_admin');
    }
    public function add_slide_preview_setting(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/add_setting_slide_preview', $data);
        $this->load->view('templates/footer_admin');
    }
    public function add_slide_preview_setting_post(){
        $data = array();
        $upload = $this->Settings_model->uploadImgSlidePreview();
        if($upload['result'] == 'success'){
            $insert = $this->Settings_model->insertSlidePreview($upload);
            if($insert){
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Slide Preview berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                redirect(base_url() . 'administrator/setting/slide_preview');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah slide preview, gambar yang kamu upload tidak berukuran 2000x1600px.
                </div>");
                redirect(base_url() . 'administrator/setting/slide_preview/add');
            }
        }else{
            $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
            Gagal menambah slide preview, Pastikan gambar berukuran maksimal 4mb, berformat png, jpg, jpeg. Dan berukuran 2000x1600px.
            </div>");
            redirect(base_url() . 'administrator/setting/slide_preview/add');
        }
    }
    
    public function delete_slide_preview($id){
        $this->db->where('id', $id);
        $this->db->delete('preview_section');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Slide Preview Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
            redirect(base_url() . 'administrator/setting/slide_preview');
    }
    public function small_thumbnail_book(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['small_thumbnail'] = $this->Settings_model->getSmallThumbnailCover();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_small_thumbnail', $data);
        $this->load->view('templates/footer_admin');
    }
    public function edit_small_thumbnail_book($id){
        $this->form_validation->set_rules('url', 'Url', 'required', ['required' => 'Url wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['thumbnail'] = $this->Settings_model->getSmallThumbnailCoverById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_setting_small_thumbnail', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $data = array();
            $upload = $this->Settings_model->uploadImgThumbnailPreview();
            if($upload['result'] == 'success'){
                $insert = $this->Settings_model->editSmallThumbnailCover($id,$upload);
                if($insert){
                    $this->session->set_flashdata('upload', "<script>
                        swal({
                        text: 'Small Thumbnail berhasil ditambahkan',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'administrator/setting/small_thumbnail_book');
                }else{
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal mengubah Small Thumbnail, gambar yang kamu upload tidak berukuran 1000x800px.
                    </div>");
                    redirect(base_url() . 'administrator/edit_small_thumbnail_book/' . $id);
                }
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal mengubah Small Thumbnail, pastikan banner berukuran maksimal 2mb, berformat png, jpg, jpeg. Dan berukuran 1000x800px.
                </div>");
                redirect(base_url() . 'administrator/edit_small_thumbnail_book/' . $id);
            }

        }
    }
    public function banner_setting(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['banner'] = $this->Settings_model->getBannerAll();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_banner', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_banner_setting(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/add_setting_banner', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_banner_setting_post(){
        $data = array();
        $upload = $this->Settings_model->uploadImg();
        if($upload['result'] == 'success'){
            $insert = $this->Settings_model->insertBanner($upload);
            if($insert){
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Banner berhasil ditambahkan',
                    icon: 'success'
                    });
                    </script>");
                redirect(base_url() . 'administrator/setting/banner');
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                Gagal menambah banner, gambar yang kamu upload tidak berukuran 1700x400px.
                </div>");
                redirect(base_url() . 'administrator/setting/banner/add');
            }
        }else{
            $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
            Gagal menambah banner, pastikan banner berukuran maksimal 2mb, berformat png, jpg, jpeg. Dan berukuran 1600x400px.
            </div>");
            redirect(base_url() . 'administrator/setting/banner/add');
        }
    }

    public function delete_banner($id){
        $this->db->where('id', $id);
        $this->db->delete('banner');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Banner Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/banner');
    }

    public function description_setting(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['desc'] = $this->Settings_model->getSetting();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_description', $data);
        $this->load->view('templates/footer_admin');
    }

    public function edit_description_setting(){
        $this->Settings_model->editDescription();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Deskripsi singkat berhasil diubah',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/description');
    }

    public function rekening_setting(){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['rekening'] = $this->Settings_model->getRekening();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_rekening', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_rekening_setting(){
        $this->form_validation->set_rules('name', 'Nama', 'required', ['required' => 'Nama wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_setting_rekening', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->addRekening();
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Rekening Berhasil Disimpan',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/rekening');
        }
    }

    public function edit_rekening_setting($id){
        $this->form_validation->set_rules('name', 'Nama', 'required', ['required' => 'Nama wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['rekening'] = $this->Settings_model->getRekeningById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_setting_rekening', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->editRekening($id);
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Rekening Berhasil Diubah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/rekening');
        }
    }

    public function delete_rekening($id){
        $this->db->where('id', $id);
        $this->db->delete('rekening');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Rekening Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/rekening');
    }

    public function sosmed_setting(){
      $data['title'] = 'Pengaturan - Dapur Panel';
      $data['sosmed'] = $this->Settings_model->getSosmed();
      $this->load->view('templates/header_admin', $data);
      $this->load->view('administrator/setting_sosmed', $data);
      $this->load->view('templates/footer_admin');
    }

    public function add_sosmed_setting(){
      $this->form_validation->set_rules('name', 'Nama', 'required', ['required' => 'Nama wajib diisi']);
      if($this->form_validation->run() == false){
          $data['title'] = 'Pengaturan - Dapur Panel';
          $this->load->view('templates/header_admin', $data);
          $this->load->view('administrator/add_setting_sosmed', $data);
          $this->load->view('templates/footer_admin');
      }else{
          $this->Settings_model->addSosmed();
          $this->session->set_flashdata('upload', "<script>
              swal({
              text: 'Sosial Media Berhasil Disimpan',
              icon: 'success'
              });
              </script>");
          redirect(base_url() . 'administrator/setting/sosmed');
      }
    }

    public function edit_sosmed_setting($id){
        $this->form_validation->set_rules('name', 'Nama', 'required', ['required' => 'Nama wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['sosmed'] = $this->Settings_model->getSosmedById($id);
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_setting_sosmed', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->editSosmed($id);
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Sosmed Berhasil Diubah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/sosmed');
        }
    }

    public function delete_sosmed($id){
        $this->db->where('id', $id);
        $this->db->delete('sosmed');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Sosmed Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/sosmed');
    }

    public function address_setting(){
      $this->form_validation->set_rules('address', 'Alamat', 'required', ['required' => 'Alamat wajib diisi']);
      if($this->form_validation->run() == false){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['address'] = $this->Settings_model->getSetting();
        $data['regency'] = $this->Settings_model->getRegency();
        $data['selectRegency'] = $this->Settings_model->getRegencyById();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_address', $data);
        $this->load->view('templates/footer_admin');
      }else{
        $this->Settings_model->editAddress();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Alamat Berhasil Diubah',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/address');
      }
    }

    public function delivery_setting(){
        $data['title'] = 'Biaya Antar - Dapur Panel';
        $data['delivery'] = $this->Settings_model->getCostDelivery();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_delivery', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_delivery_setting(){
        $this->form_validation->set_rules('destination', 'Tujuan', 'required', ['required' => 'Tujuan wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['regency'] = $this->Settings_model->getRegency();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_setting_delovery', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->addDelivery();
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Biaya Antar Berhasil Ditambah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/delivery');
        }
    }

    public function edit_delivery_setting($id){
        $this->form_validation->set_rules('destination', 'Tujuan', 'required', ['required' => 'Tujuan wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['delivery'] = $this->Settings_model->getCostDeliveryById($id);
            $data['regency'] = $this->Settings_model->getRegency();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/edit_setting_delivery', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->editDelivery($id);
            $destination = $this->input->post('destination', true);
            $price = $this->input->post('price', true);
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Biaya Antar Berhasil Diubah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/delivery');
        }
    }

    public function delete_delivery($id){
        $this->db->where('id', $id);
        $this->db->delete('cost_delivery');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Biaya Antar Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/delivery');
    }

    public function cod_setting(){
        $data['title'] = 'Cash on Delivery - Dapur Panel';
        $data['cod'] = $this->Settings_model->getCOD();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_cod', $data);
        $this->load->view('templates/footer_admin');
    }

    public function add_cod_setting(){
        $this->form_validation->set_rules('destination', 'Tujuan', 'required', ['required' => 'Tujuan wajib diisi']);
        if($this->form_validation->run() == false){
            $data['title'] = 'Pengaturan - Dapur Panel';
            $data['regency'] = $this->Settings_model->getRegency();
            $this->load->view('templates/header_admin', $data);
            $this->load->view('administrator/add_setting_cod', $data);
            $this->load->view('templates/footer_admin');
        }else{
            $this->Settings_model->addCOD();
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'COD Berhasil Ditambah',
                icon: 'success'
                });
                </script>");
            redirect(base_url() . 'administrator/setting/cod');
        }
    }

    public function delete_cod($id){
        $this->db->where('id', $id);
        $this->db->delete('cod');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'COD Berhasil Dihapus',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/cod');
    }

    public function footer_setting(){
      $this->form_validation->set_rules('page', 'Page', 'required', ['required' => 'Page wajib diisi']);
      if($this->form_validation->run() == false){
        $data['title'] = 'Pengaturan - Dapur Panel';
        $data['footerhelp'] = $this->Settings_model->getFooterHelp();
        $data['footerinfo'] = $this->Settings_model->getFooterInfo();
        $data['pages'] = $this->Settings_model->getPages();
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/setting_footer', $data);
        $this->load->view('templates/footer_admin');
      }else{
        $this->Settings_model->addFooter();
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Navigasi Footer berhasil ditambah',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/setting/footer');
      }
    }

    public function off_flashsale_setting($type){
        if($type == 1){
            $this->db->set('flashsale', 0);
            return $this->db->update('settings');
        }else{
            $this->db->set('flashsale', 1);
            return $this->db->update('settings');
        }
    }

    public function set_time_flashsale(){
        $pdate = $this->input->post("pdate");
        $this->db->set('flashsale_time', $pdate);
        return $this->db->update('settings');
    }

    public function off_promo_setting($type){
        if($type == 1){
            $this->db->set('promo', 0);
            return $this->db->update('settings');
        }else{
            $this->db->set('promo', 1);
            return $this->db->update('settings');
        }
    }

    public function set_time_promo(){
        $pdate = $this->input->post("pdate");
        $this->db->set('promo_time', $pdate);
        return $this->db->update('settings');
    }

    // ajax
    public function ajax_get_product_by_id($id){
        $return = $this->Products_model->getProductById($id);
        echo json_encode($return);
    }

    // edit
    public function edit(){
        $data['title'] = 'Edit Profil Admin - Dapur Panel';
        $admin = $this->db->get('admin')->row_array();
        $data['admin'] = $admin;
        $this->load->view('templates/header_admin', $data);
        $this->load->view('administrator/edit', $data);
        $this->load->view('templates/footer_admin');
    }

    public function edit_username(){
        $this->db->set('username', $this->input->post('username'));
        $this->db->update('admin');
        $this->session->set_flashdata('upload', "<script>
            swal({
            text: 'Username berhasil diubah',
            icon: 'success'
            });
            </script>");
        redirect(base_url() . 'administrator/edit');
    }

    public function edit_pass(){
        $admin = $this->db->get('admin')->row_array();
        if(password_verify($this->input->post('oldPassword'), $admin['password'])){
            if($this->input->post('newPassword') ==  $this->input->post('confirmPassword')){
                $pass = password_hash($this->input->post('newPassword'), PASSWORD_DEFAULT);
                $this->db->set('password', $pass);
                $this->db->update('admin');
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Password berhasil diubah',
                    icon: 'success'
                    });
                    </script>");
                redirect(base_url() . 'administrator/edit');
            }else{
                $this->session->set_flashdata('upload', "<script>
                    swal({
                    text: 'Konfirmasi password tidak sama. Silakan coba lagi',
                    icon: 'error'
                    });
                    </script>");
                redirect(base_url() . 'administrator/edit');
            }
        }else{
            $this->session->set_flashdata('upload', "<script>
                swal({
                text: 'Password lama salah. Silakan coba lagi',
                icon: 'error'
                });
                </script>");
            redirect(base_url() . 'administrator/edit');
        }
    }

    public function logout(){
        $sess = ['admin'];
		$this->session->unset_userdata($sess);
        delete_cookie('djehbicd');
        redirect(base_url() . 'dapur');
    }


}
