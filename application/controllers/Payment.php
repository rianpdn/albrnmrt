<?php
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

use Xendit\Xendit;
class Payment extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Categories_model');
        $this->load->model('Payment_model');
        $this->load->model('Settings_model');
        $this->load->model('Order_model');
        $this->load->model('User_model');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
        $allsite = current_url();
		$address_visitor = str_replace('index.php/', '', $allsite);
		$this->visitor->counter($address_visitor);
    }

    public function index(){
        if(!$this->session->userdata('login')){
            $cookie = get_cookie('ibq2cy38y');
            if($cookie == NULL){
                redirect(base_url() . 'login?redirect=payment');
            }else{
                $getCookie = $this->db->get_where('user', ['cookie' => $cookie])->row_array();
                if($getCookie){
                    $dataCookie = $getCookie;
                    $dataSession = [
                        'id' => $dataCookie['id']
                    ];
                    $this->session->set_userdata('login', true);
                    $this->session->set_userdata($dataSession);
                }else{
                    redirect(base_url() . 'login?redirect=payment');
                }
            }
        }
        $data['title'] = 'Pembayaran - ' . $this->config->item('app_name');
        $data['css'] = 'payment';
        $data['cart'] = $this->Order_model->getCartUser();
        $data['provinces'] = $this->Payment_model->getProvinces();
        $data['user'] = $this->User_model->getProfile();
        $provinsi = $this->session->userdata('provinsi');
        $city = $this->session->userdata('city');
        // $data['selectProvince'] = $this->Settings_model->getProvinceById($provinsi);
        $data['selectCity'] = $this->Settings_model->getCityById($city);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar');
        $this->load->view('page/payment', $data);
        $this->load->view('templates/footerv2');
    }
    public function apply_voucher()
    {
        $kode = $this->input->post('kode',true);
        $qgetVoucher = $this->db->get_where('voucher', ['kode' => $kode]);
        if ($qgetVoucher->num_rows() != 0) {
          
            $v = $qgetVoucher->row_array();

            $getUserVoucher = $this->db->get_where('voucher_user', ['id_voucher' => $v['id']])->row_array();
            if ($getUserVoucher) {
                if ($getUserVoucher['status']!=1) {
                    $dataup=array(
                        'status' => 1
                    );
                    $this->db->where('id_user',$this->session->userdata('id'))->update('voucher_user',$dataup);
                }
                
            }
            $res=array(
                'success' => true,
                'data' => $v
            );

        }else{
            $res = array(
                'success' => false,
                'msg' => 'Kode Voucher Tidak Ditemukan'
            );
        }
        echo json_encode($res);
    }
    public function getLocation(){
        $id = $this->input->post('id');
        $getLocation = $this->Payment_model->getCity($id);
        $list = "<option></option>";
        foreach($getLocation as $d){
            $list .= "<option value='".$d['city_id']."'>".$d['type'].' '.$d['city_name']."";
        }
        echo json_encode($list);
    }

    public function getService(){
        $jne = $this->Payment_model->getService("jne");
        // $pos = $this->Payment_model->getService("pos");
        // $tiki = $this->Payment_model->getService("tiki");
        // echo json_encode($jne);
        // exit;
        // $cart = $this->Order_model->getCartUser();
        // echo json_encode($cart);
        // exit;
        $destination = $this->input->post('destination');
        $db = $this->db->get_where('cost_delivery', ['destination' => $destination])->row_array();
        $cod = $this->db->get_where('cod', ['regency_id' => $destination])->row_array();
        $list = "<option></option>";
        $cost = "";
        if($cod){
            $list .= '<option value="'.$cod['price'].'-COD-cod">COD (Cash of Delivery)</option>';
        }
        if($db){
            $list .= '<option value="'.$db['price'].'-SehariCity-SehariCity">Sehari City Courier(Free Ongkir)</option>';
        }else {
            if(count($jne) > 0){
                foreach($jne as $s){
                    $list .= '<option value="'.$s['cost'][0]['value']."-".$s['service'].'-jne">' ."JNE"." ".$s['description']. " (".$s['service'].")". " (".number_format($s['cost'][0]['value'],0,",",".").")".'</option>';
                };
            }
            // if(count($pos) > 0){
            //     foreach($pos as $s){
            //         $list .= '<option value="'.$s['cost'][0]['value']."-".$s['service'].'-pos">'."POS"." ".$s['description']." (".$s['service'].")".'</option>';
            //     };
            // }
            // if(count($tiki) > 0){
            //     foreach($tiki as $s){
            //         $list .= '<option value="'.$s['cost'][0]['value']."-".$s['service'].'-tiki">'."TIKI"." ".$s['description']." (".$s['service'].")".'</option>';
            //     };
            // }
        }
        echo json_encode($list);
    }
    public function testwa()
    {
        $textWa ="Halo rian. Terima Kasih telah melakukan pemesanan di Albiruni Mart, \n\n mohon untuk melakukan pembayaran. Jika sudah melakukan pembayaran, silakan melakukan konfirmasi pembayaran ". base_url()."payment/confirmation serta bisa melalui Whatsapp ".$this->config->item('whatsapp')." dengan format sebagai berikut: 1. Kode Pesanan ; 2. Transfer Dari Bank ; 3. Transfer Ke Bank . *Sertakan juga bukti transfer.  Produk : test produk ; Kode Pesanan : kodetest  ; Tanggal Pesanan :31 agustus 2021 ; Total Keseluruhan yang harus ditransfer : *Rp".number_format(40000, 0, ',', '.').". *Pesanan akan dikirim setelah kami menerima pembayaran Anda. Terima Kasih";
        // echo trim($textWa);
        // exit;
        kirimWa('081294377957',$textWa);
    }
    public function succesfully(){
  
        if($this->input->post('name') == NULL){
            redirect(base_url());
        }
        $suc = $this->Payment_model->succesfully();

        if ($suc['success']) {
            
            $this->db->where('user', $this->session->userdata('id'));
            $this->db->delete('cart');

            $produknya = array(); 
            
            $getProduk = $this->db->where('id_invoice', $suc['invoice'])->get('transaction')->result();
            foreach ($getProduk as $s) {
                $produknya[] = $s->product_name;
            }
            $textWa ="Halo ".$suc['name']." \n,Terima Kasih telah melakukan pemesanan di Albiruni Mart, mohon untuk melakukan pembayaran.\nJika sudah melakukan pembayaran, silakan melakukan konfirmasi pembayaran ". base_url()."payment/confirmation serta bisa melalui Whatsapp ".$this->config->item('whatsapp')." dengan format sebagai berikut:\n  1. Kode Pesanan \n 2. Transfer Dari Bank \n 3. Transfer Ke Bank \n Sertakan juga bukti transfer.\n  ==============================================  \n Produk : ".implode(",",$produknya)." \n Kode Pesanan : ".$suc['invoice']." \n Tanggal Pesanan : ".$suc['date']." \n Total Keseluruhan yang harus ditransfer : Rp".number_format($suc['totalall'], 0, ',', '.')." \n Pesanan akan dikirim setelah kami menerima pembayaran Anda.\n Terima Kasih \n Untuk pertanyaan dan info lanjutan silahkan hubungi CS Sahimastore 081211704748
            bit.ly/cssahimastore";
            // echo $textWa;
            kirimWa($suc['telp'],$textWa);
            // exit;

            $cekpg = $this->config->item('status_payment_gateway');
            if ($cekpg == 'AKTIF') {

                if ($suc['courier'] != 'cod') {
                    $data['inv_xendit_url'] = $this->createInvoiceXendit($suc['invoice'], $suc['email'], $suc['totalall'], $suc['invoice']);
                }
            }
            
            $rek = $this->db->get('rekening');
            $data['title'] = 'Berhasil - ' . $this->config->item('app_name');
            $data['css'] = '';
            $data['invoice_id'] = $suc;
            $data['rekening'] = $rek;
            $courier = $this->input->post('paymentSelectKurir', true);
            $service1 = explode("-", $courier);
            $service2 = $service1[2];
            $data['metod'] = $service2;
            


            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar');
            $this->load->view('page/succesfully', $data);
            $this->load->view('templates/footer_notmpl');
        }else{
           
            $this->session->set_flashdata('failed', "<script>
            swal({
            text: 'Maaf terjadi kesalahan,pastikan keranjang belanja anda terisi lalu ulangi kembali',
            icon: 'error'
            });
            </script>");
            redirect(base_url() . 'payment');
        }
        
        

    }
    public function createInvoiceXendit($kodeorder, $email, $amount, $desc)
    {
        // $extId = $this->input->post("external_id");
		// $email = $this->input->post("payer_email");
		// $description = $this->input->post("description");
		// $amount = $this->input->post("amount");

		// https://github.com/xendit/xendit-php/blob/master/examples/InvoiceExample.php
		Xendit::setApiKey($this->config->item('xendit_key'));

		$params = [
			"external_id" => $kodeorder,
			"payer_email" => $email,
			"description" => $desc,
			"amount" => $amount,
            "should_send_email" => true,
            "invoice_duration" =>  259200,
            "reminder_time_unit" => 'days'
			// "success_redirect_url"=> base_url()."invoice/success",

			// "failure_redirect_url"=> base_url()."invoice/failure",
		];

		$invoice = \Xendit\Invoice::create($params);
        $data = [
            'xendit_invoice_id' => $invoice["id"],
            'external_id' => $invoice["external_id"],
            'status' => $invoice["status"],
            'amount' => $invoice["amount"],
            'invoice_url' => $invoice["invoice_url"],
            'payer_email' => $invoice["payer_email"],
            'created' => $invoice["created"],
            'updated' => $invoice["updated"],
        ];
        $qinsertInvoice = $this->db->insert('xendit_invoice', $data);
        // echo json_encode($invoice);
        // exit;
        if ($qinsertInvoice) {
            return ['inv_url' => $invoice["invoice_url"]];
        }else{
            return ['inv_url' => 'https://albirunimart.com'];
        }
       
		// header("Location: ".$invoice["invoice_url"]);	
    }
    public function giveVocuher()
    {
        $invo = $this->input->post('inv', true);
        $countInvoice = $this->db->where('user',$this->session->userdata('id'))->get('invoice')->num_rows();
        if ($countInvoice >= 4) {
            $checkVoucher = $this->db->get_where('voucher', ['dist' => 2])->row_array();
            if($checkVoucher){
                $data_voucher = [
                    'id_user' => $this->session->userdata('id'),
                    'id_voucher' => $checkVoucher['id'],
                    'status' => 0,
                    'createdAt' => date('Y-m-d H:i:s')
                ];
                $this->db->insert('voucher_user', $data_voucher);
            }
            $res = array(
                'log' => $this->db->last_query()
            );
            echo json_encode($res);
        }
        
    }
    public function confirmation(){
        $this->form_validation->set_rules('invoice', 'Invoice', 'required', ['required' => 'Order ID wajib diisi']);
	    if($this->form_validation->run() == false){
            $data['title'] = 'Bukti Pembayaran - ' . $this->config->item('app_name');
            $data['css'] = '';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar');
            $this->load->view('page/payment_proof');
            $this->load->view('templates/footer_notmpl');
        }else{
            $invoice = $this->input->post('invoice', true);
            $check = $this->db->get_where('invoice', ['invoice_code' => $invoice, 'status' => 0])->row_array();
            if($check){
                $data = array();
                $upload = $this->Order_model->uploadFile();

                if($upload['result'] == 'success'){
                    $this->Order_model->insertPaymentConfirmation($upload);
                    
                    $telp = $check['telp'];
                    $nama = $check['name'];
                    $textWa = "Dear".$nama.""."\xA"."
                    Segera kami konfirmasi atas info pembayaran pada jam kerja aktif."."\xA"."
                    Terimakasih."."\xA"."
                    ";   
                    kirimWa($telp,$textWa);

                    $this->session->set_flashdata('success', "<script>
                        swal({
                        text: 'Bukti pembayaran berhasil dikirim',
                        icon: 'success'
                        });
                        </script>");
                        redirect(base_url() . 'profile/transaction/'.$invoice);
                }else{
                    $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Gagal mengirim bukti pembayaran, pastikan file berukuran maksimal 2mb dan berformat png, jpg, jpeg, atau pdf. Silakan ulangi lagi.
                    </div>");
                    redirect(base_url() . 'payment/confirmation');
                }
            }else{
                $this->session->set_flashdata('failed', "<div class='alert alert-danger' role='alert'>
                    Order ID yang dimasukan tidak ada atau sudah terkonfirmasi!
                    </div>");
                    redirect(base_url() . 'payment/confirmation');
            }
        }
    }

}
