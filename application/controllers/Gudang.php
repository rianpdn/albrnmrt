<?php
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Categories_model');
        $this->load->model('Payment_model');
        $this->load->model('Settings_model');
        $this->load->model('Order_model');
        $this->load->model('User_model');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
    }

    public function index(){
       echo "untuk gudang";
    }
    function askForPwd()
    {
        header('WWW-Authenticate: Basic realm="My Realm"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Ups , batal';
        exit;
    }
    public function scan()
    {
            $data['gudang'] = 'gudang' ;
            $this->load->view('administrator/gudang', $data);
       
    }
    public function proses_cari()
    {
        $kode = $this->input->post('kode');
        $qgetData = $this->db->where('invoice_code',$kode)
                    ->get('invoice');
        if ($qgetData->num_rows() != 0) {
            $data = $qgetData->row_array();
           $res = array('status' => true,
                            'kode' => $data['invoice_code'],
                            'nama' => $data['name'],
                            'total_all' => number_format($data['total_all'],0,",","."),
                            'tgl_pesan' => $data['date_input'],
                            'id' => $data['id']);
        }else{
            $res = array('status' => false);
        }
        echo json_encode($res);

    }
    public function proses_scan()
    {
        $id = $this->input->post('id');
        $buyer = $this->db->get_where('invoice', ['id' => $id])->row_array();
        if ($buyer['status'] == 1) {
            $this->db->set('status', 2);
            $this->db->where('id', $id);
            $this->db->update('invoice');
            $transaction = $this->db->get_where('transaction', ['id_invoice' => $buyer['invoice_code']]);
            foreach($transaction->result_array() as $t){
                $this->db->set('transaction', 'transaction+'.$t['qty'].'', FALSE);
                $this->db->set('stock', 'stock-'.$t['qty'].'', FALSE);
                $this->db->where('slug', $t['slug']);
                $this->db->update('products');
            }
            $data_log = array('nama_petugas' => 'gudang1',
                                'invoice' => $buyer['invoice_code'],
                                'status_scan' => 'Berhasil men scan pesanan',
                                'createdAt' => date('Y-m-d H:i:s')
                                );
            $this->db->insert('log_gudang_scan',$data_log);
            $res = array('status' => true);
        }else{
            $data_log = array('nama_petugas' => 'gudang1',
            'invoice' => $buyer['invoice_code'],
            'status_scan' => 'Gagal men scan pesanan',
            'createdAt' => date('Y-m-d H:i:s')
            );
            $this->db->insert('log_gudang_scan',$data_log);
            $res = array('status' => false);
        }

       
        echo json_encode($res);
    }
}