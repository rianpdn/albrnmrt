<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Visitor
{
    protected $CI;
 
    public function __construct()
    {
        $this->CI =& get_instance();
    }
    // visitor
    public function counter($address_visitor)
    {
        date_default_timezone_set('Asia/Jakarta');
        if($this->CI->uri->segment(1) == "dapur" || $this->CI->uri->segment(1) == "assets" || $this->CI->uri->segment(1) == "vendor") {
 
        }else{
           
            if ($this->CI->agent->is_browser())
            {
                    $agent = $this->CI->agent->browser();
            }
            elseif ($this->CI->agent->is_robot())
            {
                    $agent = $this->CI->agent->robot();
            }
            elseif ($this->CI->agent->is_mobile())
            {
                    $agent = $this->CI->agent->mobile();
            }
            else
            {
                    $agent = 'Unidentified User Agent';
            }
            $ip = $this->CI->input->ip_address();
            $getDup = $this->CI->db->where('ip_address',$ip)
                                ->where('url',$address_visitor)
                                ->where('date',date('Y-m-d'))->get('visitor')->num_rows();
            if ($getDup < 1) {
                $visitor_data = array(  'url'       => $address_visitor,
                    'ip_address'    => $this->CI->input->ip_address(),
                    'user_agents' => $agent,
                    'date'          => date('Y-m-d'),
                    'time'          => date('Y-m-d H:i:s')
                    );
                $this->CI->Visitor_model->tambah($visitor_data);
            }
          
        }
    }
 
}
 
/* End of file Visitor.php */
/* Location: ./application/libraries/Visitor.php */
 