<style>
.media .media-object { max-width: 120px; }
.media-body { position: relative; }
.media-date { 
    position: absolute; 
    right: 25px;
    top: 25px;
}
.media-date li { padding: 0; }
.media-date li:first-child:before { content: ''; }
.media-date li:before { 
    content: '.'; 
    margin-left: -2px; 
    margin-right: 2px;
}
.media-comment { margin-bottom: 20px; }
.media-replied { margin: 0 0 20px 50px; }
.media-replied .media-heading { padding-left: 6px; }
.checked {
  color: orange;
}
</style>
<div class="wrapper">
    <div class="navigation">
        <a href="<?= base_url(); ?>">Home</a>
        <i class="fa fa-caret-right"></i>
        <a href="<?= base_url(); ?>c/<?= $product['slug']; ?>"><?= $product['name']; ?></a>
        <i class="fa fa-caret-right"></i>
        <a><?= $product['title']; ?></a>
    </div>
    <?php $setting = $this->db->get('settings')->row_array(); ?>
    <div class="top">
        <div class="main-top">
            <div class="img">
                
                <a href="<?= base_url(); ?>assets/images/product/<?= $product['img']; ?>" data-lightbox="img-1">
                    <img src="<?= base_url(); ?>assets/images/product/<?= $product['img']; ?>" alt="produk" class="jumbo-thumb">
                </a>
                <div class="img-slider">
                    <img src="<?= base_url(); ?>assets/images/product/<?= $product['img']; ?>" alt="gambar" class="thumb">
                    <?php foreach($img->result_array() as $d): ?>
                        <img src="<?= base_url(); ?>assets/images/product/<?= $d['img']; ?>" alt="gambar" class="thumb">
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="ket">
                <h1 class="title"><?= $product['title']; ?></h1>
                <p class="subtitle">Terjual <?=$product['transaction'];?> Produk &bull; <?=$product['viewer'];?>x Dilihat</p>
                <hr>
                <table>
                    <?php  if($setting['flashsale'] == 1) { ?>
                        <?php if($product['flashsale_price'] == 0){ ?>
                        
                            <tr>
                                <td class="t">Harga</td>
                                <td class="oldPrice">Rp <?= str_replace(",",".",number_format($product['price'])); ?></td>
                            </tr>
                            <tr class="newPrice">
                                <td></td>
                                <td class="price">Rp <?= str_replace(",",".",number_format($product['promo_price'])); ?><span class="notify-badge" style="position:relative !important;"><span class="label label-danger"><?=100-$product['diskon'].'% OFF';?></span></span></td>
                            </tr>
                        <?php }else{ ?>
                            <tr>
                                <td class="t">Harga</td>
                                <td class="oldPrice">Rp <?= str_replace(",",".",number_format($product['price'])); ?></td>
                            </tr>
                            <tr class="newPrice">
                                <td></td>
                                <td class="price">Rp <?= str_replace(",",".",number_format($product['flashsale_price'])); ?><span class="notify-badge" style="position:relative !important;"><span class="label label-danger"><?=100-$product['diskon'].'% OFF';?></span></span></td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if($setting['promo'] == 1){ ?>
                            <?php if($product['promo_price'] == 0){ ?>
                                <tr>
                                    <td class="t">Harga</td>
                                    <td class="price">Rp <?= str_replace(",",".",number_format($product['price'])); ?></td>
                                </tr>
                            <?php }else{ ?>
                                <tr>
                                    <td class="t">Harga</td>
                                    <td class="oldPrice">Rp <?= str_replace(",",".",number_format($product['price'])); ?></td>
                                </tr>
                                <tr class="newPrice">
                                    <td></td>
                                    <td class="price">Rp <?= str_replace(",",".",number_format($product['promo_price'])); ?><span class="notify-badge" style="position:relative !important;"><span class="label label-danger"><?=100-$product['diskon'].'% OFF';?></span></span></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td class="t">Kondisi</td>
                        <?php if($product['condit'] == 1){ ?>
                            <td>Baru</td>
                        <?php }else{ ?>
                            <td>Bekas</td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td class="t">Berat</td>
                        <td><?= $product['weight']; ?> gram</td>
                    </tr>
                    <tr>
                        <td class="t">Stok</td>
                        <td><?= $product['stock']; ?> produk</td>
                    </tr>
                    <tr>
                        <td class="t">Penerbit</td>
                        <td><?= $product['penerbit']; ?></td>
                    </tr>
                    <tr>
                        <td class="t">Ukuran</td>
                        <td><?= $product['ukuran']; ?></td>
                    </tr>
                    <tr>
                        <td class="t">Jumlah Halaman</td>
                        <td><?= $product['jml_halaman']; ?> Halaman</td>
                    </tr>
                    <tr>
                        <td class="t">ISBN</td>
                        <td><?= $product['isbn']; ?></td>
                    </tr>
                    <tr>
                        <?php 
                            if($setting['flashsale'] == 1) {
                                if($product['flashsale_price'] == 0){
                                        $priceP = $product['price'];  
                                }else{
                                    $priceP = $product['flashsale_price'];
                                }
                            }else{
                                if($setting['promo'] == 1){ 
                                    if($product['promo_price'] == 0){ 
                                        $priceP = $product['price']; 
                                    }else{ 
                                        $priceP = $product['promo_price']; 
                                    } 
                                }else{ 
                                    $priceP = $product['price']; 
                                } 
                            }
                        ?>
                        <td class="t">Jumlah</td>
                        <td>
                            <button onclick="minusProduct(<?= $priceP; ?>)">-</button><!--
                        --><input disabled type="text" value="1" id="qtyProduct" class="valueJml"><!--
                        --><button onclick="plusProduct(<?= $priceP; ?>, <?= $product['stock']; ?>)">+</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="t">Total</td>
                        <td>Rp <span id="detailTotalPrice"><?= str_replace(",",".",number_format($priceP)); ?></span></td>
                    </tr>
                    <?php
                    
                        $getGift = $this->Products_model->getGiftByIdProduct($product['productId']);
                        // echo json_encode($getGift->result());
                        if ($getGift->num_rows() != 0) {
                    ?>
                    <tr>
                        <td class="t">Pilih Hadiahmu</td>
                        
                        <td>
                    <?php
                            foreach ($getGift->result() as $gift) {
                    ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gift" id="gift" value="<?=$gift->title_gift;?>">
                        <label class="form-check-label" for="gift">
                        <?=$gift->title_gift;?> <img style="width: 50px" src="<?= base_url(); ?>assets/images/product_hadiah/<?= $gift->img_gift; ?>">
                        </label>
                    </div>
                    <?php
                            }
                    ?>
                    </td> 
                    </tr>
                    <?php
                        }
                    ?>
                </table>
                <hr>
                <?php
                    if ($product['stock'] != 0) {
                ?>
                <button class="btn btn-warning pl-5 pr-5" onclick="buy()">Beli</button>
                <button class="btn btn-primary" onclick="addCart()">Tambah ke Keranjang</button>
                <?php
                    }else{
                ?>
                 <button class="btn btn-warning" >Stok habis</button>
                <?php } ?>
            </div>
        </div>
    </div>
    <hr>
    
    <div class="description">
        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist"  data-tabs="tabs">
            <li class="nav-item" role="presentation">
                <a href="#home" class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Deskripsi</a>
            </li>
            <li class="nav-item" role="presentation">
                <a href="#ulasan" class="nav-link" id="ulasan" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Ulasan</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"></div>
            <div class="tab-pane fade" id="ulasan" role="tabpanel" aria-labelledby="ulasan">kkkkk</div>
        </div>
         -->
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
            <li class="nav-item active"><a href="#red" class="nav-link active" data-toggle="tab">Deksripsi</a></li>
            <li ckass="nav-item"><a href="#orange" class="nav-link" data-toggle="tab">Ulasan</a></li>
        </ul>
        <div id="my-tab-content" class="tab-content" style="margin-top:20px; padding-left: 10px;">
            <div class="tab-pane active" id="red">
                <!-- <h5>Deskripsi</h5> -->
                <p><?= nl2br($product['description']); ?></p>
            </div>
            <div class="tab-pane" id="orange">
                <ul class="media-list">
                    <?php
                        if ($ulasan->num_rows() != 0) {
                            foreach ($ulasan->result() as $u) {
                                # code...
                            }
                    ?>
                    <li class="media">
                        <div class="media-body">
                          <div class="well well-lg">
                              <h4 class="media-heading text-uppercase reviews"><?=$u->name;?></h4>
                              <div style="float:right;">
                              <?php for ($i=0; $i < $u->bintang; $i++) { 
                              ?>
                              <span class="fa fa-star checked"></span>
                               <?php } ?>
                               </div>
                              <p class="media-comment">
                              <?=$u->ulasan;?>
                              </p>
                          </div>              
                        </div>
                    </li>

                    <?php } ?>
                </ul>
            </div>
            
        </div>

    </div>
    <hr>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
    function plusProduct(price, stock){
        let inputJml;
        inputJml = parseInt($("input.valueJml").val());
        inputJml = inputJml + 1;
        if(inputJml <= stock){
            $.ajax({
                url: `<?= base_url(); ?>products/getgrosir?product=<?= $product['productId']; ?>&stock=${inputJml}`,
                type: "get",
                async: true,
                success: function(response){
                    if(response){
                        $("input.valueJml").val(inputJml);
                        $("td.price").html("Rp " + number_format(response).split(",").join(".") + " <small style='font-size: 13px; font-weight: normal' class='badge badge-info'>grosir</small>");
                        const newPrice = inputJml * response;
                        const rpFormat = number_format(newPrice);
                        $("#detailTotalPrice").text(rpFormat.split(",").join("."));
                    }else{
                        $("input.valueJml").val(inputJml);
                        $("td.price").html("Rp " + number_format(price).split(",").join("."));
                        const newPrice = inputJml * price;
                        const rpFormat = number_format(newPrice);
                        $("#detailTotalPrice").text(rpFormat.split(",").join("."));
                    }
                }
            })
        }
    }

    function minusProduct(price){
        let inputJml;
        inputJml = parseInt($("input.valueJml").val());
        inputJml = inputJml - 1;
        if(inputJml >= 1){
            $.ajax({
                url: `<?= base_url(); ?>products/getgrosir?product=<?= $product['productId']; ?>&stock=${inputJml}`,
                type: "get",
                async: true,
                success: function(response){
                    if(response){
                        $("input.valueJml").val(inputJml);
                        $("td.price").html("Rp " + number_format(response).split(",").join(".") + " <small style='font-size: 13px; font-weight: normal' class='badge badge-info'>grosir</small>");
                        const newPrice = inputJml * response;
                        const rpFormat = number_format(newPrice);
                        $("#detailTotalPrice").text(rpFormat.split(",").join("."));
                    }else{
                        $("input.valueJml").val(inputJml);
                        $("td.price").html("Rp " + number_format(price).split(",").join("."));
                        const newPrice = inputJml * price;
                        const rpFormat = number_format(newPrice);
                        $("#detailTotalPrice").text(rpFormat.split(",").join("."));
                    }
                }
            })
        }
    }

    function number_format (number, decimals, decPoint, thousandsSep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
        var n = !isFinite(+number) ? 0 : +number
        var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
        var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
        var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
        var s = ''

        var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
        }

        // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
        if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
        }
        if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
        }

        return s.join(dec)
    }

    function buy(){
        $.ajax({
            url: "<?= base_url(); ?>cart/add_to_cart",
            type: "post",
            data: {
                id: <?= $product['productId']; ?>,
                qty: $("#qtyProduct").val(),
                gift: $("#gift").val()
            },
            success: function(data){
                location.href = "<?= base_url(); ?>cart"
            }
        })
    }

    function addCart(){
        $.ajax({
            url: "<?= base_url(); ?>cart/add_to_cart",
            type: "post",
            data: {
                id: <?= $product['productId']; ?>,
                qty: $("#qtyProduct").val(),
                gift: $("#gift").val()
            },
            success: function(data){
                $(".navbar-cart-inform").html(`<i class="fa fa-shopping-cart"></i> Keranjang(<?= count($this->cart->contents()) + 1; ?>)`);
                swal({
                    title: "Berhasil Ditambah ke Keranjang",
                    text: `<?= $product['title']; ?>`,
                    icon: "success",
                    buttons: true,
                    buttons: ["Lanjut Belanja", "Lihat Keranjang"],
                    })
                    .then((cart) => {
                    if (cart) {
                        location.href = "<?= base_url(); ?>cart"
                    }
                });
            }
        })
    }

    // slider product
    const containerImgProduct = document.querySelector("div.wrapper div.top div.main-top div.img");
    const jumboImgProduct = document.querySelector("div.wrapper div.top div.main-top div.img img.jumbo-thumb");
    const jumboHrefImgProduct = document.querySelector("div.wrapper div.top div.main-top div.img a");
    const thumbsImgProduct = document.querySelectorAll("div.wrapper div.top div.main-top div.img div.img-slider img.thumb");
    
    containerImgProduct.addEventListener('click', function(e){
        if(e.target.className == 'thumb'){
            jumboImgProduct.src = e.target.src;
            jumboHrefImgProduct.href = e.target.src;
            
            thumbsImgProduct.forEach(function(thumb){
                thumb.className = 'thumb';
            })
        }
    })

</script>
