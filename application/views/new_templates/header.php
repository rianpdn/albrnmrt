
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta name="format-detection" content="telephone=no">
    <meta property="fb:app_id" content="123" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base />
    <base />
    <meta name="description" content="Albirunimart" />
    <link defer href="<?=base_url()?>vendor/img/logo.png" rel="icon" />

    <meta property="og:image" content="<?=base_url()?>vendor/img/logo.png" />
    <meta property="og:image" content="" />
    <meta property="og:image:width" content="256" />
    <meta property="og:image:height" content="256" />

    <meta property="twitter:image" content="<?=base_url()?>vendor/img/logo.png" />


    <!-- BULMA -->
    <link defer href="<?=base_url();?>vendor/bulma.css" rel="stylesheet" type="text/css" />
    <link defer href="<?=base_url();?>vendor/bulma-checkradio.min.css" rel="stylesheet" type="text/css" />


    <!-- MAIN STYLESHEET -->
    <link defer href="https://images.unicartapp.com/catalog/view/theme/aio/stylesheet/aio.css?ver=1616171573" rel="stylesheet" type="text/css" />

    <!-- CAROUSEL -->
    <link defer href="<?=base_url();?>vendor/slick/slick.css" rel="stylesheet" type="text/css" />
    <link defer href="<?=base_url();?>vendor/slick/slick-theme.css" rel="stylesheet" type="text/css" />

    <!-- FONT AWESOME -->
    <!-- <link defer rel="stylesheet" href="<?=base_url();?>vendor/fontawesome/font-awesome.min.css"> -->
    <link defer="" rel="stylesheet" href="https://images.unicartapp.com/catalog/view/theme/aio/stylesheet/fontawesome-5.6.3/css/all.min.css">
    <!-- MATERIAL DESIGN ICON -->
    <link defer rel="stylesheet" href="https://images.unicartapp.com/catalog/view/theme/aio/stylesheet/materialdesignicons-3.3.92/materialdesignicons.min.css">

    <!-- STAATLICHES -->
    <link defer rel="stylesheet" href="<?=base_url();?>vendor/staatliches/staatliches.css">

    <!-- ANIMATION -->
    <link defer href="<?=base_url();?>vendor/animate.css" rel="stylesheet" type="text/css" />

    <!-- BULMA CALENDAR -->
    <link defer href="<?=base_url();?>vendor/bulma-calendar.min.css" rel="stylesheet" type="text/css" />

    <!-- JQUERY -->
    <script src="<?=base_url();?>vendor/jquery-3.3.1.min.js"></script>
    <script defer type="text/javascript" src="<?=base_url();?>vendor/jquery.cookie.js"></script>

    <!-- TOTAL STORAGE -->
    <script defer type="text/javascript" src="<?=base_url();?>vendor/jquery.total-storage.min.js"></script>

    <!-- FANCYBOX -->
    <link defer rel="stylesheet" href="<?=base_url();?>vendor/fancybox/jquery.fancybox.min.css">
    <script async src="<?=base_url();?>vendor/fancybox/jquery.fancybox.min.js"></script>

    <!-- ELEVATEZOOM -->
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.easing.min.js"></script>
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.mousewheel.js"></script>
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.ez-plus.js"></script>

    <!-- LAZYLOAD -->
    <script src="<?=base_url();?>vendor/jquery.lazy.min.js"></script>
    <script src="<?=base_url();?>vendor/jquery.lazy.plugins.min.js?ver=1.0"></script>

    <!-- SWEETALERT -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- CUSTOMIZE -->
    <link defer id="customize_css" href="<?=base_url();?>vendor/aio_customize_css.css?ver=10022021" rel="stylesheet" type="text/css" />


    <!-- OVERWRITE CSS -->
    <link defer rel="stylesheet" type="text/css" href="<?=base_url();?>vendor/overwrite.css?ver=10022021" />

    <style>
        .btn-wishlist,
        .btn-compare {
            display: none;
        }
    </style>


    <!-- TITLE -->
    <title><?= $title ?></title>
</head>
  <?php
  $setting = $this->db->get('settings')->row_array();
  $dateNow = date('Y-m-d H:i');
  $dateDB = $setting['promo_time'];
  $dateDBflashsale = $setting['flashsale_time'];
  $dateDBNew = str_replace("T"," ",$dateDB);
  $dateDBNewFlashsale = str_replace("T"," ",$dateDBflashsale);
  if($dateNow >= $dateDBNew){
    $this->db->set('promo', 0);
    $this->db->update('settings');
  }
  if($dateNow >= $dateDBNewFlashsale){
    $this->db->set('flashsale', 0);
    $this->db->update('settings');
  }
  ?>
  
<body class="body-style wide  clamp-1">

    <div id="modal-search-product" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <form action="<?= base_url(); ?>search" method="get">
                <section class="modal-card-body">
                    <div class="search-input">
                        <input class="search-bar-input" name="q" type="text" placeholder="Search" v/>
                        <i id="icon-search" class="mdi mdi-magnify"></i>
                    </div>
                </section>
                <footer class="modal-card-foot">
                    <button type="button" class="search-cancel" onclick="closeModals();">Batal</button>
                    <button type="submit" id="btn-search">Cari</button>
                </footer>
            </form>
        </div>
    </div>

    <div id="modal-checkout-cartmodal"></div>



    <!-- ANNOUCEMENT BAR  -->
    <style>
        #announcement-bar-content {
            background-color: #efefef;
        }

        #announcement-bar-banner {
            color: #000000;
        }

        #announcement-bar-button {
            color: #ffffff !important;
            background-color: #f63636 !important;
            border-color: #f63636 !important;
        }
    </style>
<!--     <div id="announcement-bar-content">
        <div id="announcement-bar-banner">
            <span>Pre Order Embroidery Mask by The Angkasa (8-15 Feb)</span>
            <button id="announcement-bar-button" class="button" onclick="window.open('embroidery-mask-by-the-angkasa.html','_blank');">
				PRE ORDER					
            </button>

        </div>
    </div> -->
    <!-- END ANNOUCEMENT BAR  -->
    
    <div id="wrapper" class="clearfix">

        <!-- HEADER -->
        <div id="header" class="uni-head-1  ">
            <div class="container">
                <!-- BURGER - MAIN MENU -->
                <span id="burger-mainmenu" class="navbar-burger burger burger-mainmenu">
                    <span></span>
                <span></span>
                <span></span>
                </span>
