
        <!-- BACK TO TOP BUTTON -->
        <button onclick="backToTop()" id="backToTop" title="Back to top"><i class="fa fa-chevron-up"></i></button>
<footer id="footer" class="footer uni-foot-4">

            <!-- ADD TO CART POPUP  -->
            <input type="hidden" id="current_product_id" />
            <input type="hidden" id="config_cart_modal" value="1">
            <!-- YOU MIGHT NEED THIS APP -->
            <input type="hidden" id="you_might_need_this_status" value="0">

            <!-- FOOTER MODULES -->
            <div class="container">

                <div class="el_1">
                    <!-- FOOTER MENUS -->
                    <!-- END FOOTER MENU -->

                    <div id="newsletter_socialmedia" class="module-container">
                        <div id="newsletter">
                            <div class="title">Newsletter</div>

                            <head>
                                <script id="mcjs">
                                    // ! function(c, h, i, m, p) {
                                    //     m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
                                    // }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/6d694bd739302ba342afbde49/f85154678eafefaa12d0e0fd2.js");
                                </script>

                                <!-- Begin Mailchimp Signup Form -->
                                <link href="http://cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                                <style type="text/css">
                                    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                                </style>
                                <div id="mc_embed_signup">
                                    <form action="https://nukilanbiruni.us10.list-manage.com/subscribe/post?u=6d694bd739302ba342afbde49&amp;id=303f0d4008" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll">
                                            <h2>Subscribe</h2>
                                            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                            <div class="mc-field-group">
                                                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>
                                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                            </div>
                                            <div id="mce-responses" class="clear">
                                                <div class="response" id="mce-error-response" style="display:none"></div>
                                                <div class="response" id="mce-success-response" style="display:none"></div>
                                            </div>
                                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6d694bd739302ba342afbde49_303f0d4008" tabindex="-1" value=""></div>
                                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                        </div>
                                    </form>
                                </div>

                                <!--End mc_embed_signup-->
                        </div>

                        <!-- SOCIAL MEDIA -->
                        <div id="socialmedia">
                            <div class="title">
                                Follow Us </div>

                            <div class="icons-social-media">
                                <a id="footer-social-fb" class="icon" href="javascript:void(0);" style="display:none;" target="_blank">
                                    <div class="mdi mdi-facebook"></div>
                                </a>
                                <a id="footer-social-youtube" class="icon" href="javascript:void(0);" style="display:none;" target="_blank">
                                    <div class="mdi mdi-youtube"></div>
                                </a>
                                <a id="footer-social-instagram" class="icon" href="https://www.instagram.com/albiruni.mart" target="_blank">
                                    <div class="mdi mdi-instagram"></div>
                                </a>
                            </div>
                        </div>
                        <!-- END SOCIAL MEDIA -->
                    </div>


                    <!-- PAGE CONTENT FOOTER  -->
                    <div id="payment_method" class="module-container">

                        <div class="title">
                        </div>
                        <div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT FOOTER  -->
                </div>

                <div class="el_2">
                    <div id="el_2_logo">
                    </div>
                    <div class="el_2_content">
                    </div>
                </div>

                <div class="el_3">
                    <div id="additional_cms" class="module-container">
                        <div class="title"></div>
                    </div>
                </div>
            </div>



            <!-- COPYRIGHTS INFO -->
            <div id="copyrights">
                <div class="container">
                    <!-- <div class="powered_left">
                        Copyright  © 2021  Albiruni Mart. All rights reserved.                    </div>
                    <div class="powered_right">
                        <a target="_top" href="https://www.sitegiant.my">eCommerce by SiteGiant</a>                    </div> -->

                    <div class="powered">
                        <span class="copyright">Copyright  © 2021  Albiruni Mart. All rights reserved.</span><span style="float:right;" class="powered"></span> </div>
                </div>
            </div>
        </footer>



        <!-- ==== SCRIPTS ==== -->
        <!-- JQUERY -->
        <script defer type="text/javascript" src="<?=base_url()?>vendor/jquery-ui-1.12.1.min.js"></script>
        <!-- CAROUSEL -->
        <script defer src="<?=base_url()?>vendor/slick.min.js"></script>
        <!-- CLIPBOARD -->
        <script defer src="<?=base_url()?>vendor/clipboard.min.js"></script>
        <!-- FUNCTIONS -->
        <script defer type="text/javascript" src="<?=base_url()?>vendor/notifyme.js"></script>
        <!-- SWEET ALERT -->
        <script defer src="<?=base_url()?>vendor/sweetalert2.all.min.js"></script>
        <!-- PRINTTHIS -->
        <script defer src="<?=base_url()?>vendor/printThis.js"></script>
        <!-- FLATPICKR -->
        <link defer rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <script defer src="<?=base_url()?>vendor/flatpickr.js"></script>

        <!-- BULMA CALENDAR -->
        <script defer src="<?=base_url()?>vendor/doc.js"></script>
        <script defer src="<?=base_url()?>vendor/bulma-calendar.min.js"></script>
        <!-- <script defer src="https://images.unicartapp.com/catalog/view/theme/aio/plugins/bulma-calendar/main.js"></script> -->

        <!-- UI CONTROL SCRIPT -->
        <script defer src="<?=base_url()?>vendor/ui-control.js?ver=10022021"></script>

        <!-- LAZYLOAD -->
        <script>
            function lazyload() {
                $('img.lazy').lazy({
                    attribute: 'data-lazy',
                    effect: "fadeIn",
                    effectTime: 2000
                });
            }
        </script>
        <!-- LOAD USER SETTINGS -->
        <script>
            $(document).ready(function() {
                lazyload();


                productslick(3, 3, 4);

                search_autocomplete();

                multitabs(3, 3, 4, true);
                // END PRODUCT EFFECT FADE
                productfade();
            });
        </script>

</body>
</html>