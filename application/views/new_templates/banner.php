        <?php $banner = $this->Settings_model->getBanner(); ?>
        <!-- END HEADER -->
        <section id="home" class="section content container per-row-3">
            <div id="contents" class="home">
                <div id="main-content">
                    <div class="holder">
                        <!-- ###AIO### -->
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-banner {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-banner .carousel {
                                    padding-top: 0px;
                                    padding-bottom: 0px;
                                }

                                .desktop .home #module-banner .carousel {
                                    max-width: 100%;
                                    margin: 0 auto;
                                }
                            </style>

                            <div id="module-banner">
                                <div class="carousel">
                                    <?php
                                    foreach ($banner->result_array() as $key => $value) {
                                    ?>
                                    <a href="<?=$value['url'];?>">
                                        <div>
                                            <img src="<?=base_url()?>assets/images/banner/<?=$value['img'];?>" alt="" title="" />
                                        </div>
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>

                            <script>
                                function bannerSettings() {
                                    $("#module-banner .carousel").not('.slick-initialized').slick({
                                        dots: false,
                                        infinite: true,
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        fade: true,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        cssEase: 'linear'
                                    });
                                }

                                $(document).ready(function() {
                                    bannerSettings();
                                });
                            </script>
                        </div>