<?php if($this->session->userdata('login')){ ?>
  <?php
  $user = $this->db->get_where('user', ['id' => $this->session->userdata('id')])->row_array();
  $cart = $this->db->get_where('cart', ['user' => $this->session->userdata('id')]);
  $order = $this->db->get_where('invoice', ['user' => $this->session->userdata('id'), 'status !=' => 4]);
  ?>
<?php } ?>
                <div class="el_1">
                    <div class="holder">
                        <div id="logo" class="">
                           <a href="<?=base_url();?>">
                                <img class="header-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                                <img class="mobile-header-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                                <img class="sticky-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="el_2">
                    <div class="holder">
                        <!-- NAVIGATION BAR -->
                        <div id="navi-bar" class="navbar-menu">
                            <div class="navbar">
                                <a class="navbar-item" href="<?=base_url();?>"><span>Home</span></a>
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#"><span>Kategori</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                        <?php $categories = $this->Categories_model->getAllCategories(); ?>
                                        <?php foreach($categories->result_array() as $cat){
                                            if ($cat['parent'] == 0) {
                                        ?>
                                        <div class="sub-menu-dropdown navbar-item is-hoverable">
                                            <a class="navbar-link is-arrowless" href="<?=base_url(); ?>c/<?= $cat['slug']; ?>"><span><?= $cat['name']; ?></span></a>
                                            <?php
                                                $getSub = $this->db->where('parent',$cat['id'])->get('categories');

                                                if ($getSub->num_rows() > 0) {

                                            ?>
                                            <i class="accordion"></i>

                                            <?php } ?>
                                            <div class="navbar-dropdown">
                                                <?php
                                                    
                                                    foreach ($getSub->result_array() as $sub) {
                                                ?>
                                                <a class="navbar-item" href="<?=base_url(); ?>c/<?= $sub['slug']; ?>"><span><?= $sub['name']; ?></span></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php 
                                            } 
                                            // else {
                                        ?>
                                            <!-- <a class="navbar-item" href="<?=base_url(); ?>c/<?= $cat['slug']; ?>"><span><?= $cat['name']; ?></span></a> -->
                                        <?php 
                                            // }
                                            } 
                                        ?>
                                    </div>
                                </div>
                                <a class="navbar-item" href="<?= base_url(); ?>about"><span>About Us</span></a>

                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="javascript:void(0)"><span>Customer Service</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                    <?php
                                        $page = $this->Settings_model->getFooterInfo();
                                         foreach($page->result_array() as $f): 
                                    ?>
                                        <a class="navbar-item" href="<?= base_url(); ?><?= $f['slug']; ?>"><span><?= $f['title']; ?></span></a>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                                 <?php if(!$this->session->userdata('login')){ ?>
                                    <a class="navbar-item" href="<?= base_url(); ?>login"><span>My Account</span></a>
                                 <?php }else{ ?>
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#"><span>My Account</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                            <a class="navbar-item" href="<?=base_url(); ?>cart"><span>
                                            <?php if($cart->num_rows() > 0){ ?>
                                                Keranjang(<?= $cart->num_rows(); ?>)
                                              <?php }else{ ?>
                                                Keranjang
                                              <?php } ?></span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>profile/transaction"><span>Transaksi</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>profile/histories"><span>Riwayat Transaksi</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>payment/confirmation"><span>Konfirmasi Pembayaran</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>logout"><span>Logout</span></a>
                                    </div>
                                </div>
                                 <?php } ?>
                                
                                 <?php 
                                 $setting = $this->db->get('settings')->row_array();
                                 if($setting['flashsale'] == 1){ ?>
                                    <a class="navbar-item" href="<?= base_url(); ?>flashsale">Flashsale</a>
                                 <?php } ?>

                            </div>
                        </div>
                        <!-- END NAVIGATION BAR -->
                    </div>
                </div>

                <!-- SEARCH -->
                <div id="search-toggle" style="color: rgb(255 255 255) !important;">
                    <div class="search-bar-container">
                        <i class="mdi mdi-magnify" onclick="search();" style="font-size:36px;"></i>
                        <div id="search-bar">
                            <input type="text" id="header_search" name="search" placeholder="Search" value="" oninput="$('#search').val($(this).val());">
                        </div>
                    </div>
                </div>

                <!-- SHOPPING CART -->
                <!-- ###AIO### -->
                <div id="shopping-cart" style="display:none;">
                    <div id="checkout-cart">
                        <span class="cart-info">0 item(s) Rp. 0.00</span>
                        <i id="clickme" class="mdi mdi-cart"></i>
                    </div>

                    <div class="checkout-overlay"></div>

                    <div class="shopping-cart-frame">
                        <div class="title">
                            <span>Cart</span>
                            <i id="btn-close-cart" onclick="closeSide()" class="mdi mdi-close"></i>
                        </div>
                        <div class="empty-cart">
                            <div>
                                <i class="icon-empty-cart"></i>
                            </div>
                            <div>
                                Your shopping cart is empty! </div>
                        </div>
                    </div>

                    <div class="count-frame">
                    </div>

                    <script>
                        $(document).ready(function() {});

                        function changeQuantity(own) {
                            minValue = parseInt($(own).attr('min'));
                            maxValue = parseInt($(own).attr('max'));
                            valueCurrent = parseInt($(own).val());

                            name = $(own).attr('name');
                            if (valueCurrent >= minValue) {
                                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled');
                            } else {
                                alert('');
                                if ($(own)[0].defaultValue < minValue) {
                                    $(own).val(minValue).change();
                                } else {
                                    $(own).val($(own)[0].defaultValue).change();
                                }
                            }
                            if (valueCurrent <= maxValue) {
                                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled');
                            } else {
                                alert('');
                                if ($(own)[0].defaultValue > maxValue) {
                                    $(own).val(maxValue).change();
                                } else {
                                    $(own).val($(own)[0].defaultValue).change();
                                }
                            }

                            if (valueCurrent >= minValue && valueCurrent <= maxValue) {
                                submitQuantity('#my-side-cart');
                            }
                        }

                        function updateQuantity(own) {

                            fieldName = $(own).attr('data-field');
                            type = $(own).attr('data-type');
                            var input = $("input[name='" + fieldName + "']");
                            var currentVal = parseInt(input.val());

                            if (!isNaN(currentVal)) {
                                if (type == 'minus') {

                                    if (currentVal > input.attr('min')) {
                                        input.val(currentVal - 1).change();
                                    }
                                    if (parseInt(input.val()) == input.attr('min')) {
                                        $(this).attr('disabled', true);
                                    }

                                } else if (type == 'plus') {

                                    if (currentVal < input.attr('max')) {
                                        input.val(currentVal + 1).change();
                                    }
                                    if (parseInt(input.val()) == input.attr('max')) {
                                        $(this).attr('disabled', true);
                                    }

                                }

                                submitQuantity('#my-side-cart');

                            } else {
                                input.val(0);
                            }
                        }

                        function applyCoupon() {
                            submitQuantity('#coupon-form');
                        }

                        function removeCoupon() {
                            var r = confirm('Are you sure to remove the coupon?');
                            if (r == true) {
                                $.ajax({
                                    url: 'index.php?route=checkout/cart/remove_coup_session',
                                    data: '',
                                    dataType: 'json',
                                    success: function(json) {
                                        updateCart();
                                    }
                                });
                            }
                        }

                        function applyVoucher() {
                            submitQuantity('#voucher-form');
                        }

                        function removeVoucher() {
                            var r = confirm('Are you sure to remove the voucher?');
                            if (r == true) {
                                $.ajax({
                                    url: 'index.php?route=checkout/cart/remove_vouc_session',
                                    data: '',
                                    dataType: 'json',
                                    success: function(json) {
                                        updateCart();
                                    }
                                });
                            }
                        }

                        function submitQuantity(formID) {
                            var form = $(formID).serialize();
                            $.ajax({
                                type: 'post',
                                url: 'index.php?route=module/cart/update',
                                data: form,
                                dataType: 'json',
                                success: function(json) {
                                    // console.log(json);
                                    if (json['type'] == 'quantity') {
                                        $('#shopping-cart > .count-frame').html('<span class="shopping-cart-count">' + json['total'] + '</span>');
                                        $('#checkout-cart').html('<span class="cart-info">' + json['total2'] + '</span><i class="mdi mdi-cart"></i>');
                                    }
                                    updateCart();
                                }
                            });
                        }
                    </script>

                </div>
                <!-- ACCOUNT -->
                <!-- <div id="myaccount">
                    <div class="dropdown is-right is-hoverable">
                        <div class="dropdown-trigger">
                            <a class="account-dropdown" aria-haspopup="true">
                                <i class="mdi mdi-account"></i>
                            </a>
                        </div>
                        <div class="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="login.html">Login</a>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>

        <!-- NOTIFICATION -->
        <div id="notification" class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                    <button class="delete" aria-label="close"></button>
                </header>
                <section class="modal-card-body">
                    <div class="notification-info"></div>
                </section>
                <footer class="modal-card-foot">
                    <div class="buttons">
                        <button type="button" class="button" onclick="closeModals();">Ok</button>
                    </div>
                </footer>
            </div>
        </div>







        <div><a href="https://web.whatsapp.com/send?phone=081389164293&amp;text=Hi!%20Enquiring%20from%20www.albirunimart.com&amp;source&amp;data" target="_blank"><i class="icon-whatsapp float_whatsapp" ></i></a></div>

