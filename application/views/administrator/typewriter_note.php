<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">

	<?php
         $getBotSec = $this->db->get('home_setting')->row()->bottom_title_section;
         echo $getBotSec;
    ?>
	</h1>
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		
	    <div class="row">
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Data </p>
	                </div>
	                <div class="card-body"> 
                    <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($getTypewriter->num_rows() > 0){ ?>
				<div class="table-responsive">
					<table
						class="table table-bordered"
						id="dataTable"
						width="100%"
						cellspacing="0">
						<thead>
							<tr>
								<th style="width: 10px">#</th>
								<th>Gambar</th>
								<th>Judul</th>
								<th style="width: 10px">Aksi</th>
							</tr>
						</thead>
						<tbody class="data-content">
							<?php $no =1; ?>
							<?php foreach($getTypewriter->result_array() as $data): ?>
							<tr>
								<td><?= $no ?></td>
								<td><img style="width: 50px" src="<?= base_url(); ?>assets/images/typewriter_note/<?= $data['img']; ?>"></td>
								<td><?= $data['title']; ?></td>
								<td>
									<a href="<?= base_url() ;?>administrator/delete_typewriter/<?= $data['id']; ?>" onclick="return confirm('Yakin ingin menghapus typewriter ini?')" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
								</td>
							</tr>
							<?php $no++ ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php }else{ ?>
				<div class="alert alert-warning" role="alert">
					Opss, Typewriter Note masih kosong, yuk tambah sekarang.
				</div>
				<?php } ?>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Tambah Data</p>
	                </div>
	                <div class="card-body">
	                    <?php echo $this->session->flashdata('failed'); ?>
	                    <form action="<?= base_url(); ?>administrator/setting/typewriter_note" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
                                <label>Judul <?=$getBotSec;?></label>
                                <input type="text" class="form-control" name="title" id="title">
                                
	                        </div>
	                        <div class="form-group">
                                <label>Gambar <?=$getBotSec;?></label>
	                            <input type="file" name="img" id="file" class="form-control" required>
	                        </div>
	                        <input type="hidden" name="help" value="1">
	                        <button class="btn btn-sm btn-info" type="submit">Proses</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- /.container-fluid -->
