<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Data Sub Kategori</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<a
				href="#"
				class="btn btn-primary"
				data-toggle="modal"
				data-target="#addCategory"
				>Tambah Sub Kategori</a
			>
		</div>
		<div class="card-body">
            <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($getSubCategories->num_rows() > 0){ ?>
			<div class="table-responsive">
				<table
					class="table table-bordered"
					id="dataTable"
					width="100%"
					cellspacing="0"
				>
					<thead>
						<tr>
							<th>#</th>
                            <th>Kategori Utama</th>
							<th>Nama Sub Kategori</th>
                            <th>Slug</th>
                            <th>Aksi</th>
						</tr>
					</thead>
					<tfoot></tfoot>
					<tbody class="data-content">
                        <?php $no = 1 ?>
						<?php foreach($getSubCategories->result_array() as $data): ?>
						<tr>
                            <td><?= $no ?></td>
                            <td><?= $data['name_parent']; ?></td>
                            <td><?= $data['sub_name']; ?></td>
                            <td><?= $data['sub_slug']; ?></td>
                            <td>
                                <a href="<?= base_url() ;?>administrator/category/<?= $data['id_sub']; ?>" class="btn btn-sm btn-info"><i class="fa fa-pen"></i></a>
                                <a href="<?= base_url() ;?>administrator/deleteCategory/<?= $data['id_sub']; ?>" onclick="return confirm('Yakin ingin menghapus Sub kategori? Semua produk dengan Sub kategori ini akan ikut terhapus')" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php }else{ ?>
			<div class="alert alert-warning" role="alert">
				Opss,Sub kategori masih kosong, yuk tambah kategori sekarang.
			</div>
            <?php } ?>
		</div>
	</div>
</div>
<!-- /.container-fluid -->

<!-- Modal Add Category -->
<div
	class="modal fade"
	id="addCategory"
	tabindex="-1"
	role="dialog"
	aria-labelledby="exampleModalLabel"
	aria-hidden="true"
>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-label="Close"
				>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form
					action="<?= base_url(); ?>administrator/sub_categories"
					method="post"
					enctype="multipart/form-data">
					<div class="form-group">
						<label for="name">Pilih Kategori Utama</label>
						<select class="form-control" name="id_parent_cat" id="id_parent_cat">
                            <option disabled selected>Silahkan Pilih Kategori Utama</option>
                            <?php foreach($getCategories->result_array() as $datas): ?>
                                <option value="<?=$datas['id'];?>"><?=$datas['name'];?></option>

                            <?php endforeach; ?> 
                        </select>
					</div>
					<div class="form-group">
						<label for="name">Nama Sub Kategori</label>
						<input
							type="text"
							class="form-control"
							id="name"
							name="name"
							autocomplete="off"
							required
						/>
					</div>
					<button type="submit" class="btn btn-primary" id="btnAddCategory">
						Tambahkan
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
