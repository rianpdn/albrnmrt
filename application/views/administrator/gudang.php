<!DOCTYPE html>
<html>
<head>
 <title>Gudang Scan Penjualan Buku</title>
</head>
<body>
 <div align="center">
  <h2>Scan Barcode</h2>
   <input type="text" id="kode">
   <button type="buttons" id="submit">Submit</button>
   <button type="buttons" id="reset">Reset</button>
  <br>
  <hr>
  <table> 
        <tr>
            <th>Kode<th>
            <th>Nama<th>
            <th>Total Pesanan<th>
            <th>Tanggal Pesan<th>
            <th>Aksi</th>
        </tr>
        <tr id="result_scan">
            <td class="kode"><td>
            <td class="nama"><td>
            <td class="total"><td>
            <td class="tgl"><td>
            <td class="aksi"></td>
        </tr>
  </table>
 </div>
 
    <!-- JQUERY -->
    <script src="<?=base_url();?>vendor/jquery-3.3.1.min.js"></script>
    <script>
    $(function() {
        $("#kode").focus();
    });

    $("#reset").click(function () {
        $("#kode").val('');
        $("#kode").focus();
    })
    $("#submit").click(function () {
        scanProses();
    })
    $("#kode").on("input", function () {
        var kode = $("#kode").val();
        if (kode.length >= 7) {
            scanProses();
        }
    });
    function scanProses() {
        $.ajax({
            url:'<?=base_url();?>gudang/proses_cari',
            method: "POST",
            dataType: "JSON",
            data:{
                kode:$("#kode").val(),
            },
            success:function (res) {
                if(res.status){
                    $(".kode").text(res.kode);
                    $(".nama").text(res.nama);
                    $(".total").text('Rp. '+res.total_all);
                    $(".tgl").text(res.tgl_pesan);
                    $(".aksi").html('<button onclick="proses('+res.id+')">Proses Sekarang</button>');
                    
                }else{
                    alert("Data Tidak Ditemukan");
                }
            },
            error:function (data) {
                alert("Terjadi Kesalahan");
            }
        })
    }
    function proses(id) {
        $.ajax({
            url:'<?=base_url();?>gudang/proses_scan',
            method: "POST",
            dataType: "JSON",
            data:{
                id:id,
            },
            success:function (res) {
                if(res.status){
                    $(".kode").text('');
                    $(".nama").text('');
                    $(".total").text('');
                    $(".tgl").text('');
                    $(".aksi").html('');

                    $("#kode").val('');
                    $("#kode").focus();
                    alert("Berhasil memproses");
                    
                }else{
                    $(".kode").text('');
                    $(".nama").text('');
                    $(".total").text('');
                    $(".tgl").text('');
                    $(".aksi").html('');

                    $("#kode").val('');
                    $("#kode").focus();
                    alert("Data ini sudah discan atau diproses sebelumnya");
                    
                }
            },
            error:function (data) {
                alert("Terjadi Kesalahan");
            }
        })
    }
    </script>
</body>
</html>