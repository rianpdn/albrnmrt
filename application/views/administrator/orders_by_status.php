<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Data Pesanan <?=$label;?></h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<form action="<?= base_url(); ?>administrator/export_data_orders" method="post">
				<div class="row">
					<div class="col-md-3">
						<p class="mb-0">Tanggal Awal</p>
						<!-- <div class="form-inline"> -->
							<input type="date" name="tgl_awal" id="inputDatePromo" class="form-control tgl_awal" required>
							<small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
						<!-- </div> -->
					</div>
					<div class="col-md-3">
						<p class="mb-0">Tanggal Akhir</p>
						<!-- <div class="form-inline"> -->
							<input type="date" name="tgl_akhir" id="inputDatePromo" class="form-control tgl_akhir" required>
							<small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
						<!-- </div> -->
					</div>
					<div class="col-md-2">
						<p class="mb-0">Status Pesanan</p>
						<!-- <div class="form-inline"> -->
							<select name="status" class="form-control">
								<option value="-">Semua Status</option>
								<option value="0">Belum Dibayar</option>
								<option value="1">Belum Diproses</option>
								<option value="2">Sedang Diproses</option>
								<option value="3">Sedang Dikirim</option>
								<option value="4">Selesai</option>
							</select>
						<!-- </div> -->
					</div>
					<div class="col-md-3">
					<p class="mb-0">&nbsp;</p>
						<button class="btn btn-primary btn-sm" type="submit">Download Data Pesanan</button>
					</div>
					<div class="col-md-1">
					<p class="mb-0">&nbsp;</p>
						<a href="javascript:;" class="btn btn-warning btn-sm" id="arsip_pesanan" onclick="arsip_pesanan()">Arsipkan</a>
					</div>
				</div>
			</form>
			<!-- <form class="form-inline" action="<?= base_url(); ?>administrator/export_data_orders">
				<div class="form-group mb-2">
					<label for="tgl_awal" class="sr-only">Tanggal Awal</label>
					<input type="datetime-local" name="tgl_awal" id="inputDatePromo" class="form-control" required>
				</div>
				<div class="form-group mb-2">
					<label for="inputPassword2" class="sr-only">Tanggal Akhir</label>
					<input type="datetime-local" name="tgl_akhir" id="inputDatePromo" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-primary mb-2">Export Data Pesanan</button>
			</form> -->
			
		</div>
		<div class="card-body">
            <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($orders->num_rows() > 0){ ?>
			<div class="table-responsive">
				<table
					class="table table-bordered"
					id="dataTable"
					width="100%"
					cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Kode/Invoice</th>
							<th>Nama</th>
							<th>EKSPEDISI</th>
							<th>Total Pesanan</th>
                            <th>Tanggal Pesan</th>
                            <th>Produk</th>
                            <th>Aksi</th>
						</tr>
					</thead>
					<tfoot></tfoot>
					<tbody class="data-content">
						<?php $no = $this->uri->segment(3) + 1; ?>
						<?php foreach($orders->result_array() as $data): ?>
						<tr>
							<td><?= $no; ?></td>
                            <td><?= $data['invoice_code']; ?></td>
                            <td><?= $data['name']; ?></td>
                            <td>
								<?php
								echo $data['courier'];
									// $usertype = $this->db->where('id', $data['user'])->get('user')->row()->user_type;
									// if ($usertype == 1) {
									// 	echo "Pelanggan";
									// }elseif ($usertype == 2) {
									// 	echo "Reseller";
									// }elseif ($usertype == 3) {
									// 	echo "Penulis";
									// }
								?>
							</td>
                            <td>Rp <?= number_format($data['total_all'],0,",","."); ?></td>
							<td><?= $data['date_input']; ?></td>
							<?php
								$produknya = array(); 
								$getProduk = $this->db->where('id_invoice', $data['invoice_code'])->get('transaction')->result();
								foreach ($getProduk as $s) {
									$produknya[] = $s->product_name;
								}
							?>
							<td><?php echo implode(",",$produknya); ?></td>
                            <td>
                                <a href="<?= base_url() ;?>administrator/order/<?= $data['invoice_code']; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
								<?php
									if ($icon_wa == 1) {
								?>
								<!-- href="<?= base_url() ;?>administrator/sendNotifWa?nomor=" -->
								<a title="Kirim Whatsapp Pengingat Bayar" onclick="sendnotiwa('<?= $data['telp']; ?>')" id="sendnotifwa" href="javascript:;"  class="btn btn-sm btn-info" title="Kirim Whatsapp Pengingat Bayar"><i class="fa-whatsapp"></i></a>
								<?php
									}
									if ($icon_print == 1) {
								?>
									 <a href="<?= base_url() ;?>administrator/print_detail_order/<?= $data['invoice_code']; ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i></a>
								<?php
									}
								?>
                            </td>
                        </tr>
						<?php $no++; ?>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php }else{ ?>
			<div class="alert alert-warning" role="alert">
				Opss, pesanan masih kosong.
			</div>
            <?php } ?>
		</div>
	</div>
</div>
<!-- /.container-fluid -->