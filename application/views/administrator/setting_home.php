<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Pengaturan</h1>

	<div class="row">
        <div class="col-md-3">
            <div class="card shadow">
                <div class="card-body">
                <div class="list-group">
                    <a href="<?= base_url(); ?>administrator/setting/home" class="list-group-item list-group-item-action">Home</a>
                    <a href="<?= base_url(); ?>administrator/setting/banner" class="list-group-item list-group-item-action">Banner Slider</a>
                    <a href="<?= base_url(); ?>administrator/setting/description" class="list-group-item list-group-item-action">Deskripsi Singkat</a>
                    <a href="<?= base_url(); ?>administrator/setting/rekening" class="list-group-item list-group-item-action">Rekening</a>
                    <a href="<?= base_url(); ?>administrator/setting/sosmed" class="list-group-item list-group-item-action">Sosial Media</a>
                    <a href="<?= base_url(); ?>administrator/setting/address" class="list-group-item list-group-item-action">Alamat</a>
                    <a href="<?= base_url(); ?>administrator/setting/delivery" class="list-group-item list-group-item-action">Biaya Antar</a>
                    <a href="<?= base_url(); ?>administrator/setting/cod" class="list-group-item list-group-item-action">Cash On Delivery</a>
                    <a href="<?= base_url(); ?>administrator/setting/footer" class="list-group-item list-group-item-action">Footer</a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card shadow">
                <div class="card-header">
                    <h2 class="lead text-dark mb-0">Home</h2>
                </div>
                <div class="card-body">
                    <form action="<?= base_url(); ?>administrator/setting/home" method="post">
						<div class="form-group">
		                    <label for="category_first">Kategori Top</label>
		                    <select name="category_first" id="category_first" class="form-control" required>
		                            <option value="<?= $selectCategoryTop['id']; ?>"> <?= $selectCategoryTop['name']; ?></option>
		                            <?php foreach($categories as $r): ?>
		                                <option value="<?= $r['id']; ?>"><?= $r['name']; ?></option>
		                            <?php endforeach; ?>
		                        </select>
		                </div>
                        <div class="form-group">
							<label for="is_preorder">Slide Preview Buku</label>
                            <a style="color:#fff;" href="<?=base_url();?>administrator/setting/slide_preview" class="btn btn-success">Klik disini untuk Preview Buku</a>
                        </div>
                        <div class="form-group">
							<label for="is_preorder">Small Thumbnail Cover Buku</label>
                            <a style="color:#fff;" href="<?=base_url()?>administrator/setting/small_thumbnail_book" class="btn btn-success">Klik disini untuk Small Thumbnail Preview Buku</a>
                        </div>
                        <div class="form-group">
							<label for="title_preorder">Status Aktif Section Preorder</label>
                            <select class="form-control" name="is_preorder" id="is_preorder">
                                <option value="0" <?php echo $getHomesettings['is_preorder_section'] == 0 ? 'selected="selected"' : '' ; ?> >Tidak Aktif</option>
                                <option value="1" <?php echo $getHomesettings['is_preorder_section'] == 1 ? 'selected="selected"' : '' ; ?> >Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
							<label for="title_preorder">Judul Preorder</label>
                            <input type="text" class="form-control" name="title_preorder" id="title_preorder" value="<?=$getHomesettings['title_preorder'];?>" />
                        </div>
                        <div class="form-group">
							<label for="caption_preorder">Caption Preorder</label>
                            <input type="text" class="form-control" name="caption_preorder" id="caption_preorder" value="<?=$getHomesettings['caption_preorder'];?>" />
                        </div>
                        <div class="form-group">
							<label for="url_prod_preorder">Url Preorder</label>
                            <input type="text" class="form-control" name="url_prod_preorder" id="url_prod_preorder" value="<?=$getHomesettings['url_prod_preorder'];?>" />
                        </div>
                        <div class="form-group">
							<label for="thumbnail_preorder">Url Thumbnail Preorder</label>
                            <input type="text" class="form-control" name="thumbnail_preorder" id="url_prod_preorder" value="<?=$getHomesettings['thumbnail_preorder'];?>" />
                        </div>

						<div class="form-group">
		                    <label for="section_category_two">Kategori Center</label>
		                    <select name="section_category_two" id="section_category_two" class="form-control" required>
		                            <option value="<?= $selectCategoryTwo['id']; ?>"> <?= $selectCategoryTwo['name']; ?></option>
		                            <?php foreach($categories as $r): ?>
		                                <option value="<?= $r['id']; ?>"><?= $r['name']; ?></option>
		                            <?php endforeach; ?>
		                        </select>
		                </div>
                        
						<div class="form-group">
		                    <label for="category_first">Kategori Bottom</label>
		                    <select name="section_category_three" id="section_category_three" class="form-control" required>
		                            <option value="<?= $selectCategoryThree['id']; ?>"> <?= $selectCategoryThree['name']; ?></option>
		                            <?php foreach($categories as $r): ?>
		                                <option value="<?= $r['id']; ?>"><?= $r['name']; ?></option>
		                            <?php endforeach; ?>
		                        </select>
		                </div>
                        <div class="form-group">
							<label for="thumbnail_preorder">Judul Section Bawah</label>
                            <input type="text" class="form-control" name="bottom_title_section" id="bottom_title_section" value="<?=$getHomesettings['bottom_title_section'];?>" />
                        </div>
                        <div class="form-group">
							<label for="is_preorder"><?=$getHomesettings['bottom_title_section'];?></label>
                            <a style="color:#fff;" href="<?=base_url()?>administrator/setting/typewriter_note" class="btn btn-success">Klik disini untuk <?=$getHomesettings['bottom_title_section'];?></a>
                        </div>
                        <button class="btn btn-info">Ubah Home</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
