<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Voucher</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		
	    <div class="row">
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Data Voucher</p>
	                </div>
	                <div class="card-body"> 
                    <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($getVouchers->num_rows() > 0){ ?>
				<div class="table-responsive">
					<table
						class="table table-bordered"
						id="dataTable"
						width="100%"
						cellspacing="0">
						<thead>
							<tr>
								<th style="width: 10px">#</th>
								<th>Judul Voucher</th>
								<th>Kode Voucher</th>
								<th>Nominal</th>
								<th>Type</th>
								<th style="width: 10px">Aksi</th>
							</tr>
						</thead>
						<tbody class="data-content">
							<?php $no =1; ?>
							<?php foreach($getVouchers->result_array() as $data): ?>
							<tr>
								<td><?= $no ?></td>
								<td><?= $data['judul']; ?></td>
								<td><?= $data['kode']; ?></td>
								<td><?= $data['nominal']; ?></td>
								<td><?php
                                    if ($data['type'] == 1) {
                                        echo 'Voucher Belanja';
                                    }else{
                                        echo 'Subsidi Ongkir';
                                    }
                                    ?></td>
								<td>
									<a href="<?= base_url() ;?>administrator/delete_voucher/<?= $data['id']; ?>" onclick="return confirm('Yakin ingin menghapus voucher  ini?')" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
								</td>
							</tr>
							<?php $no++ ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php }else{ ?>
				<div class="alert alert-warning" role="alert">
					Opss, Voucher masih kosong, yuk tambah voucher  sekarang.
				</div>
				<?php } ?>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Tambah Voucher Produk</p>
	                </div>
	                <div class="card-body">
	                    <?php echo $this->session->flashdata('failed'); ?>
	                    <form action="<?= base_url(); ?>administrator/voucher" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
                                <label>Judul Voucher</label>
                                <input type="text" class="form-control" name="judul" id="judul">
                                
	                        </div>
	                        <div class="form-group">
                                <label>Pilih Publik / Pivate</label>
	                            <select class="form-control" name="pubpv" id="pubpv">
                                <option disabled selected>Pilih Publik / Private</option>
                                <option value="1">Publik</option>
                                <option value="2">Private</option>
                                </select>
	                        </div>
	                        <div class="form-group" id="grup_kriteria" style="display: none !important;">
                                <label>Pilih Kriteria</label>
									<select class="form-control" name="kriteria" id="kriteria">
									<option disabled selected>Pilih kriteria</option>
									<option value="1">Pertama Kali Daftar</option>
									<option value="2">Akumulasi Belanja Lebih dari 4 kali</option>
                                </select>
	                        </div>
	                        <div class="form-group" id="grup_kode">
                                <label>Kode Voucher</label>
                                <input type="text" class="form-control" name="kode" id="kode">
                                
	                        </div>
	                        <div class="form-group">
                                <label>Nominal Voucher</label>
                                <input type="text" class="form-control" name="nominal" id="nominal">
                                
	                        </div>
	                        <div class="form-group">
                                <label>Pilih Tipe</label>
	                            <select class="form-control" name="type" id="type">
                                <option disabled selected>Pilih Tipe</option>
                                <option value="1">Voucher Belanja</option>
                                <option value="2">Voucher Subsidi Ongkir</option>
                                </select>
                                
	                        </div>
	                        <div class="form-group">
                                <label>Expired Voucher</label>
                                <input type="date" name="expired" id="inputDatePromo" class="form-control" required>
                                
	                        </div>
	                        <input type="hidden" name="help" value="1">
	                        <button class="btn btn-sm btn-info" type="submit">Proses</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- /.container-fluid -->
