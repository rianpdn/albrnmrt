<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Bulk upload Resi</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		
	    <div class="row">
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Download Format</p>
	                </div>
	                <div class="card-body">
	                        <div class="alert alert-warning">Download Format Excel <a href="https://docs.google.com/spreadsheets/d/1eHHxKKjukLnMzJ2nfmxjUR_lEaYGX4Q__XXyXR96rS8/edit#gid=0" target="_tab">Disini</a></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Upload Excel Resi</p>
	                </div>
	                <div class="card-body">
	                    <?php echo $this->session->flashdata('failed'); ?>
	                    <form action="<?= base_url(); ?>administrator/upload_resi" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
	                            <input type="file" name="file" id="file" class="form-control" required>
	                        </div>
	                        <input type="hidden" name="help" value="1">
	                        <button class="btn btn-sm btn-info" type="submit">Proses</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- /.container-fluid -->
