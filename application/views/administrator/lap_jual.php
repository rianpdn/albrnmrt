<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Laporan Penjualan</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<form action="<?= base_url(); ?>administrator/lap_jual" method="post">
				<div class="row">
					<div class="col-md-3">
						<p class="mb-0">Tanggal Awal</p>
						<!-- <div class="form-inline"> -->
							<input type="date" name="tgl_awal" id="inputDatePromo" class="form-control tgl_awal" required>
							<small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
						<!-- </div> -->
					</div>
					<div class="col-md-3">
						<p class="mb-0">Tanggal Akhir</p>
						<!-- <div class="form-inline"> -->
							<input type="date" name="tgl_akhir" id="inputDatePromo" class="form-control tgl_akhir" required>
							<small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
						
						<!-- </div> -->
					</div>
					<div class="col-md-2">
						<p class="mb-0">&nbsp;</p>
						<input type="hidden" name="help" value="1">
							<button class="btn btn-primary btn-sm" type="submit">Filter</button>
					</div>
					<div class="col-md-2">
						<p class="mb-0">&nbsp;</p>
							<a class="btn btn-primary btn-sm" href="javascript:;" onclick="getLapJual()">Download</a>
					</div>
				</div>
			</form>
			
		</div>
		<div class="card-body">
            <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($getLapJual->num_rows() > 0){ ?>
				<div class="table-responsive">
					<table
						class="table table-bordered"
						id="dataTable"
						width="100%"
						cellspacing="0">
						<thead>
							<tr>
								<th>#</th>
								<th>KODE</th>
								<th>JUDUL BUKU</th>
								<th>HARGA</th>
								<th>QTY</th>
								<th>BRUTO</th>
							</tr>
						</thead>
						<tfoot></tfoot>
						<tbody class="data-content">
							<?php $no = $this->uri->segment(3) + 1; $total_qty=0; $total_bruto=0; ?>
							<?php foreach($getLapJual->result_array() as $data): ?>
							<tr>
								<td><?= $no ?></td>
								<td><?=$data['kode_buku'];?></td>
								<td><?=$data['title'];?></td>
								<td><?=$data['harga'];?></td>
								<td><?=$data['QTY'];?></td>
								<td>Rp <?= str_replace(",",".",number_format($data['bruto'])); ?></td>
							</tr>
                            <?php
                                $total_qty += $data['QTY'];
                                $total_bruto += $data['bruto'];
                            ?>
							<?php $no++ ?>
							<?php endforeach; ?>
						</tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4" style="text-align:right">Total:</th>
                            <th><?=$total_qty;?> </th>
                            <th>Rp <?= str_replace(",",".",number_format($total_bruto)); ?></th>
                        </tr>
                    </tfoot>

					</table>
				</div>
				<?php }else{ ?>
				<div class="alert alert-warning" role="alert">
					Opss, Laporan Penjualan masih kosong, yuk tingkatkan penjualan sekarang.
				</div>
				<?php } ?>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
