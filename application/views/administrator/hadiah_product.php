<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Hadiah Produk</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		
	    <div class="row">
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Data Hadiah</p>
	                </div>
	                <div class="card-body"> 
                    <?php echo $this->session->flashdata('failed'); ?> 
            <?php if($getHadiahProducts->num_rows() > 0){ ?>
				<div class="table-responsive">
					<table
						class="table table-bordered"
						id="dataTable"
						width="100%"
						cellspacing="0">
						<thead>
							<tr>
								<th style="width: 10px">#</th>
								<th>Foto</th>
								<th>Judul</th>
								<th>Produk</th>
								<th style="width: 10px">Aksi</th>
							</tr>
						</thead>
						<tbody class="data-content">
							<?php $no =1; ?>
							<?php foreach($getHadiahProducts->result_array() as $data): ?>
							<tr>
								<td><?= $no ?></td>
								<td><img style="width: 50px" src="<?= base_url(); ?>assets/images/product_hadiah/<?= $data['img_gift']; ?>"></td>
								<td><?= $data['title_gift']; ?></td>
								<td><?= $data['nama_produk']; ?></td>
								<td>
									<a href="<?= base_url() ;?>administrator/delete_hadiah/<?= $data['id']; ?>" onclick="return confirm('Yakin ingin menghapus hadiah produk ini?')" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
								</td>
							</tr>
							<?php $no++ ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php }else{ ?>
				<div class="alert alert-warning" role="alert">
					Opss, Hadiah produk masih kosong, yuk tambah hadiah produk sekarang.
				</div>
				<?php } ?>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Tambah Hadiah Produk</p>
	                </div>
	                <div class="card-body">
	                    <?php echo $this->session->flashdata('failed'); ?>
	                    <form action="<?= base_url(); ?>administrator/hadiah_product" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
                                <label>Judul Hadiah</label>
                                <input type="text" class="form-control" name="title" id="title">
                                
	                        </div>
	                        <div class="form-group">
                                <label>Pilih Produk</label>
	                            <select class="form-control" name="produk" id="pilihProduk">
                                <option disabled selected>Pilih Produk</option>
							<?php foreach($getProducts->result_array() as $data): ?>
                                    <option value="<?=$data['id'];?>"><?=$data['title'];?></option>
                                    <?php endforeach; ?>  
                                </select>
                                
	                        </div>
	                        <div class="form-group">
                                <label>Gambar Hadiah</label>
	                            <input type="file" name="img" id="file" class="form-control" required>
	                        </div>
	                        <input type="hidden" name="help" value="1">
	                        <button class="btn btn-sm btn-info" type="submit">Proses</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- /.container-fluid -->
