<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Import Produk</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		
	    <div class="row">
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Download Format</p>
	                </div>
	                <div class="card-body"> 
                            <div class="alert alert-success">Download Excel Data Produk Terbaru <a href="<?=base_url();?>administrator/download_data_produk" target="_tab">Disini</a></div><br>
	                        <div class="alert alert-warning">Download Format Produk Excel <a href="https://docs.google.com/spreadsheets/d/1Y8nCo7hJCeQZ11FzS1ImQZ8jpvYjxfEmYwvvqU5xcSU/edit#gid=1764023047" target="_tab">Disini</a></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="card shadow mb-4">
	                <div class="card-header">
	                    <p class="lead mb-0 pb-0">Upload Excel Produk</p>
	                </div>
	                <div class="card-body">
	                    <?php echo $this->session->flashdata('failed'); ?>
	                    <form action="<?= base_url(); ?>administrator/import_produk" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
                                <label>Pilih Tipe</label>
	                            <select class="form-control" name="tipe">
                                    <option value="0">Import Produk Baru</option>
                                    <option value="1">Import Update Produk</option>
                                    <option value="2">Import Produk Flashsale</option>
                                </select>
                                
	                        </div>
	                        <div class="form-group">
	                            <input type="file" name="file" id="file" class="form-control" required>
	                        </div>
	                        <input type="hidden" name="help" value="1">
	                        <button class="btn btn-sm btn-info" type="submit">Proses</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- /.container-fluid -->
