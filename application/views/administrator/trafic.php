<?php echo $this->session->flashdata('upload'); ?>
<style type="text/css">
    #chartdiv {
        width: 100%;
        height: 500px;
    }
   
    </style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Traffic Pengunjung Albiruni</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
        <div class="card-header py-3">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="mb-0">Waktu Awal</p>
                            <input type="datetime-local" name="date_start" id="inputDatePromo" class="form-control col-md-7 mr-1 form-control-sm date_start" required>
                            <small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
                        </div>
                        <div class="col-md-4">
                            <p class="mb-0">Waktu Akhir</p>
                            <input type="datetime-local" name="date_end" id="inputDatePromo" class="form-control col-md-7 mr-1 form-control-sm date_end" required>
                            <small>(Jika Browser Firefox .Contoh: 2021-01-31 13:50:00)</small>
                        </div>
                        <div class="col-md-3">
                        <p class="mb-0">&nbsp;</p>
                            <button class="btn btn-primary btn-sm" id="filter_trafic" onclick="loadtraf()">Filter</button>
                        </div>
                    </div>
                    
        <div class="row">
            <div class="col-xl-6 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Pengunjung</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="totalpengunjung">
						0
						</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Pembeli</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="total pembeli">
						0</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-star fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
		<div class="card-body">
        <canvas id="myChart"></canvas>
    <!-- Chart code -->
    
    </div>



		</div>
	</div>
</div>
<!-- /.container-fluid -->
