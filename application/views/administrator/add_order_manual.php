<?php echo $this->session->flashdata('upload'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

<?php echo $this->session->flashdata('failed'); ?>
			<form
				action="<?= base_url(); ?>administrator/add_order_manual"
				method="post"
				enctype="multipart/form-data">
	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Tambah Order Manual</h1>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="paymentSelectProvinces">Pilih Pembeli </label>
                        <select name="reseller" id="reseller" class="form-control" required>
                            <?php 
                                foreach($reseller as $p){ 
                            ?>
                                <option value="<?= $p['id']; ?>"><?= $p['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
				<div class="col">
                    <p class="mb-0">&nbsp;</p>
                    <a href="javascript:;" class="btn btn-warning btn-sm" style="margin-top:12px;" data-toggle="modal" data-target="#adduser">Tambah Pembeli</a>
				</div>
            </div>
		</div>
		<div class="card-body">
            <h3 class="lead">Produk</h3>
            <hr>
				<div class="row">
					<div class="col">
						<p class="mb-0">Pilih Produk</p>
						<!-- <div class="form-inline"> -->
                            <select name="pilih_produk" id="pilih_produk" class="form-control" >
                                <?php foreach($product as $d){ ?>
                                    <option value="<?= $d['productsId']; ?>"><?= $d['productsTitle'].' - '.number_format($d['productsPrice'],0,",","."); ?></option>
                                <?php } ?>
                            </select>
						<!-- </div> -->
					</div>
					<div class="col">
						<p class="mb-0">Jumlah Pesanan</p>
						<!-- <div class="form-inline"> -->
						<input type="number" id="jml" autocomplete="off" class="form-control" placeholder=""  name="jml" value=""> 
						<!-- </div> -->
					</div>
					<div class="col">
						<p class="mb-0">Diskon (%)</p>
						<!-- <div class="form-inline"> -->
						<input type="number" id="diskon" autocomplete="off" class="form-control" placeholder=""  name="diskon" value="0"> 
						<!-- </div> -->
					</div>
					<div class="col">
                        <p class="mb-0">&nbsp;</p>
                        <a href="javascript:;" class="btn btn-warning btn-sm" onclick="addProduk();">Submit Produk</a>
					</div>
				</div>

            <div class="row" style="margin-top: 15px;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Diskon</th>
                            <th>Total</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="loop_produk">
                    </tbody>
                </table>
            </div>

            <hr>
            <h3 class="lead">Data Alamat Pengiriman</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td>Label Alamat</td>
                            <td><input type="text" id="label" autocomplete="off" class="form-control" placeholder="Contoh: Rumah, Kantor, Kos, dll" required name="label" value=""> </td>
                        </tr>
                        <tr>
                            <td>Nama Penerima</td>
                            <td><input type="text" id="name" autocomplete="off" class="form-control" required name="name" value=""></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="email" id="email" autocomplete="off" class="form-control" required name="email" value="">
                        </td>
                        </tr>
                        <tr>
                            <td>Telepon</td>
                            <td><input type="number" id="telp" autocomplete="off" class="form-control" required name="telp" value="">
                        <small class="text-muted">Contoh: 081234567890</small></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>
                                <select name="paymentSelectProvinces" id="paymentSelectProvinces" class="form-control" required>
                                <?php foreach($provinces as $p){ ?>
                                    <option value="<?= $p['province_id']; ?>"><?= $p['province']; ?></option>
                                <?php } ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td>Kabupaten/Kota</td>
                            <td>
                                <select name="paymentSelectRegencies" id="paymentSelectRegencies" class="form-control" required>
                                   
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>
                                <input type="text" class="form-control" autocomplete="off" id="district" name="district" required value="">
                            </td>
                        </tr>
                        <tr>
                            <td>Desa/Kelurahan</td>
                            <td>
                            <input type="text" class="form-control" autocomplete="off" id="village" name="village" required value="">
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Post</td>
                            <td><input type="number" id="zipcode" autocomplete="off" class="form-control" required name="zipcode" value=""></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td style="width: 65%">
                            <textarea name="address" id="address" class="form-control" placeholder="Isi dengan nama jalan, nomor rumah, nama gedung, dsb" required></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr>
            <h3 class="lead">Metode Pengiriman</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td>Nama Ekspedisi</td>
                            <td>
                               <input name="nama_ekspedisi" type="text" id="nama_ekspedisi" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Biaya Ekspedisi</td>
                            <td>
                            <input name="biaya_ekspedisi" type="text" id="biaya_ekspedisi" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Pilih Pengiriman</td>
                            <td>
                                <select name="paymentSelectKurir" id="paymentSelectKurir" class="form-control">
                                    <option value="-">Harap Pilih Kota Tujuan</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="paymentSelectProvinces">Total Belanja  : </label>
                        <span class="total_belanja"></span>
                        <input type="hidden" id="paymentPriceTotalAll" value="0">
                    </div>

                    <div class="form-group">
                        <label for="paymentSendingPrice">Biaya Pengiriman : </label>
                        <span id="paymentSendingPrice"></span>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="totaltagihan">Total Tagihan : </label>
                        <span id="paymentTotalAll">Rp0</span>
                        
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col">
                <button type="submit" class="btn btn-primary">Buat Order</button>
                </div>
            </div>
				
		</div>
	</div>

	</form>
</div>
<!-- /.container-fluid -->

<div
	class="modal fade"
	id="adduser"
	tabindex="-1"
	role="dialog"
	aria-labelledby="exampleModalLabel"
	aria-hidden="true"
>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Pembeli</h5>
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-label="Close"
				>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form
					action="<?= base_url(); ?>administrator/add_user_pembeli"
					method="post"
					enctype="multipart/form-data"
				>
					<div class="form-group">
						<label for="name">Nama Lengkap</label>
						<input
							type="text"
							class="form-control"
							id="nama"
							name="nama"
							autocomplete="off"
							required
						/>
					</div>
					<div class="form-group">
						<label for="name">Email</label>
						<input
							type="email"
							class="form-control"
							id="email"
							name="email"
							autocomplete="off"
							required
						/>
					</div>
					<div class="form-group">
						<label for="name">No Handphone</label>
						<input
							type="text"
							class="form-control"
							id="telp"
							name="telp"
							autocomplete="off"
							required
						/>
                        <input type="hidden" value="1" name="tipe" />
					</div>
					<button type="submit" class="btn btn-primary" id="btnAddCategory">
						Tambahkan
					</button>
				</form>
			</div>
		</div>
	</div>
</div>