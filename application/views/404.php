
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta name="format-detection" content="telephone=no">
    <meta property="fb:app_id" content="123" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base />
    <base />
    <meta name="description" content="Albirunimart" />
    <link defer href="<?=base_url()?>vendor/img/logo.png" rel="icon" />
    <!-- <meta property="og:image" content="https://images.unicartapp.com/image/nukilanbiruni/image/data/EAMn95mt1589657358.png" /> -->

    <meta property="og:image" content="" />
    <meta property="og:image:width" content="256" />
    <meta property="og:image:height" content="256" />

    <!-- <meta property="twitter:image" content="https://images.unicartapp.com/image/nukilanbiruni/image/data/EAMn95mt1589657358.png" /> -->


    <!-- BULMA -->
    <link defer href="<?=base_url();?>vendor/bulma.css" rel="stylesheet" type="text/css" />
    <link defer href="<?=base_url();?>vendor/bulma-checkradio.min.css" rel="stylesheet" type="text/css" />


    <!-- MAIN STYLESHEET -->
    <link defer href="https://images.unicartapp.com/catalog/view/theme/aio/stylesheet/aio.css?ver=1613034029" rel="stylesheet" type="text/css" />

    <!-- CAROUSEL -->
    <link defer href="<?=base_url();?>vendor/slick/slick.css" rel="stylesheet" type="text/css" />
    <link defer href="<?=base_url();?>vendor/slick/slick-theme.css" rel="stylesheet" type="text/css" />

    <!-- FONT AWESOME -->
    <link defer rel="stylesheet" href="<?=base_url();?>vendor/fontawesome/font-awesome.min.css">

    <!-- MATERIAL DESIGN ICON -->
    <link defer rel="stylesheet" href="https://images.unicartapp.com/catalog/view/theme/aio/stylesheet/materialdesignicons-3.3.92/materialdesignicons.min.css">

    <!-- STAATLICHES -->
    <link defer rel="stylesheet" href="<?=base_url();?>vendor/staatliches/staatliches.css">

    <!-- ANIMATION -->
    <link defer href="<?=base_url();?>vendor/animate.css" rel="stylesheet" type="text/css" />

    <!-- BULMA CALENDAR -->
    <link defer href="<?=base_url();?>vendor/bulma-calendar.min.css" rel="stylesheet" type="text/css" />

    <!-- JQUERY -->
    <script src="<?=base_url();?>vendor/jquery-3.3.1.min.js"></script>
    <script defer type="text/javascript" src="<?=base_url();?>vendor/jquery.cookie.js"></script>

    <!-- TOTAL STORAGE -->
    <script defer type="text/javascript" src="<?=base_url();?>vendor/jquery.total-storage.min.js"></script>

    <!-- FANCYBOX -->
    <link defer rel="stylesheet" href="<?=base_url();?>vendor/fancybox/jquery.fancybox.min.css">
    <script async src="<?=base_url();?>vendor/fancybox/jquery.fancybox.min.js"></script>

    <!-- ELEVATEZOOM -->
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.easing.min.js"></script>
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.mousewheel.js"></script>
    <script async src="<?=base_url();?>vendor/elevatezoom-plus/jquery.ez-plus.js"></script>

    <!-- LAZYLOAD -->
    <script src="<?=base_url();?>vendor/jquery.lazy.min.js"></script>
    <script src="<?=base_url();?>vendor/jquery.lazy.plugins.min.js?ver=1.0"></script>

    <!-- SWEETALERT -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- CUSTOMIZE -->
    <link defer id="customize_css" href="<?=base_url();?>vendor/aio_customize_css.css?ver=10022021" rel="stylesheet" type="text/css" />


    <!-- OVERWRITE CSS -->
    <link defer rel="stylesheet" type="text/css" href="<?=base_url();?>vendor/overwrite.css?ver=10022021" />

    <style>
        .btn-wishlist,
        .btn-compare {
            display: none;
        }
    </style>


    <!-- TITLE -->
    <title><?= $title ?></title>
</head>
  <?php
  $setting = $this->db->get('settings')->row_array();
  $dateNow = date('Y-m-d H:i');
  $dateDB = $setting['promo_time'];
  $dateDBflashsale = $setting['flashsale_time'];
  $dateDBNew = str_replace("T"," ",$dateDB);
  $dateDBNewFlashsale = str_replace("T"," ",$dateDBflashsale);
  if($dateNow >= $dateDBNew){
    $this->db->set('promo', 0);
    $this->db->update('settings');
  }
  if($dateNow >= $dateDBNewFlashsale){
    $this->db->set('flashsale', 0);
    $this->db->update('settings');
  }
  ?>
<body class="body-style wide  clamp-1">

    <div id="modal-search-product" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <form action="<?= base_url(); ?>search" method="get">
                <section class="modal-card-body">
                    <div class="search-input">
                        <input class="search-bar-input" name="q" type="text" placeholder="Search" v/>
                        <i id="icon-search" class="mdi mdi-magnify"></i>
                    </div>
                </section>
                <footer class="modal-card-foot">
                    <button type="button" class="search-cancel" onclick="closeModals();">Batal</button>
                    <button type="submit" id="btn-search">Cari</button>
                </footer>
            </form>
        </div>
    </div>

    <div id="modal-checkout-cartmodal"></div>



    <!-- ANNOUCEMENT BAR  -->
    <style>
        #announcement-bar-content {
            background-color: #efefef;
        }

        #announcement-bar-banner {
            color: #000000;
        }

        #announcement-bar-button {
            color: #ffffff !important;
            background-color: #f63636 !important;
            border-color: #f63636 !important;
        }
    </style>
<!--     <div id="announcement-bar-content">
        <div id="announcement-bar-banner">
            <span>Pre Order Embroidery Mask by The Angkasa (8-15 Feb)</span>
            <button id="announcement-bar-button" class="button" onclick="window.open('embroidery-mask-by-the-angkasa.html','_blank');">
				PRE ORDER					
            </button>

        </div>
    </div> -->
    <!-- END ANNOUCEMENT BAR  -->
    
    <div id="wrapper" class="clearfix">

  
<section id="error-not_found" class="section content container">
	<div id="contents">
		<div id="main-content">
			<div class="holder">

				<div class="content">
					<div id="lottie"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 230 230" width="230" height="230" preserveAspectRatio="xMidYMid meet" style="width: 100%; height: 100%; transform: translate3d(0px, 0px, 0px);"><defs><clipPath id="animationMask_PYU5XMlT7O"><rect width="230" height="230" x="0" y="0"></rect></clipPath></defs><g clip-path="url(#animationMask_PYU5XMlT7O)"><g transform="matrix(1,0,0,1,-14,-13.999000549316406)" opacity="1" style="display: block;"><g opacity="1" transform="matrix(1,0,0,1,126.5,126.5)"><path stroke-linecap="round" stroke-linejoin="miter" fill-opacity="0" stroke-miterlimit="10" stroke-dasharray=" 14 25" stroke-dashoffset="-193.54440307617188" stroke="rgb(197,197,197)" stroke-opacity="1" stroke-width="11" d=" M99,0 C99,54.67599868774414 54.676998138427734,99 0,99 C-54.67599868774414,99 -99,54.67599868774414 -99,0 C-99,-54.67599868774414 -54.67599868774414,-99 0,-99 C54.676998138427734,-99 99,-54.67599868774414 99,0z"></path></g></g><g transform="matrix(1,0,0,1,37,16)" opacity="1" style="display: block;"><g opacity="1" transform="matrix(1,0,0,1,76.5,84.97599792480469)"><path stroke-linecap="round" stroke-linejoin="round" fill-opacity="0" stroke="rgb(197,197,197)" stroke-opacity="1" stroke-width="19" d=" M-29,-14.47599983215332 C-29,-14.47599983215332 -27,-37.47600173950195 -1,-37.47600173950195 C17,-37.47600173950195 29,-24.475000381469727 26,-9.475000381469727 C23.05900001525879,5.232999801635742 9,14.524999618530273 3,20.524999618530273 C-0.9589999914169312,24.483999252319336 -1.430999994277954,31.628000259399414 -1.3029999732971191,35.297000885009766 C-1.2269999980926514,37.48899841308594 -1.125,40.60100173950195 -1.125,40.60100173950195"></path></g></g><g transform="matrix(1,0,0,1,101.75,156.8560028076172)" opacity="1" style="display: block;"><g opacity="1" transform="matrix(1,0,0,1,11.25,11.25)"><path fill="rgb(197,197,197)" fill-opacity="1" d=" M11,0 C11,6.074999809265137 6.074999809265137,11 0,11 C-6.074999809265137,11 -11,6.074999809265137 -11,0 C-11,-6.074999809265137 -6.074999809265137,-11 0,-11 C6.074999809265137,-11 11,-6.074999809265137 11,0z"></path></g></g></g></svg></div>

											<div class="title">
							Not Found						</div>
										<div class="message">
						The page you requested cannot be found.					</div>
					<button class="button" onclick="location.href='<?=base_url();?>';">
						Continue					</button>
				</div>	

			</div>
		</div>
	</div>
</section>
 <!-- ###AIO### -->
        <!-- BACK TO TOP BUTTON -->
        <button onclick="backToTop()" id="backToTop" title="Back to top"><i class="fa fa-chevron-up"></i></button>

        <footer id="footer" class="footer uni-foot-4">

            <!-- ADD TO CART POPUP  -->
            <input type="hidden" id="current_product_id" />
            <input type="hidden" id="config_cart_modal" value="1">
            <!-- YOU MIGHT NEED THIS APP -->
            <input type="hidden" id="you_might_need_this_status" value="0">

            <!-- FOOTER MODULES -->
            <div class="container">

                <div class="el_1">
                    <!-- FOOTER MENUS -->
                    <!-- END FOOTER MENU -->

                    <div id="newsletter_socialmedia" class="module-container">
                        <div id="newsletter">
                            <div class="title">Newsletter</div>

                            <head>
                                <script id="mcjs">
                                    // ! function(c, h, i, m, p) {
                                    //     m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
                                    // }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/6d694bd739302ba342afbde49/f85154678eafefaa12d0e0fd2.js");
                                </script>

                                <!-- Begin Mailchimp Signup Form -->
                                <link href="http://cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                                <style type="text/css">
                                    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                                </style>
                                <div id="mc_embed_signup">
                                    <form action="https://nukilanbiruni.us10.list-manage.com/subscribe/post?u=6d694bd739302ba342afbde49&amp;id=303f0d4008" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll">
                                            <h2>Subscribe</h2>
                                            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                            <div class="mc-field-group">
                                                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>
                                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                            </div>
                                            <div id="mce-responses" class="clear">
                                                <div class="response" id="mce-error-response" style="display:none"></div>
                                                <div class="response" id="mce-success-response" style="display:none"></div>
                                            </div>
                                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6d694bd739302ba342afbde49_303f0d4008" tabindex="-1" value=""></div>
                                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                        </div>
                                    </form>
                                </div>

                                <!--End mc_embed_signup-->
                        </div>

                        <!-- SOCIAL MEDIA -->
                        <div id="socialmedia">
                            <div class="title">
                                Follow Us </div>

                            <div class="icons-social-media">
                                <a id="footer-social-fb" class="icon" href="javascript:void(0);" style="display:none;" target="_blank">
                                    <div class="mdi mdi-facebook"></div>
                                </a>
                                <a id="footer-social-youtube" class="icon" href="javascript:void(0);" style="display:none;" target="_blank">
                                    <div class="mdi mdi-youtube"></div>
                                </a>
                                <a id="footer-social-instagram" class="icon" href="https://www.instagram.com/albiruni.mart" target="_blank">
                                    <div class="mdi mdi-instagram"></div>
                                </a>
                            </div>
                        </div>
                        <!-- END SOCIAL MEDIA -->
                    </div>


                    <!-- PAGE CONTENT FOOTER  -->
                    <div id="payment_method" class="module-container">

                        <div class="title">
                        </div>
                        <div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT FOOTER  -->
                </div>

                <div class="el_2">
                    <div id="el_2_logo">
                    </div>
                    <div class="el_2_content">
                    </div>
                </div>

                <div class="el_3">
                    <div id="additional_cms" class="module-container">
                        <div class="title"></div>
                    </div>
                </div>
            </div>



            <!-- COPYRIGHTS INFO -->
            <div id="copyrights">
                <div class="container">
                    <!-- <div class="powered_left">
                        Copyright  © 2021  Albiruni Mart. All rights reserved.                    </div>
                    <div class="powered_right">
                        <a target="_top" href="https://www.sitegiant.my">eCommerce by SiteGiant</a>                    </div> -->

                    <div class="powered">
                        <span class="copyright">Copyright  © 2021  Albiruni Mart. All rights reserved.</span><span style="float:right;" class="powered"></span> </div>
                </div>
            </div>
        </footer>



        <!-- ==== SCRIPTS ==== -->
        <!-- JQUERY -->
        <script defer type="text/javascript" src="<?=base_url()?>vendor/jquery-ui-1.12.1.min.js"></script>
        <!-- CAROUSEL -->
        <script defer src="<?=base_url()?>vendor/slick.min.js"></script>
        <!-- CLIPBOARD -->
        <script defer src="<?=base_url()?>vendor/clipboard.min.js"></script>
        <!-- FUNCTIONS -->
        <script defer type="text/javascript" src="<?=base_url()?>vendor/notifyme.js"></script>
        <!-- SWEET ALERT -->
        <script defer src="<?=base_url()?>vendor/sweetalert2.all.min.js"></script>
        <!-- PRINTTHIS -->
        <script defer src="<?=base_url()?>vendor/printThis.js"></script>
        <!-- FLATPICKR -->
        <link defer rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <script defer src="<?=base_url()?>vendor/flatpickr.js"></script>

        <!-- BULMA CALENDAR -->
        <script defer src="<?=base_url()?>vendor/doc.js"></script>
        <script defer src="<?=base_url()?>vendor/bulma-calendar.min.js"></script>
        <!-- <script defer src="https://images.unicartapp.com/catalog/view/theme/aio/plugins/bulma-calendar/main.js"></script> -->

        <!-- UI CONTROL SCRIPT -->
        <script defer src="<?=base_url()?>vendor/ui-control.js?ver=10022021"></script>

        <!-- LAZYLOAD -->
        <script>
            function lazyload() {
                $('img.lazy').lazy({
                    attribute: 'data-lazy',
                    effect: "fadeIn",
                    effectTime: 2000
                });
            }
        </script>
        <!-- LOAD USER SETTINGS -->
        <script>
            $(document).ready(function() {
                lazyload();


                productslick(3, 3, 4);

                search_autocomplete();

                multitabs(3, 3, 4, true);
                // END PRODUCT EFFECT FADE
                productfade();
            });
        </script>

</body>
</html>