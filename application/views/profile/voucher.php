<div class="wrapper">
    <?php include 'menu.php'; ?>
    <div class="core">
        <h2 class="title">Voucher Anda</h2>
        <hr>
        <?php if($voucher->num_rows() > 0){ ?>
            <table class="table table-bordered">
                <tr>
                    <th>Kode Voucher</th>
                    <th>Status</th>
                </tr>
                <?php foreach($voucher->result_array() as $d): ?>
                    <tr>
                        <td><?= $d['kode']; ?></td>
                        <td>
                            <?php
                                if ($d['status'] == 0) {
                                   echo 'Belum terpakai';
                                }else{
                                    echo 'Sudah terpakai';
                                }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php }else{ ?>
            <div class="alert alert-warning">Anda Belum Punya Voucher.</div>
        <?php } ?>
    </div>
</div>