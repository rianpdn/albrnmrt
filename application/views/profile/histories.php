<style>
/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}

</style>
<?php echo $this->session->flashdata('success'); ?>
<div class="wrapper">
    <?php include 'menu.php'; ?>
    <div class="core">
        <h2 class="title">Riwayat Transaksi</h2>
        <hr>
        <?php if($finish->num_rows() > 0){ ?>
            <table class="table table-bordered">
                <tr>
                    <th>Order ID</th>
                    <th>Tanggal Pesan</th>
                    <th>Total Pembayaran</th>
                    <th>#</th>
                </tr>
                <?php foreach($finish->result_array() as $d): ?>
                    <tr>
                        <td><?= $d['invoice_code']; ?></td>
                        <td><?= $d['date_input']; ?></td>
                        <td>Rp<?= number_format($d['total_all'],0,",","."); ?></td>
                        <td>
                            <small><a href="<?= base_url(); ?>profile/transaction/<?= $d['invoice_code']; ?>" class="btn btn-warning">Detail</a></small>
                            <?php
                            $cekreview = $this->db->where('invoice',$d['invoice_code'])
                                                    ->get('review')->num_rows();
                                if ($cekreview == 0) {
                            ?>
                            <small><a href="javascript:;" onclick="giveReview('<?= $d['invoice_code']; ?>')" class="btn btn-info">Berikan Ulasan</a></small>
                            <?php } ?>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php }else{ ?>
            <div class="alert alert-warning">Tidak ada pesanan. Yuk Belanja.</div>
        <?php } ?>
    </div>
</div>

<!-- Modal Add Category -->
<div
	class="modal fade"
	id="addReview"
	tabindex="-1"
	role="dialog"
	aria-labelledby="exampleModalLabel"
	aria-hidden="true"
>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-label="Close"
				>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form
					action="<?= base_url(); ?>profile/saveUlasan"
					method="post"
					enctype="multipart/form-data"
				>
					<div class="form-group">
						<label for="name">Berikan bintang kamu :)</label>
						  <!-- Rating Stars Box -->
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        <input type="hidden" id="bintang" value="0" name="bintang" required/>
                        <input type="hidden" id="invoice" value="0" name="invoice"/>
                        </div>
					</div>
					<div class="form-group">
						<label for="ulasan">Berikan ulasan kamu</label>
						<textarea
							class="form-control"
							required
							name="ulasan"
							id="ulasan"></textarea>
					</div>
					<button type="submit" class="btn btn-primary" id="btnAddCategory">
						Simpan Ulasan
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
