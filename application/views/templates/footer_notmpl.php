<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="<?= base_url(); ?>assets/select2-4.0.6-rc.1/dist/js/select2.min.js"></script>
<script>
//loading screen
$(window).ready(function(){
    $(".loading-animation-screen").fadeOut("slow");
    $('#modalRegisterSuccess').modal('show');
    var loadsuksespage = $('#sukses_page');
    function giveVoucher() {
        var invo = $('#invo').val();
        $.ajax({
            url : '<?=base_url();?>payment/giveVocuher',
            type : 'POST',
            data : {
                'inv' : invo
            },
            dataType:'json',
            success : function(data) {   
                console.log("res >>", data)
            },
            error : function(request,error)
            {
                console.log("error")
            }
            });
    }
    if(loadsuksespage.length){giveVoucher();}
    getProv();
})
function getProv() {
    var html = '';
        $.getJSON('prov.json', 
        function(res){
            html = '<option value="0">Pilih Provinsi</option>';
            var prov = res.rajaongkir.results;
            var i;
            for(i=0; i<prov.length; i++){
                html += '<option value="'+prov[i].province_id+'">'+prov[i].province+'</option>';
            }
            $('#paymentSelectProvinces').html(html);
        });
}
$("#paymentSelectProvinces").select2({
            placeholder: 'Pilih Provinsi',
            language: 'id'
        })

        $("#paymentSelectRegencies").select2({
            placeholder: 'Pilih Kabupaten/Kota',
            language: 'id'
        })
        $("#paymentSelectProvinces").change(function(){
            $("#paymentSelectRegencies").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            const id = $(this).val();
            // $.ajax({
            //     url: "<?= base_url(); ?>payment/getLocation",
            //     type: "post",
            //     dataType: "json",
            //     async: true,
            //     data: {
            //         id: id
            //     },
            //     success: function(data){
            //         $("#paymentSelectRegencies").select2({
            //             placeholder: 'Pilih Kabupaten/Kota',
            //             language: 'id'
            //         })
            //         $("#paymentSelectRegencies").html(data);
            //         $("#paymentTextErrorAboveSelectKurir").hide();
            //     }
            // });
            var html = '';
                console.log("id provinsi setelah get prov >> ",id)
                $.getJSON('kota.json', 
                function(res){
                    html = '<option value="0">Pilih Kota / Kabupaten</option>';
                    var city = res.rajaongkir.results;
                    var i;
                
                    for(i=0; i<city.length; i++){
                        if(city[i].province_id == id) {
                            console.log("kota di filter >>> ", city[i])
                            html += '<option value="'+city[i].city_id+'">'+city[i].type+' '+city[i].city_name+'</option>';
                        }
                        
                    }
                    $('#paymentSelectRegencies').html(html);
                });
        })
</script>
</body>
</html>