
<?php
$categoriesLimit = $this->Categories_model->getCategoriesLimit();
$setting = $this->Settings_model->getSetting();
$sosmed = $this->Settings_model->getSosmed();
$footerhelp = $this->Settings_model->getFooterHelp();
$footerinfo = $this->Settings_model->getFooterInfo();
$rekening = $this->db->get('rekening');
 ?>
    <footer class="bg-light">
        <div class="information">
            <div class="main">
            <div class="about about-hide">
                <h2 class="brand-footer"><?= $this->config->item('app_name'); ?></h2>
                <p>
                <?= $setting['short_desc']; ?>
                </p>
                <div class="sosmed">
                <h3>Temukan kami di</h3>
                <?php foreach($sosmed->result_array() as $s): ?>
                  <a href="<?= $s['link']; ?>" target="_blank" title="<?= $s['name']; ?>">
                      <i class="fab fa-<?= $s['icon']; ?>"></i>
                  </a>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="item item-hide">
                <h3 class="title">KATEGORI</h3>
                <?php foreach($categoriesLimit->result_array() as $c): ?>
                    <a href="<?= base_url(); ?>c/<?= $c['slug']; ?>"><?= $c['name']; ?></a>
                <?php endforeach; ?>
            </div>
            <div class="item">
                <h3 class="title">INFO <?= strtoupper($this->config->item('app_name')); ?></h3>
                <?php foreach($footerinfo->result_array() as $f): ?>
                  <a href="<?= base_url(); ?><?= $f['slug']; ?>"><?= $f['title']; ?></a>
                <?php endforeach; ?>
            </div>
            <div class="item">
                <h3 class="title">BANTUAN</h3>
                <?php foreach($footerhelp->result_array() as $f): ?>
                  <a href="<?= base_url(); ?><?= $f['slug']; ?>"><?= $f['title']; ?></a>
                <?php endforeach; ?>
            </div>
            <div class="item item-hide">
              <h3 class="title">PEMBAYARAN</h3>
              <?php foreach($rekening->result_array() as $r): ?>
                  <p class="mb-0"><?= $r['rekening']; ?></p>
              <?php endforeach; ?>
            </div>
            </div>
        </div>
        <div class="contact">
            <div class="main">
                <div class="item">
                    <i class="fa fa-map-marker-alt"></i>
                    <p><?= nl2br($setting['address']); ?></p>
                </div>
                <div class="item">
                    <i class="fa fa-phone"></i>
                    <p><?= $this->config->item('whatsapp'); ?></p>
                </div>
                <div class="item">
                    <i class="fa fa-envelope"></i>
                    <p><?= $this->config->item('email_contact'); ?></p>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p class="mb-0">Copyright &copy; <span id="footer-cr-years"></span> <?= $this->config->item('app_name'); ?>. All Right Reserved.</p>
        </div>
        </footer>

        <?php
        if($this->session->userdata('login')){
            $cart = $this->db->get_where('cart', ['user' => $this->session->userdata('id')]);
            $totalall = 0;
            foreach($cart->result_array() as $c){
                $totalall += intval($c['price']) * intval($c['qty']);
            }
        }
        ?>

    <!-- JQUERY -->
    <script src="<?=base_url();?>vendor/jquery-3.3.1.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/lightbox2-2.11.1/dist/js/lightbox.js"></script>
    <script src="<?= base_url(); ?>assets/select2-4.0.6-rc.1/dist/js/select2.min.js"></script>
    <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/twitterbootstrap/js/bootstrap-tab.js"></script> -->
    <script>
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

    // make it as accordion for smaller screens
    if ($(window).width() < 992) {
    $('.dropdown-menu a').click(function(e){
        e.preventDefault();
        if($(this).next('.submenu').length){
            $(this).next('.submenu').toggle();
        }
        $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.submenu').hide();
    })
    });
    }
        $('.recent-product').slick({
            infinite: false,
            slidesToShow: 6,
            slidesToScroll: 1
        });

        $("i.icon-search-navbar").on('click', function(){
            $("div.search-form").slideDown('fast');
            $("div.search-form input").focus();
        })

        $("div.search-form i").on('click', function(){
            $("div.search-form").slideUp('fast');
        })

        $("div.product-wrapper div.main-product button.slick-prev").html("<i class='fa fa-chevron-left'></i>");
        $("div.product-wrapper div.main-product button.slick-next").html("<i class='fa fa-chevron-right'></i>");

        const years = new Date().getFullYear();
        $("#footer-cr-years").text(years);

        $("#countdownPromo").countdown({
            date: "<?= $setting['flashsale_time']; ?>",
            render: function (data) {
                var el = $(this.el);
                el.empty()
                .append(
                    this.leadingZeros(data.days, 2) + " : "
                )
                .append(
                    this.leadingZeros(data.hours, 2) + " : "
                )
                .append(
                    this.leadingZeros(data.min, 2) + " : "
                )
                .append(
                    this.leadingZeros(data.sec, 2)
                );
            },
        });

        //loading screen
        $(window).ready(function(){
            $(".loading-animation-screen").fadeOut("slow");
        })

        // detail
        $("#detailBtnPlusJml").click(function(){
            var val = parseInt($(this).prev('input').val());
            $(this).prev('input').val(val + 1).change();
            return false;
        })

        $("#detailBtnMinusJml").click(function(){
            var val = parseInt($(this).next('input').val());
            if (val !== 1) {
                $(this).next('input').val(val - 1).change();
            }
            return false;
        })
        var loadpagepayment = $('#paymentSelectProvinces');
        if(loadpagepayment.length){
            getProv()
            getKota()
        };

        $("#paymentSelectProvinces").select2({
            placeholder: 'Pilih Provinsi',
            language: 'id'
        })
        function getProv() {
            var html = '';
            var prop = $('#propinsi').val();
                $.getJSON('prov.json', 
                function(res){
                    html = '<option value="0">Pilih Provinsi</option>';
                    var prov = res.rajaongkir.results;
                    var i;
                    for(i=0; i<prov.length; i++){
                        if (prov[i].province_id == prop) {
                            html += '<option value="'+prov[i].province_id+'" selected>'+prov[i].province+'</option>';
                        }
                        html += '<option value="'+prov[i].province_id+'">'+prov[i].province+'</option>';
                    }
                    $('#paymentSelectProvinces').html(html);
                });
        }
        function getKota() {
            var prop = $('#propinsi').val();
            var kota = $('#kota').val();
                console.log("id provinsi setelah get prov >> ",prop)
                $.getJSON('kota.json', 
                function(res){
                    html = '<option value="0">Pilih Kota / Kabupaten</option>';
                    var city = res.rajaongkir.results;
                    var i;
                
                    for(i=0; i<city.length; i++){
                        if(city[i].province_id == prop) {
                            if (city[i].city_id == kota) {
                                html += '<option value="'+city[i].city_id+'" selected>'+city[i].type+' '+city[i].city_name+'</option>';
                            }
                            console.log("kota di filter >>> ", city[i])
                            html += '<option value="'+city[i].city_id+'">'+city[i].type+' '+city[i].city_name+'</option>';
                        }
                        
                    }
                    $('#paymentSelectRegencies').html(html);
                    
                });
        }
        $("#paymentSelectRegencies").select2({
            placeholder: 'Pilih Kabupaten/Kota',
            language: 'id'
        })

        $("#paymentSelectKurir").select2({
            placeholder: 'Pilih Salah Satu',
            language: 'id'
        })

        $("#paymentSelectService").select2({
            placeholder: 'Pilih Service',
            language: 'id'
        })

        $("#paymentSelectProvinces").change(function(){
            $("#paymentSelectKurir").html(' ');
            $("#paymentSelectRegencies").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            const id = $(this).val();
            // $.ajax({
            //     url: "<?= base_url(); ?>payment/getLocation",
            //     type: "post",
            //     dataType: "json",
            //     async: true,
            //     data: {
            //         id: id
            //     },
            //     success: function(data){
            //         $("#paymentSelectRegencies").select2({
            //             placeholder: 'Pilih Kabupaten/Kota',
            //             language: 'id'
            //         })
            //         $("#paymentSelectRegencies").html(data);
            //         $("#paymentTextErrorAboveSelectKurir").hide();
            //     }
            // });
            var html = '';
            var kota = $('#kota').val();
                console.log("id provinsi setelah get prov >> ",id)
                $.getJSON('kota.json', 
                function(res){
                    html = '<option value="0">Pilih Kota / Kabupaten</option>';
                    var city = res.rajaongkir.results;
                    var i;
                
                    for(i=0; i<city.length; i++){
                        if(city[i].province_id == id) {
                            if (city[i].city_id == kota) {
                                html += '<option value="'+city[i].city_id+'" selected>'+city[i].type+' '+city[i].city_name+'</option>';
                            }
                            console.log("kota di filter >>> ", city[i])
                            html += '<option value="'+city[i].city_id+'">'+city[i].type+' '+city[i].city_name+'</option>';
                        }
                        
                    }
                    $('#paymentSelectRegencies').html(html);
                    
                });
        })

        // $("#paymentSelectProvinces").change(resetKurir);
        $("#paymentSelectRegencies").change(paymentSelectKurir);
        function setKurirLoadPage() {
            $("#paymentSelectKurir").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            $("#paymentTotalAll").text("Rp"+'<?= number_format($totalall,0,",","."); ?>');
            $("#paymentSendingPrice").text("Rp0");
            const destination = $("#kota").val();
           
            if(destination === ""){
                $("#paymentTextErrorAboveSelectKurir").show();
            }else{
                $("#paymentTextErrorAboveSelectKurir").hide();
                $.ajax({
                    url: "<?= base_url(); ?>payment/getService",
                    type: "post",
                    dataType: "json",
                    async: true,
                    data: {
                        destination: destination
                    },
                    success: function(data){
                        $("#paymentSelectKurir").select2({
                            placeholder: 'Pilih Salah Satu',
                            language: 'id'
                        })
                        $("#paymentSelectKurir").html(data);
                    }
                });
            }
        }
        function paymentSelectKurir(){
            $("#paymentSelectKurir").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            $("#paymentTotalAll").text("Rp"+'<?= number_format($totalall,0,",","."); ?>');
            $("#paymentSendingPrice").text("Rp0");
            const destination = $("#paymentSelectRegencies").val();
            if(destination === ""){
                $("#paymentTextErrorAboveSelectKurir").show();
            }else{
                $("#paymentTextErrorAboveSelectKurir").hide();
                $.ajax({
                    url: "<?= base_url(); ?>payment/getService",
                    type: "post",
                    dataType: "json",
                    async: true,
                    data: {
                        destination: destination
                    },
                    success: function(data){
                        $("#paymentSelectKurir").select2({
                            placeholder: 'Pilih Salah Satu',
                            language: 'id'
                        })
                        $("#paymentSelectKurir").html(data);
                    }
                });
            }
        }

        $("#paymentSelectKurir").change(paymentSelectService);

        function paymentSelectService(){
            let id = $("#paymentSelectKurir").val();
            id = id.split('-');
            id = id[0];
            if(id === ""){
                id = 0;
            }
            $("#paymentSendingPrice").text("Rp"+number_format(id).split(",").join("."));
            const price = $("#paymentPriceTotalAll").val();
            const total = parseInt(price) + parseInt(id);
            $("#paymentTotalAll").text("Rp"+number_format(total).split(",").join("."));

            $("#input_voucher").show();

        }
        function klaimVoucher() {
            var voucher = $('#voucher').val()
            if (voucher.length != 0) {
                $.ajax({
                    url: "<?= base_url(); ?>payment/apply_voucher",
                    type: "post",
                    dataType: "json",
                    async: true,
                    data: {
                        kode: voucher
                    },
                    success: function(res){
                        if (res.success) {
                            var type = parseInt(res.data.type);
                            var nominal = res.data.nominal;
                            var totalbelanja = $("#paymentPriceTotalAll").val();

                            let id = $("#paymentSelectKurir").val();
                            id = id.split('-');
                            id = id[0];
                            if(id === ""){
                                id = 0;
                            }

                            if (type == 1) {
                                var applytotal = totalbelanja - nominal;
                                const total = parseInt(applytotal) + parseInt(id);
                                $("#paymentTotalAll").text("Rp"+number_format(total).split(",").join("."));
                                $("#kode_voucher").val(res.data.kode);
                                $("#nominal_voucher").val(res.data.nominal);
                            } else {
                                if (id != 0) {
                                    var applyongkir = parseInt(id) - parseInt(nominal)
                                    $("#paymentSendingPrice").text("Rp"+number_format(applyongkir).split(",").join("."));
                                    const price = $("#paymentPriceTotalAll").val();
                                    const total = parseInt(price) + parseInt(applyongkir);
                                    $("#paymentTotalAll").text("Rp"+number_format(total).split(",").join("."));   
                                    $("#kode_voucher").val(res.data.kode);
                                    $("#nominal_voucher").val(res.data.nominal); 
                                }else{
                                    $('#voucher').val('');
                                    alert('Voucher ini hanya berlaku jika bukan free ongkir');
                                }
                            }
                        } else {
                            alert(res.msg);
                        }
                    }
                });
            } else {
                alert('Harap Masukan Voucher terlebih dahulu')
            }
        }
        function number_format (number, decimals, decPoint, thousandsSep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
            var n = !isFinite(+number) ? 0 : +number
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
            var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
            var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
            var s = ''

            var toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec)
            return '' + (Math.round(n * k) / k)
                .toFixed(prec)
            }

            // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
            if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
            }
            if ((s[1] || '').length < prec) {
            s[1] = s[1] || ''
            s[1] += new Array(prec - s[1].length + 1).join('0')
            }

            return s.join(dec)
        }

        function giveReview(inv) {
            $('#invoice').val(inv);
            $('#addReview').modal('show');
        }

        $(document).ready(function(){
  
        /* 1. Visualizing things on Hover - See next part for action on click */
        $('#stars li').on('mouseover', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
        
            // Now highlight all the stars that's not after the current hovered star
            $(this).parent().children('li.star').each(function(e){
            if (e < onStar) {
                $(this).addClass('hover');
            }
            else {
                $(this).removeClass('hover');
            }
            });
            
        }).on('mouseout', function(){
            $(this).parent().children('li.star').each(function(e){
            $(this).removeClass('hover');
            });
        });
        
        
        /* 2. Action to perform on click */
        $('#stars li').on('click', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently selected
            var stars = $(this).parent().children('li.star');
            
            for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
            }
            
            for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
            }
            
            // JUST RESPONSE (Not needed)
            var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
            var msg = "";
            var bintang = 0;
            if (ratingValue > 1) {
                bintang = ratingValue;
            }
            else {
                 bintang = ratingValue;
            }
            $('#bintang').val(bintang);
        });
        });


        function responseMessage(msg) {
            $('.success-box').fadeIn(200);  
            $('.success-box div.text-message').html("<span>" + msg + "</span>");
        }
        jQuery(document).ready(function ($) {
            $('#myTab').tab();
        });
    </script>
</html>
