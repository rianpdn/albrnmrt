</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Yakin ingin keluar</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Pilih "Keluar" di bawah ini jika Anda siap untuk mengakhiri sesi Anda saat ini.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="<?= base_url(); ?>administrator/logout">Keluar</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="<?= base_url(); ?>assets/vendor/datatables/jquery.dataTables.js"></script>
<!-- Core plugin JavaScript-->
<script src="<?= base_url(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url(); ?>assets/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="<?= base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>
<script src="<?= base_url(); ?>assets/select2-4.0.6-rc.1/dist/js/select2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>

$("#backpage").click(function (){
  window.history.go(-1);
});

var load_description = $('#description');
function loadEditor() {
  ClassicEditor.create( document.querySelector( '#description' ) )
  .then( editor => {
          console.log( editor );
  } )
  .catch( error => {
          console.error( error );
  } );
}
if (load_description.length) {
  loadEditor();
}


$("#pilihProduk").select2({
    placeholder: 'Pilih Produk',
    language: 'id'
})
$("#settingSelectRegency").select2({
    placeholder: 'Pilih Kabupaten/Kota',
    language: 'id'
})

$("#sendMailTo").select2({
    placeholder: 'Pilih Tujuan',
    language: 'id'
})

$("#cat").select2({
    placeholder: 'Pilih Kategori',
    language: 'id'
})

$("#pilih_produk").select2({
    placeholder: 'Pilih Produk',
    language: 'id'
})
$(document).ready( function () {
    $('#dataTable').DataTable(); 
    $('.prod').DataTable();
} );

var load_chart = $('#myChart');
function getChart() {
    var start_date = $('.date_start').val();
    var end_date = $('.date_end').val();
        $.ajax({
            url: '<?=base_url();?>Administrator/datatrafic',
            method: 'post',
            data:{
              start : start_date,
              end : end_date
            },
            dataType: 'json',
            success:function(dt){
                generateChart(dt);
            }
        })
    }
if (load_chart.length) { getChart(); }
var myChart = '';
    function generateChart(datas) {
        console.log("data >> ",datas)
        var day = [],dataChartVisit = [],dataChartOrder = [];
        for(var i in datas){
            // day.push(dayName);
            day.push(datas[i].date)
            dataChartVisit.push(datas[i].count);
            dataChartOrder.push(datas[i].count_order);
        }
        const data = {
        labels: day,
            datasets: [{
                label: 'Total Pengunjung',
                backgroundColor: 'rgb(255, 87, 54)',
                borderColor: 'rgb(255, 87, 54)',
                data: dataChartVisit,
            },
            {
                label: 'Total Pembeli',
                backgroundColor: 'rgb(21, 209, 230)',
                borderColor: 'rgb(21, 209, 230)',
                data: dataChartOrder,
            }]
        };
        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Grafik Pengunjung Albiruni'
                }
                }
            },
            };

            myChart = new Chart(
                document.getElementById('myChart'),
                config
            );
    }
    function loadtraf() {
      myChart.destroy();
      getChart()

    }
    function arsip_pesanan() {
      var start_date = $('.tgl_awal').val();
      var end_date = $('.tgl_akhir').val();
          $.ajax({
              url: '<?=base_url();?>Administrator/arsip_pesanan',
              method: 'post',
              data:{
                tgl_awal : start_date,
                tgl_akhir : end_date
              },
              dataType: 'json',
              success:function(dt){
                  swal({
                    text: 'Pesanan berhasil di Arsipkan',
                    icon: 'success'
                  });
                  setTimeout(() => {
                    location.reload();
                  }, 1000);
              }
          })
    }
        $("#reseller").select2({
            placeholder: 'Pilih Pembeli',
            language: 'id'
        })
        $("#paymentSelectProvinces").select2({
            placeholder: 'Pilih Provinsi',
            language: 'id'
        })

        $("#paymentSelectRegencies").select2({
            placeholder: 'Pilih Kabupaten/Kota',
            language: 'id'
        })

        $("#paymentSelectKurir").select2({
            placeholder: 'Pilih Salah Satu',
            language: 'id'
        })

        $("#paymentSelectService").select2({
            placeholder: 'Pilih Service',
            language: 'id'
        })

        $("#paymentSelectProvinces").change(function(){
            $("#paymentSelectRegencies").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            const id = $(this).val();
            $.ajax({
                url: "<?= base_url(); ?>payment/getLocation",
                type: "post",
                dataType: "json",
                async: true,
                data: {
                    id: id
                },
                success: function(data){
                    $("#paymentSelectRegencies").select2({
                        placeholder: 'Pilih Kabupaten/Kota',
                        language: 'id'
                    })
                    $("#paymentSelectRegencies").html(data);
                    $("#paymentTextErrorAboveSelectKurir").hide();
                }
            });
        })
        
        // $("#paymentSelectProvinces").change(paymentSelectKurir);

        $("#paymentSelectRegencies").change(paymentSelectKurir);
        function paymentSelectKurir(){
            $("#paymentSelectKurir").select2({
                placeholder: 'Loading..',
                language: 'id'
            })
            $("#paymentSendingPrice").text("Rp0");
            const destination = $("#paymentSelectRegencies").val();
            if(destination === ""){
                $("#paymentTextErrorAboveSelectKurir").show();
            }else{
                $("#paymentTextErrorAboveSelectKurir").hide();
                $.ajax({
                    url: "<?= base_url(); ?>payment/getService",
                    type: "post",
                    dataType: "json",
                    data: {
                        destination: destination
                    },
                    success: function(data){
                        $("#paymentSelectKurir").select2({
                            placeholder: 'Pilih Salah Satu',
                            language: 'id'
                        })
                        $("#paymentSelectKurir").html(data);
                    }
                });
            }
        }
        $("#biaya_ekspedisi").change(ekspedisi);
        $("#biaya_ekspedisi").keyup(ekspedisi);
        function ekspedisi() {
            var kur = $("#biaya_ekspedisi").val();
            $("#paymentSendingPrice").text("Rp"+number_format(kur).split(",").join("."));
            const price = $("#paymentPriceTotalAll").val();
            const total = parseInt(price) + parseInt(kur);
            $("#paymentTotalAll").text("Rp"+number_format(total).split(",").join("."));
        }
        $("#paymentSelectKurir").change(paymentSelectService);

        function paymentSelectService(){
            let id = $("#paymentSelectKurir").val();
            id = id.split('-');
            id = id[0];
            if(id === ""){
                id = 0;
            }
            $("#paymentSendingPrice").text("Rp"+number_format(id).split(",").join("."));
            const price = $("#paymentPriceTotalAll").val();
            const total = parseInt(price) + parseInt(id);
            $("#paymentTotalAll").text("Rp"+number_format(total).split(",").join("."));
        }
        
        function number_format (number, decimals, decPoint, thousandsSep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
            var n = !isFinite(+number) ? 0 : +number
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
            var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
            var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
            var s = ''

            var toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec)
            return '' + (Math.round(n * k) / k)
                .toFixed(prec)
            }

            // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
            if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
            }
            if ((s[1] || '').length < prec) {
            s[1] = s[1] || ''
            s[1] += new Array(prec - s[1].length + 1).join('0')
            }

            return s.join(dec)
        }
        $('.total_prt').each(function() {
            var sum = 0;
            $(this).parents('table').find('.precio').each(function() {
                var floted = parseFloat($(this).text());
                if (!isNaN(floted)) sum += floted;
            });

            $(this).html(sum);
        });
        function addProduk() {
            // getProductbyId
            var id = $('#pilih_produk').val();
            var jml = $('#jml').val();
            var diskon = $('#diskon').val();
            var hdiskon = 0;
            var total = 0;
            var totaldisk = 0;
            var hargasatuanafterdiskon = 0;
            var produk =[];
            $.ajax({
                    url: "<?= base_url(); ?>Administrator/getProductbyId/"+id,
                    type: "get",
                    dataType: "json",
                    async: true,
                    success: function(res){
                       console.log("response product >>",res);
                        var judul = res.data.title;
                        var slug = res.data.slugP;
                        var price = res.data.price;
                        var real_price = res.data.price;
                        var img = res.data.img;
                        var idp = res.data.id;
                        hdiskon = (price*jml)*(diskon/100);
                        total = price*jml;
                        totaldisk = total-hdiskon;
                        var diskonsatuan = price*(diskon/100);
                        hargasatuanafterdiskon = price - diskonsatuan;


                        if(localStorage.getItem('produk')){
                            produk = JSON.parse(localStorage.getItem('produk'));
                        }
                        produk.push({'productId' : idp, 'judul' : judul,'price' : price,'real_price' : real_price,'jml' : jml,'slug' : slug,'total' : totaldisk,'image' : img,'diskon' : diskon,'hargasatuandiskon' : hargasatuanafterdiskon});
                        localStorage.setItem('produk', JSON.stringify(produk));

                        setTimeout(() => {
                            loadProduk(); 
                        }, 500);
                        
                    }
                });  
           
        }
        function loadProduk() {
            var rows = "";
            var total = 0;
            var gProduk = localStorage.getItem('produk');
                        var response = $.parseJSON(gProduk);
                        $.each(response, function(i, item) {
                            var no = i+1;
                            rows += "<tr><td>"+no+"</td><td>" + item.judul + "</td><td>" + item.jml + "</td><td>Rp "+number_format(item.price).split(',').join('.')+"</td><td> " + item.diskon + " %</td><td> " + item.total + " </td><td><a href='javascript:;' onclick='return removeProduct(\""+item.productId+"\");'><i class='fa fa-trash'></i></a><input type='hidden' name='product_name[]' value='" + item.judul + "'><input type='hidden' name='price[]' value='" + item.hargasatuandiskon + "'><input type='hidden' name='qty[]' value='" + item.jml + "'><input type='hidden' name='slug[]' value='" + item.slug + "'><input type='hidden' name='img[]' value='" + item.image + "'><input type='hidden' name='real_price[]' value='" + item.price + "'></td></tr> ";
                            total += item.total;
                        });
                        $("#paymentPriceTotalAll").val(total);
                        $('#loop_produk').html(rows);
                        $('.total_belanja').text('Rp '+number_format(total).split(',').join('.'));
                        $('#paymentTotalAll').text('Rp '+number_format(total).split(',').join('.'));
            
        }
        function removeProduct(productId){
            var storageProducts = JSON.parse(localStorage.getItem('produk'));
            var products = storageProducts.filter(product => product.productId !== productId );
            localStorage.setItem('produk', JSON.stringify(products));
            setTimeout(() => {
                    loadProduk(); 
                }, 500);
        }
        var loadresellerpage = $('#reseller');
        if (loadresellerpage.length) {
            loadProduk();
        }

        function sendnotiwa(hp) {
            $.ajax({
                    url: "<?= base_url() ;?>administrator/sendNotifWa",
                    type: "get",
                    dataType: "json",
                    async: true,
                    data:{
                        nomor:hp.toString()
                    },
                    success: function(res){
                        swal({
                            text: 'Sukses Kirim Pengingat Bayar',
                            icon: 'success'
                        });
                    }
                });  
        
        }
        function getLapJual() {
            
            var start = $('.tgl_awal').val();
            var end = $('.tgl_akhir').val();
            if (start.length == 0) {
                swal({
                            text: 'Harap Pilih tanggal',
                            icon: 'error'
                        });
            } else {
                var viewUrl='<?=base_url();?>administrator/download_lap_jual?tgl_awal='+start+'&tgl_akhir='+end;
                window.open(viewUrl, "_blank"); 
            }
           
        }

        $("#pubpv").change(function(){
            var id = $(this).val();
            if (id != 1) {
                $('#grup_kode').hide();
                $('#grup_kriteria').show();
            } else {
                $('#grup_kode').show();
                $('#grup_kriteria').hide();
            }
           
        })
        var isprop = $('#prov');
        if (isprop.length) {
            var prop = $('#prov').val();
            $.getJSON('<?=base_url();?>prov.json', 
                function(res){
                    var prov = res.rajaongkir.results;
                    var i;
                    for(i=0; i<prov.length; i++){
                        if(prov[i].province_id == prop.replace(" ","")) {
                            console.log("kota di filter >>> ", prov[i].province)
                            $('.propinsi').text(prov[i].province);
                        }
                    }
            });
            var kot = $('#cit').val();
            $.getJSON('<?=base_url();?>kota.json', 
                function(res){
                    var city = res.rajaongkir.results;
                    var i;
                    for(i=0; i<city.length; i++){
                        if(city[i].city_id == kot.replace(" ","")) {
                            console.log("kota di filter >>> ", city[i].city_name)
                            $('.kota').text(city[i].city_name);
                        }
                        
                    }
            });
        }
        
        function filterUangDash() {
            $('#uangbrutodash').text(" ")
            $('#uangmasukdash').text(" ")
            var start = $('.date_start').val();
            var end = $('.date_end').val();
            $.ajax({
                    url: "<?= base_url() ;?>administrator/filterUangDash",
                    type: "post",
                    dataType: "json",
                    async: true,
                    data:{
                        start:start,
                        end:end
                    },
                    success: function(res){
                        // setTimeout(() => {
                            $('#uangbrutodash').text("Rp "+number_format(res.data.bruto).split(",").join("."))
                            $('#uangmasukdash').text("Rp "+number_format(res.data.uang_masuk).split(",").join("."))
                        // }, 500);
                        
                    }
                });  
        
        }
</script>

</body>

</html>
