
<div id="wrapper" class="clearfix">
   <!-- HEADER -->
   <div id="header" class="uni-head-1  ">
            <div class="container">
                <!-- BURGER - MAIN MENU -->
                <span id="burger-mainmenu" class="navbar-burger burger burger-mainmenu">
                    <span></span>
                <span></span>
                <span></span>
                </span>
                <div class="el_1">
                    <div class="holder">
                        <div id="logo" class="">
                           <a href="<?=base_url();?>">
                                <img class="header-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                                <img class="mobile-header-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                                <img class="sticky-logo" src="<?=base_url()?>vendor/img/logo.png" title="<?=$this->config->item('app_name'); ?>" alt="<?=$this->config->item('app_name'); ?>">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="el_2">
                    <div class="holder">
                        <!-- NAVIGATION BAR -->
                        <div id="navi-bar" class="navbar-menu">
                            <div class="navbar">
                                <a class="navbar-item" href="<?=base_url();?>"><span>Home</span></a>
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#"><span>Store</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                        <?php $categories = $this->Categories_model->getCategories(); ?>
                                        <?php foreach($categories->result_array() as $cat): ?>
                                            <a class="navbar-item" href="<?=base_url(); ?>c/<?= $cat['slug']; ?>"><span><?= $cat['name']; ?></span></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <a class="navbar-item" href="<?= base_url(); ?>about"><span>About Us</span></a>

                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="javascript:void(0)"><span>Customer Service</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                    <?php
                                        $page = $this->Settings_model->getFooterInfo();
                                         foreach($page->result_array() as $f): 
                                    ?>
                                        <a class="navbar-item" href="<?= base_url(); ?><?= $f['slug']; ?>"><span><?= $f['title']; ?></span></a>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                                 <?php if(!$this->session->userdata('login')){ ?>
                                    <a class="navbar-item" href="<?= base_url(); ?>login"><span>My Account</span></a>
                                 <?php }else{ ?>
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#"><span>My Account</span></a>
                                    <i class="accordion"></i>
                                    <div class="navbar-dropdown">
                                            <a class="navbar-item" href="<?=base_url(); ?>cart"><span>
                                            <?php if($cart->num_rows() > 0){ ?>
                                                Keranjang(<?= $cart->num_rows(); ?>)
                                              <?php }else{ ?>
                                                Keranjang
                                              <?php } ?></span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>profile/transaction"><span>Transaksi</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>profile/histories"><span>Riwayat Transaksi</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>payment/confirmation"><span>Konfirmasi Pembayaran</span></a>
                                            <a class="navbar-item" href="<?=base_url(); ?>logout"><span>Logout</span></a>
                                    </div>
                                </div>
                                 <?php } ?>
                                <?php if(!$this->session->userdata('login')){ ?>
                                <a class="navbar-item" href="<?= base_url(); ?>login"><span>Login/Register</span></a>
                                 <?php } ?>

                            </div>
                        </div>
                        <!-- END NAVIGATION BAR -->
                    </div>
                </div>

                <!-- SEARCH -->
                <!-- <div id="search-toggle" style="color: rgb(255 255 255) !important;"> -->
                    <!-- <div class="search-bar-container">
                        <i class="mdi mdi-magnify" onclick="search();"></i>
                        <div id="search-bar">
                            <input type="text" id="header_search" name="search" placeholder="Search" value="" oninput="$('#search').val($(this).val());">
                        </div>
                    </div>
                </div> -->

                </div>
                <!-- ACCOUNT -->
                <!-- <div id="myaccount">
                    <div class="dropdown is-right is-hoverable">
                        <div class="dropdown-trigger">
                            <a class="account-dropdown" aria-haspopup="true">
                                <i class="mdi mdi-account"></i>
                            </a>
                        </div>
                        <div class="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="login.html">Login</a>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>





        <div><a href="https://web.whatsapp.com/send?phone=085789921140&amp;text=Hi!%20Enquiring%20from%20www.albirunimart.com&amp;source&amp;data" target="_blank"><i class="icon-whatsapp float_whatsapp" ></i></a></div>


<section id="account-login" class="section container account-access">

<div id="contents">

    <div id="main-content">

        <!-- LOGIN -->
        <div class="holder">
            <div id="login">
                <div class="account-access-header">
                    <div class="title">Log in</div>
                    <div class="title-message">Belum punya akun? <a class="txt-interact txt-underline" href="<?= base_url(); ?>register">Daftar disini</a></div>
                </div>
                <?php echo $this->session->flashdata('verification'); ?>
                <!-- LOGIN FORM -->
                <form action="<?= base_url(); ?>login?redirect=<?=$redirect;?>" method="post" enctype="multipart/form-data">

                    <div class="form-body">
                        <!-- EMAIL -->
                        <div class="field">
                            <label class="label ">
                                Alamat Email								
                            </label>
                            <div class="control">
                                <input type="text" class="input" name="email" value="" />
                                <p class="help is-danger">* <?php echo form_error('email'); ?></p>
                            </div>
                        </div>

                        <!-- PASSWORD -->
                        <div class="field">
                            <label class="label">
                                Password								
                            </label>
                            <div class="field has-addons">
                                <div class="control addon-fix">
                                    <input type="password" class="input " name="password" value="" />
                                </div>
                                <div class="control">
                                    <a class="button btn-view-password">
                                        <span><i class="mdi mdi-eye-off"></i></span>
                                    </a>
                                </div>
                            </div>
                            <p class="help is-danger">* <?php echo form_error('password'); ?></p>
                        </div>
                    </div>

                    <div class="form-footer field">

                        <input type="submit" class="button btn-login" value="Log in" />

                        <!-- FORGOT PASSWORD -->
                        <div class="forget-password">
                            <a href="<?=  base_url(); ?>reset-password" class="txt-interact txt-underline">Forgot password?</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>


</div>

</section>
</div>
<script>

    $('.btn-view-password').on('click', function() {
        var type = $('input[name="password"]').attr('type');
        if (type == "password") {
            $('input[name="password"]').attr('type', 'text');
            $('.btn-view-password').html('<i class="mdi mdi-eye"></i>');
        } else {
            $('input[name="password"]').attr('type', 'password');
            $('.btn-view-password').html('<i class="mdi mdi-eye-off"></i>');
        }
    });
</script>