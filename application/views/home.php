                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-photo-grid-252 {
                                    background-color: #36b9cc;
                                background-image: linear-gradient(180deg,#36b9cc 10%,#258391 100%);
                                    /*background-image: url('<?=base_url();?>vendor/img/bg_transparent.jpg');*/
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                .module-photo-grid #photogrid-252 {
                                    padding-top: 10px;
                                    padding-bottom: 10px;
                                }

                                .desktop .home #photogrid-252 {
                                    max-width: 100%;
                                    margin: 0 auto;
                                }
                            </style>
                            <div id="module-photo-grid-252" class="module-photo-grid  ">
                                <div class="box-content">
                                    <div id="photogrid-252" class="photo-grid style-7">
                                    <?php
                                    $banner_bottom = $this->Settings_model->getBannerBot();
                                    foreach ($banner_bottom->result_array() as $key => $value) {
                                        $no = $key+1;
                                    ?>

                                        <div class="grid grid-<?=$no;?>">
                                            <a href="<?=$value['url'];?>">

                                                <div class="image">
<!-- KBxaVVXy1612626039-1080x1080.png -->
                                                    <img src="<?=base_url()?>assets/images/banner/<?=$value['img'];?>">

                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-collections-467 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_more.jpg');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-collections-467 .title {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-collections-467 .product-name a {
                                    color: rgba(250, 250, 250, 1);
                                }

                                #module-collections-467 .price .price-new {
                                    color: rgba(255, 220, 0, 1);
                                }
                            </style>
                            <div id="module-collections-467" class="box module-product module-collections imageswap">
                                <div class="title"><?= $selectCategoryTop['name']; ?><a class="fp-more collections-more txt-interact" href="<?=base_url();?>c/<?= $selectCategoryTop['slug']; ?>">More</a> </div>
                                <div class="carousel">
                                    <?php
                                    $i = 0;
                                    $ind_books = $this->Products_model->getProductByCategoryId($selectCategoryTop['id']);
                                    foreach ($ind_books->result_array() as $key => $p) {
                                        
                                    ?>
                                    <div>
                                        <div class="frame">
                                            <div class="item-img">
                                                <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                                                    <div class="image">
                                                        <!-- <div class="label-product">
                                                            <div class="cut_rotated"><span class="top_right small-db" style="background-image:url('<?=base_url();?>vendor/img/product_label/pre-order.png');background-repeat:no-repeat;background-position:center center;width:72px; height:71px;"></span></div>
                                                        </div> -->
                                                      <!--   <div class="label-howmanybought">
                                                            <span>31</span>
                                                        </div> -->
                                                        <img class="first-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img'] ?>"
                                                        alt="Nomor 2" />
                                                        <img class="sec-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img'] ?>"
                                                            alt="Nomor 2" />
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="item-info">
                                                <div class="product-name">
                                                    <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>" title="<?= $p['title']; ?>"><?= $p['title']; ?></a>
                                                </div>

                                                <div class="price">
                                                    <?php if($setting['promo'] == 1){ ?>
                                                    <?php if($p['promo_price'] == 0){ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php }else{ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                    <?php }else{ ?>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                </div>

                                                <div class="rating">

                                                </div>

                                                <div class="floating-cart-button">
                                                    <div class="btn-wishlist">
                                                        <a class="button"></a>
                                                    </div>

                                                    <div class="button">
                                                        <span class="icon-add-cart"></span>
                                                        <span class="btn-add-cart">
												<a class="button" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">Add to Cart</a>
											</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if (++$i == 2) break;
                                     } 
                                    ?>

                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-slideshow {
                                    background-color: ;
                                    padding-top: 0px !important;
                                    padding-bottom: 0px !important;
                                }

                                .desktop .home #module-slideshow .carousel {
                                    max-width: 100%;
                                    margin: 0 auto;
                                }
                            </style>
                            <div id="module-slideshow">
                                <div class="carousel">
                                <?php
                                    $slide_preview_buku = $this->Settings_model->getSlidePreviewBuku();
                                    foreach ($slide_preview_buku->result_array() as $value) {
                                       
                                    ?>
                                    <a href="<?=$value['url'];?>">
                                        <div>
                                            <img src="<?=base_url()?>assets/images/banner/<?=$value['img'];?>" alt="" title="" />
                                        </div>
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>

                            <script>
                                function slideshowSetting() {
                                    // SLIDESHOW MODULE
                                    $("#module-slideshow .carousel").not('.slick-initialized').slick({
                                        dots: true,
                                        infinite: true,
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        cssEase: 'linear'
                                    });
                                }

                                $(document).ready(function() {
                                    slideshowSetting();
                                });
                            </script>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-photo-grid-781 {
                                    background-color: ;
                                }

                                .module-photo-grid #photogrid-781 {
                                    padding-top: 10px;
                                    padding-bottom: 10px;
                                }

                                .desktop .home #photogrid-781 {
                                    max-width: 100%;
                                    margin: 0 auto;
                                }
                            </style>
                            <div id="module-photo-grid-781" class="module-photo-grid  ">
                                <div class="box-content">
                                    <div id="photogrid-781" class="photo-grid style-7">
                                        <?php
                                        $small_thumb_cover = $this->Settings_model->getSmallThumbnailCover();
                                        foreach ($small_thumb_cover->result_array() as $key => $value) {
                                            $no = $key+1;
                                        ?>

                                            <div class="grid grid-<?=$no;?>">
                                                <a href="<?=$value['url'];?>">

                                                    <div class="image">
    <!-- KBxaVVXy1612626039-1080x1080.png -->
                                                        <img src="<?=base_url()?>assets/images/banner/<?=$value['img'];?>">

                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-content-760 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                .desktop #module-content-760 .box-content {
                                    padding-top: 10px;
                                    padding-bottom: 10px;
                                }

                                .desktop #module-content-760 .main-title {
                                    font-size: 48px;
                                }

                                .desktop #module-content-760 .box-content {
                                    font-size: 16px;
                                }

                                #module-content-760 .main-title {
                                    text-align: center;
                                }

                                #module-content-760 .box-content {
                                    text-align: center !important;
                                }

                                #module-content-760 .content-info .title,
                                #module-content-760 .content-info .description {
                                    text-align: center;
                                }

                                #module-content-760 .main-title {
                                    color: rgba(255, 183, 0, 1);
                                }

                                #module-content-760 .box-content {
                                    color: rgba(255, 255, 255, 1);
                                }
                            </style>
                        <?php
                         if ($getHomeSettings['is_preorder_section'] == 1) {
                        ?>
                            <div id="module-content-760" class="box module-content style3">
                                <div class="box-content">
                                    <div class="text-content">

                                        <div class="main main_1">
                                            <div class="title main-title">
                                            <?=$getHomeSettings['title_preorder'];?> </div>

                                            <div class="description">
                                            <?=$getHomeSettings['caption_preorder'];?>
                                            </div>


                                            <div class="button button-content">
                                                <a href="<?=$getHomeSettings['url_prod_preorder'];?>">PRE ORDER NOW</a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="media-content">


                                        <img class="lazy" data-lazy="<?=$getHomeSettings['thumbnail_preorder'];?>" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-featured {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-featured .title {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-featured .product-name a {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-featured .price .price-new {
                                    color: rgba(255, 220, 0, 1);
                                }

                                #module-featured .frame {
                                    background-color: rgba(0, 0, 0, 0);
                                }
                            </style>
                            <div id="module-featured" class="box module-product imageswap">

                                <div class="title">
                                    New Arrival<a class="fp-more featured-more txt-interact" href="index3997.php?route=product/special&amp;sg_type_pg=featured">More</a> </div>

                                <div class="carousel">
                                    <?php if($recent->num_rows() > 0){ ?>
                                    <?php foreach($recent->result_array() as $p): ?>
                                    <div>
                                        <div class="frame">
                                            <div class="item-img">
                                                <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                                                    <div class="image">
                                                       <!--  <div class="label-product">
                                                            <div class="cut_rotated"><span class="top_right small-db" style="background-image:url('<?=base_url();?>vendor/img/product_label/best-seller.png');background-repeat:no-repeat;background-position:center center;width:72px; height:71px;"></span></div>
                                                        </div>

                                                        <div class="label-howmanybought">
                                                            <span>8905</span>
                                                        </div>
 -->

                                                        <img class="first-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                        <img class="sec-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                    </div>
                                                </a>
                                            </div>

                                            <div class="item-info">
                                                <div class="product-name">
                                                    <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>" title="<?= $p['title']; ?>"><?= $p['title']; ?></a>
                                                </div>
                                                <div class="price">
                                                    <?php if($setting['promo'] == 1){ ?>
                                                    <?php if($p['promo_price'] == 0){ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php }else{ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                    <?php }else{ ?>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                </div>
                                                <div class="rating">
                                                    <span class="rate-5"></span>
                                                </div>
                                                <div class="floating-cart-button">
                                                    <!-- <div class="btn-wishlist">
                                                        <a class="button" onclick="addToWishList('170');"></a>
                                                    </div> -->
                                                    <div class="button">
                                                        <span class="icon-add-cart"></span>
                                                        <span class="btn-add-cart">
											             <a class="button" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">Add to Cart</a>
										                  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-collections-796 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-collections-796 .title {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-collections-796 .product-name a {
                                    color: rgba(240, 240, 240, 1);
                                }

                                #module-collections-796 .price .price-new {
                                    color: rgba(255, 220, 0, 1);
                                }

                                #module-collections-796 .frame {
                                    background-color: rgba(255, 255, 255, 0);
                                }
                            </style>
                            <div id="module-collections-796" class="box module-product module-collections imageswap">

                                <div class="title">
                                <?= $selectCategoryTwo['name']; ?><a class="fp-more collections-more txt-interact" href="<?=base_url();?>c/<?= $selectCategoryTwo['slug']; ?>">More</a> </div>

                                <div class="carousel">
                                    <?php
                                    $i = 0;
                                    $ind_books = $this->Products_model->getProductByCategoryId($selectCategoryTwo['id']);
                                    foreach ($ind_books->result_array() as $key => $p) {
                                        
                                    ?>
                                    <div>
                                        <div class="frame">
                                            <div class="item-img">
                                                <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                                                    <div class="image">
                                                        <img class="first-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                        <img class="sec-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                    </div>
                                                </a>
                                            </div>

                                            <div class="item-info">

                                                <div class="product-name">
                                                    <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>" title="<?= $p['title']; ?> "><?= $p['title']; ?> </a>
                                                </div>

                                                <div class="price">
                                                    <?php if($setting['promo'] == 1){ ?>
                                                    <?php if($p['promo_price'] == 0){ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php }else{ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                    <?php }else{ ?>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                </div>

                                                <div class="rating">

                                                </div>

                                                <div class="floating-cart-button">
                                                    <div class="button">
                                                        <span class="icon-add-cart"></span>
                                                        <span class="btn-add-cart">
												        <a class="button" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">Add to Cart</a>
											            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                     } 
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-collections-433 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-collections-433 .title {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-collections-433 .product-name a {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-collections-433 .price .price-new {
                                    color: rgba(255, 220, 0, 1);
                                }

                                #module-collections-433 .frame {
                                    background-color: rgba(0, 0, 0, 0);
                                }
                            </style>
                            <div id="module-collections-433" class="box module-product module-collections imageswap">

                                <div class="title">
                                <?= $selectCategoryThree['name']; ?><a class="fp-more collections-more txt-interact" href="<?=base_url();?>c/<?= $selectCategoryThree['slug']; ?>">More</a> </div>

                                <div class="carousel">
                                    <?php
                                    $i = 0;
                                    $ind_books = $this->Products_model->getProductByCategoryId($selectCategoryThree['id']);
                                    foreach ($ind_books->result_array() as $key => $p) {
                                        
                                    ?>
                                    <div>
                                        <div class="frame">
                                            <div class="item-img">
                                                <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>">
                                                    <div class="image">
                                                        <img class="first-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                        <img class="sec-img lazy" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg" data-lazy="<?= base_url(); ?>assets/images/product/<?= $p['img']; ?>"
                                                            alt="<?= $p['title']; ?> " />

                                                    </div>
                                                </a>
                                            </div>

                                            <div class="item-info">

                                                <div class="product-name">
                                                    <a href="<?= base_url(); ?>p/<?= $p['slug']; ?>" title="<?= $p['title']; ?> "><?= $p['title']; ?> </a>
                                                </div>

                                                <div class="price">
                                                    <?php if($setting['promo'] == 1){ ?>
                                                    <?php if($p['promo_price'] == 0){ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php }else{ ?>
                                                        <div class="price-new">Rp <?= str_replace(",",".",number_format($p['promo_price'])); ?></div>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                    <?php }else{ ?>
                                                        <div class="price-old">Rp <?= str_replace(",",".",number_format($p['price'])); ?></div>
                                                    <?php } ?>
                                                </div>

                                                <div class="rating">

                                                </div>

                                                <div class="floating-cart-button">
                                                    <div class="button">
                                                        <span class="icon-add-cart"></span>
                                                        <span class="btn-add-cart">
												        <a class="button" href="<?= base_url(); ?>p/<?= $p['slug']; ?>">Add to Cart</a>
											            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                     } 
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-content-884 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top right;
                                }

                                .desktop #module-content-884 .box-content {
                                    padding-top: 10px;
                                    padding-bottom: 10px;
                                }

                                .desktop #module-content-884 .main-title {
                                    font-size: 48px;
                                }

                                .desktop #module-content-884 .box-content {
                                    font-size: 16px;
                                }

                                .desktop #module-content-884 .frame .content-info .title {
                                    font-size: 14px;
                                }

                                .desktop #module-content-884 .frame .content-info .description {
                                    font-size: 12px;
                                }

                                #module-content-884 .main-title {
                                    text-align: center;
                                }

                                #module-content-884 .box-content {
                                    text-align: center !important;
                                }

                                #module-content-884 .content-info .title,
                                #module-content-884 .content-info .description {
                                    text-align: center;
                                }

                                #module-content-884 .main-title {
                                    color: rgba(255, 255, 255, 1);
                                }

                                #module-content-884 .box-content {
                                    color: rgba(255, 255, 255, 1);
                                }
                            </style>

                            <div id="module-content-884" class="box module-content style8">
                                <div class="box-content">
                                    <div class="text-content">

                                        <div class="main main_1">
                                            <div class="title main-title">
                                                <?php
                                                    $getBotSec = $this->db->get('home_setting')->row()->bottom_title_section;
                                                    echo $getBotSec;
                                                ?>
                                               
                                            </div>

                                        </div>
                                    </div>
                                    <div class="media-content">
                                    <?php
                                        $getTypewriter = $this->Settings_model->getAllTypewriter();
                                        if ($getTypewriter->num_rows() != 0) {
                                            foreach ($getTypewriter->result_array() as $t) {
                                    ?>
                                        <div class="frame">
                                            <div class="content-media">
                                                <a><img class="lazy" data-lazy="<?= base_url(); ?>assets/images/typewriter_note/<?= $t['img']; ?>" src="<?=base_url();?>vendor/img/defaultlazyload-420x420.jpg">	</a>
                                            </div>
                                            <div class="content-info">
                                                <div class="title">
                                                </div>
                                                <div class="description">
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        }
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-photo-grid-267 {
                                    background-image: url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                .module-photo-grid #photogrid-267 {
                                    padding-top: 10px;
                                    padding-bottom: 10px;
                                }

                                .desktop .home #photogrid-267 {
                                    max-width: 100%;
                                    margin: 0 auto;
                                }
                            </style>
                            <div id="module-photo-grid-267" class="module-photo-grid  ">
                                <div class="box-content">
                                    <div id="photogrid-267" class="photo-grid style-7">
                                    <?php
                                    foreach ($testimoni->result_array() as $key => $value) {
                                        $no = $key+1;
                                    ?>

                                        <div class="grid grid-<?=$no;?>">
                                            <a>
                                                <div class="image">
                                                    <img src="<?= base_url(); ?>assets/images/testimonial/<?= $value['photo']; ?>">
                                                </div>
                                            </a>
                                        </div>
                                    <?php
                                        }
                                    ?>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="module-container">
                            <!-- ###AIO### -->
                            <style>
                                #module-total-review {
                                    background-image:url('<?=base_url();?>vendor/img/bg_blue_dark.png');
                                    background-repeat: repeat;
                                    background-position: top center;
                                }

                                #module-total-review .title {
                                    color: rgba(247, 44, 30, 1);
                                }

                                #module-total-review .box-content .review-frame .review-user .icon-user {
                                    color: rgba(8, 8, 8, 1);
                                }

                                #module-total-review .box-content .review-frame .review-product a {
                                    color: rgba(13, 13, 13, 1);
                                }

                                #module-total-review .box-content .review-frame .review-comment {
                                    color: rgba(26, 26, 26, 1);
                                }

                                #module-total-review .box-content .review-frame .review-date {
                                    color: rgba(224, 97, 97, 1);
                                }
                            </style>
                            <div class="box" id="module-total-review">

                                <div class="title">
                                    Customer Reviews 
                                </div>
                                <?php 
                                    foreach ($review->result_array() as $value) {
                                        $originalDate = $value['review_tgl'];
                                        $newDate = date("d F Y", strtotime($originalDate));
                                ?>
                                <div class="box-content">
                                    <div class="review-frame">
                                        <div class="review-user">
                                            <div class="icon-user">
                                               <?=$value['nama'];?> </div>
                                            <div class="rating">
                                                <span class="rate-<?=$value['review_star'];?> "></span>
                                            </div>
                                        </div>
                                        <div class="review-product">
                                            <img src="<?= base_url(); ?>assets/images/product/<?= $value['img_product']; ?>" /> <a href="<?= base_url(); ?>p/<?= $value['slug_product']; ?>" target="_blank">
								            <?= $value['nama']; ?></a>
                                        </div>
                                        <div class="review-comment">
                                            <div class="review-msg">
                                                <p><?= $value['review_content']; ?></p>
                                            </div>
                                            <div class="review-image">
                                            </div>
                                        </div>
                                        <div class="review-date">
                                           <?= $newDate;?> </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <script>
                                $.fancybox.defaults.buttons = [
                                    'close'
                                ];
                                $('[data-fancybox="preview0"]').fancybox({
                                    clickContent: false
                                });
                                $('[data-fancybox="preview3"]').fancybox({
                                    clickContent: false
                                });
                                $('[data-fancybox="preview4"]').fancybox({
                                    clickContent: false
                                });
                                $('[data-fancybox="preview7"]').fancybox({
                                    clickContent: false
                                });
                            </script>
                        </div>
                        <!-- ###AIO### -->
                    </div>
                </div>
            </div>
        </section>