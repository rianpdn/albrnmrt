<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testi_model extends CI_Model {

    public function getTesti(){
        $this->db->order_by('id', 'desc');
        return $this->db->get('testimonial');
    }
    public function uploadImgTesti(){
        $config['upload_path'] = './assets/images/testimonial/';
        $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
        $config['max_size'] = '2048';
        $config['file_name'] = round(microtime(true)*1000);

        $this->load->library('upload', $config);
        if($this->upload->do_upload('img')){
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
    
    public function insertTesti($upload){
        $name = $this->input->post('name');
        $content = $this->input->post('content');
        $data = [
            'name' => $name,
            'content' => $content,
            'photo' => $upload['file']['file_name']
        ];
        $this->db->insert('testimonial', $data);
    }

    public function getTestiById($id){
        return $this->db->get_where('testimonial', ['id' => $id])->row_array();
    }

    public function updateTesti($id){
        $name = $this->input->post('name');
        $content = $this->input->post('content');
        $data = [
            'name' => $name,
            'content' => $content
        ];
        $this->db->where('id', $id);
        $this->db->update('testimonial', $data);
    }

}