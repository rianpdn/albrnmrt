<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Visitor_model extends CI_Model {
 
  public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
  }
 
  // add
  public function tambah($visitor_data) {
    $this->db->insert('visitor', $visitor_data);
  }
 
  // Total visitor Website on Dashboard
  public function visit_dash($start,$end) {
    $now = date('Y');
    $nowday = date('Y-m-d');
    $this->db->select('FLOOR(UNIX_TIMESTAMP(time)/(240 * 60)) AS timekey, time,date, COUNT(*) AS total');
    $this->db->from('visitor');
    // $this->db->group_by('hour(time)');
    // $this->db->group_by('time');
    $this->db->group_by('timekey');
    $this->db->order_by('timekey', 'asc');
    if ($start == '') {
      // $this->db->where('SUBSTR(date,1,4)', $now);
      $this->db->where('date', $nowday);
    }else{
      $this->db->where('time BETWEEN "'. date('Y-m-d H:i', strtotime($start)). '" and "'. date('Y-m-d H:i', strtotime($end)).'"');
    }
    // $this->db->limit();
    $query = $this->db->get();
    return $query->result_array();
  }
 
  public function getCountoRderByDate($date)
  {
    //  $query = $this->db->query("SELECT COUNT(*) as total FROM `invoice` WHERE date(date_input) = ")
     $this->db->select('COUNT(*) AS total_order');
     $this->db->where('date(date_input)',$date);
     $query = $this->db->get('invoice');
     return $query->row()->total_order;

  }
 
}
 
 