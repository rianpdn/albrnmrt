<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_model extends CI_Model {
    
    public function tambah() {
        $judul = $this->input->post('judul', true);
        $kode = $this->input->post('kode', true);
        $nominal = $this->input->post('nominal', true);
        $type = $this->input->post('type', true);
        $expired = $this->input->post('expired', true);
        $pubpv = $this->input->post('pubpv', true);
        $kriteria = $this->input->post('kriteria', true);
        
        if ($pubpv=='1') {
            $data = array(
                'judul' => $judul,
                'kode' => $kode,
                'nominal' => $nominal,
                'type' => $type,
                'kriteria' => $pubpv,
                'expiredAt' => $expired,
                'createdAt' => date('Y-m-d H:i:s')
            );
        }else{
            $voucher ='';
            if ($kriteria=='1') {
                $voucher = 'DFTR' .  substr(rand(),0,1) . substr(time(),1);
            }else{
                $voucher = 'BLNJ' .  substr(rand(),0,1) . substr(time(),1);
            }
            $data = array(
                'judul' => $judul,
                'kode' =>  $voucher,
                'nominal' => $nominal,
                'type' => $type,
                'kriteria' => $pubpv,
                'dist' => $kriteria,
                'expiredAt' => $expired,
                'createdAt' => date('Y-m-d H:i:s')
            );
        }
        $insert = $this->db->insert('voucher', $data);
        return $insert;
    }
     
    public function getVoucher(){
        $this->db->order_by('id', 'desc');
        return $this->db->get('voucher');
      }
}