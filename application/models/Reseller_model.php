<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Reseller_model extends CI_Model {
 
    public function getReseller()
    {
        $this->db->where('user_type BETWEEN "2" and "3"');
        return $this->db->get('user');
    }
    public function getPembeli()
    {
        $this->db->where('user_type',1);
        return $this->db->get('user');
    }
    public function prosesOrder(){
        $getuser = $this->input->post('reseller', true);;
        $invoice = $getuser .  substr(rand(),0,5) . substr(time(),7);;
        $label = $this->input->post('label', true);
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $telp = $this->input->post('telp', true);
        $province = $this->input->post('paymentSelectProvinces', true);
        $regency = $this->input->post('paymentSelectRegencies', true);
        $district = $this->input->post('district', true);
        $village = $this->input->post('village', true);
        $zipcode = $this->input->post('zipcode', true);
        $address = $this->input->post('address', true);
        $courier = $this->input->post('paymentSelectKurir', true);

       

        $ekspedisi = $this->input->post('nama_ekspedisi', true);
        $biaya_ekspedisi = $this->input->post('biaya_ekspedisi', true);
        $service2 ='';
        $kurir='';
        $ongkir='';
        if ($ekspedisi != '') {
            $service2 =  $ekspedisi;

            $kurir =  $ekspedisi;
            $ongkir =  $biaya_ekspedisi;
        }else{
            $service1 = explode("-", $courier);
            $service2 = $service1[2];

            $kurir = $service1[1];
            $ongkir = $service1[0];
        }
        $dateInput = date('Y-m-d H:i:s');
        
        $judul = $this->input->post('product_name', true);


        foreach ($judul as $key => $value) {
            $price += (intval($this->input->post('price', true)[$key]) * intval($this->input->post('qty', true)[$key]));
            $realprice += (intval($this->input->post('real_price', true)[$key]) * intval($this->input->post('qty', true)[$key]));
        }
        
        
        $totalPrices = $price;

        $total_realprice = $realprice;
        $number = (string) $this->gen_urut_kodeunik();
        if (strlen($number) > 3) {
            $number = substr($number, strlen($number) - 3);
        }
        $number = (int) $number;
        $totalPrice = $totalPrices - $number;

        $totalAll = intval($ongkir) + intval($totalPrice);

        if($service2 == 'cod'){
            $numOne = 1;
        }else{
            $numOne = 0;
        }

        $data = [
            'user' => $getuser,
            'invoice_code' => $invoice,
            'label' => $label,
            'name' => $name,
            'email' => $email,
            'telp' => $telp,
            'province' => $province,
            'regency' => $regency,
            'district' => $district,
            'village' => $village,
            'zipcode' => $zipcode,
            'address' => $address,
            'courier' => $service2,
            'courier_service' => $kurir,
            'ongkir' => $ongkir,
            'total_price' => $totalPrice,
            'total_all' => $totalAll,
            'date_input' => $dateInput,
            'resi' => '0',
            'total_realprice' => $total_realprice
        ];
        $this->db->insert('invoice', $data);
        $id_invo = $this->db->insert_id();

        $getLastInvo = $this->db->where('id',$id_invo)
                                    ->get('invoice')->row_array();
                                    
        foreach ($judul as $key => $value) { 
            $data = [
                'id_invoice' =>  $getLastInvo['invoice_code'],
                'user' => $getuser,
                'product_name' => $this->input->post('product_name', true)[$key],
                'price' => $this->input->post('price', true)[$key],
                'qty' => $this->input->post('qty', true)[$key],
                'slug' => $this->input->post('slug', true)[$key],
                'ket' => $this->input->post('ket', true)[$key],
                'img' => $this->input->post('img', true)[$key],
                'real_price' => $this->input->post('real_price', true)[$key]
            ];
            $this->db->insert('transaction', $data);
        }
        
        // $produknya = array(); 
        // $getProduk = $this->db->where('id_invoice', $getLastInvo['invoice_code'])->get('transaction')->result();
        // foreach ($getProduk as $s) {
        //     $produknya[] = $s->product_name;
        // }
        // $textWa ="Halo ".$name.""."\xA"."
        // Terima Kasih telah melakukan pemesanan di Albiruni Mart, mohon untuk melakukan pembayaran."."\xA"."
        // Jika sudah melakukan pembayaran, silakan melakukan konfirmasi pembayaran ". base_url()."payment/confirmation serta bisa melalui Whatsapp ".$this->config->item('whatsapp')." dengan format sebagai berikut:"."\xA"."
        // 1. Kode Pesanan"."\xA"."
        // 2. Transfer Dari Bank"."\xA"."
        // 3. Transfer Ke Bank"."\xA"."
        // *Sertakan juga bukti transfer."."\xA"."
        // =============================================="."\xA"."
        // Produk : *".implode(",",$produknya)."*"."\xA"."
        // Kode Pesanan : *".$getLastInvo['invoice_code']."*"."\xA"."
        // Tanggal Pesanan : ".$dateInput.""."\xA"."
        // Total Keseluruhan yang harus ditransfer : *Rp".number_format($totalAll, 0, ',', '.')."*"."\xA"."
        // Pesanan akan dikirim setelah kami menerima pembayaran Anda."."\xA"."
        // Terima Kasih";
        // kirimWa($telp,$textWa);

        return ['invoice' =>$getLastInvo['invoice_code'], 'total' => $totalAll,'result'=>'success'];
    }
    public function gen_urut_kodeunik()   {

        // $nama = substr($namaLanding, 0, 2);
        $this->db->select('RIGHT(id,4) as kode', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('invoice');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
         //jika kode ternyata sudah ada.      
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
        }
        else {      
         //jika kode belum ada      
         $kode = 1;    
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        // $kodejadi = $nama.$idLanding."-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
        return $kodemax;  
    }
 
}
 
 