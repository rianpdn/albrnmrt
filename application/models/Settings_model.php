<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model {

    public function getAllTypewriter()
    {
      return $this->db->get('typewriter_note');
    }

    public function uploadImgTypewriter(){
      $config['upload_path'] = './assets/images/typewriter_note/';
      $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
      $config['max_size'] = '2048';
      $config['file_name'] = round(microtime(true)*1000);

      $this->load->library('upload', $config);
      if($this->upload->do_upload('img')){
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
      }else{
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
      }
    }
    public function insertTypewriter($upload){
      $data = [
          'title' => $this->input->post('title'),
          'img' => $upload['file']['file_name']
      ];
      $this->db->insert('typewriter_note', $data);
    }
    public function getSetting(){
        return $this->db->get('settings')->row_array();
    }
    public function getReviewCustomer(){
      $this->db->select("review_book.*, user.name AS nama, products.title AS title_product,products.img AS img_product,products.slug AS slug_product");
      $this->db->from("review_book");
      $this->db->join("user", "review_book.id_user=user.id");
      $this->db->join("products", "review_book.id_product=products.id");
      return $this->db->get();
    }

    public function getTesti(){
      return $this->db->get('testimonial');
    }
    public function getSmallThumbnailCover(){
      return $this->db->get('thumbnail_preview_section');
    }
    public function getSmallThumbnailCoverById($id){
      return $this->db->get_where('thumbnail_preview_section', ['id' => $id])->row_array();
    }
    public function uploadImgThumbnailPreview(){
      $config['upload_path'] = './assets/images/banner/';
      $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
      $config['max_size'] = '4096';
      $config['file_name'] = round(microtime(true)*1000);
  
      $this->load->library('upload', $config);
      if($this->upload->do_upload('img')){
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
      }else{
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
      }
    }
    
    public function editSmallThumbnailCover($id,$upload){
      $img = $upload['file']['file_name'];
      $url = $this->input->post('url', true);
      $info = getimagesize(base_url() . 'assets/images/banner/' . $img);
      // echo $info[0].'|'.$info[1];
      // exit;
      if($info[0] > 1000 || $info[1] > 800){
        unlink("./assets/images/banner/$img");
        return false;
      }else{
        if($url == ""){
          $FixUrl = "#";
        }else{
          $FixUrl = $url;
        }
        $data = [
          'img' => $img,
          'url' => $FixUrl
        ];
        $this->db->where('id', $id);
        $this->db->update('thumbnail_preview_section', $data);
        return true;
      }
    }
    public function getRekening(){
        return $this->db->get('rekening');
    }

    public function getRekeningById($id){
        return $this->db->get_where('rekening', ['id' => $id])->row_array();
    }

    public function getPages(){
        return $this->db->get('pages');
    }

    public function getSosmed(){
      return $this->db->get('sosmed');
    }

    public function getSosmedById($id){
      return $this->db->get_where('sosmed', ['id' => $id])->row_array();
    }

    public function getFooterInfo(){
      $this->db->select("distinct(page), pages.title AS title, pages.slug AS slug");
      $this->db->from("footer");
      $this->db->join("pages", "footer.page=pages.id");
      $this->db->where('footer.type', 1);
      return $this->db->get();
    }

    public function getCOD(){
      $this->db->order_by('id', 'desc');
      return $this->db->get('cod');
    }

    public function getFooterHelp(){
      $this->db->select("distinct(page), pages.title AS title, pages.slug AS slug");
      $this->db->from("footer");
      $this->db->join("pages", "footer.page=pages.id");
      $this->db->where('footer.type', 2);
      return $this->db->get();
    }

    public function getPageById($id){
      return $this->db->get_where('pages', ['id' => $id])->row_array();
    }
    public function getSlidePreview(){
      return $this->db->get('preview_section');
    }
    public function getBannerAll(){
      return $this->db->get('banner');
    }
    public function getBanner(){
      $this->db->where('position',1);
      return $this->db->get('banner');
    }
    public function getSlidePreviewBuku(){
      return $this->db->get('preview_section');
    }
    public function getBannerBot(){
      $this->db->where('position',2);
      $this->db->limit(3);
      return $this->db->get('banner');
    }
    public function getEmailAccount(){
      return $this->db->get('subscriber');
    }

    public function getCostDelivery(){
      $this->db->order_by('id', 'desc');
      return $this->db->get('cost_delivery');
    }

    public function getCostDeliveryById($id){
      return $this->db->get_where('cost_delivery', ['id' => $id])->row_array();
    }

    public function getRegency(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pro.rajaongkir.com/api/city",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function getCategory()
    {
      $q = $this->db->get('categories');
      return $q->result_array();
    }

    public function getCategoryById($id)
    {
      $this->db->where('id',$id);
      $q = $this->db->get('categories');
      return $q->row_array();
    }
    public function getProvinceById($id){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }
    public function getCityById($id){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }
    public function getRegencyById(){
      $getDB = $this->db->get('settings')->row_array();
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=".$getDB['regency_id'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
          "key: ". $this->config->item('api_rajaongkir')
      ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
          echo "cURL Error #:" . $err;
      } else {
          $response =  json_decode($response, true);
          return $response['rajaongkir']['results'];
      }
    }

    public function insertPage(){
        $title = $this->input->post('title');
        $content = $this->input->post('description');
        $slug = $this->input->post('slug');
        $data = [
            'title' => $title,
            'content' => $content,
            'slug' => $slug
        ];
        $this->db->insert('pages', $data);
    }

    public function updatePage($id){
        $title = $this->input->post('title');
        $content = $this->input->post('description');
        $slug = $this->input->post('slug');
        $data = [
            'title' => $title,
            'content' => $content,
            'slug' => $slug
        ];
        $this->db->where('id', $id);
        $this->db->update('pages', $data);
    }

    public function getPageBySlug($slug){
        return $this->db->get_where('pages', ['slug' => $slug])->row_array();
    }

    public function uploadImg(){
      $config['upload_path'] = './assets/images/banner/';
      $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
      $config['max_size'] = '2048';
      $config['file_name'] = round(microtime(true)*1000);

      $this->load->library('upload', $config);
      if($this->upload->do_upload('img')){
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
      }else{
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
      }
  }
    public function insertBanner($upload){
      $img = $upload['file']['file_name'];
      $url = $this->input->post('url', true);
      $posisi = $this->input->post('posisi', true);
      $info = getimagesize(base_url() . 'assets/images/banner/' . $img);
      if($info[0] != 1000 || $info[1] != 667){
        unlink("./assets/images/banner/$img");
        return false;
      }else{
        if($url == ""){
          $FixUrl = "#";
        }else{
          $FixUrl = $url;
        }
        $data = [
          'img' => $img,
          'url' => $FixUrl,
          'position' => $posisi
        ];
        $this->db->insert('banner', $data);
        return true;
      }
    }

    public function uploadImgSlidePreview(){
      $config['upload_path'] = './assets/images/banner/';
      $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
      $config['max_size'] = '4096';
      $config['file_name'] = round(microtime(true)*1000);
  
      $this->load->library('upload', $config);
      if($this->upload->do_upload('img')){
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
      }else{
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
      }
    }
    
    public function insertSlidePreview($upload){
      $img = $upload['file']['file_name'];
      $url = $this->input->post('url', true);
      $info = getimagesize(base_url() . 'assets/images/banner/' . $img);
      // echo $info[0].'|'.$info[1];
      // exit;
      if($info[0] > 2000 || $info[1] > 1600){
        unlink("./assets/images/banner/$img");
        return false;
      }else{
        if($url == ""){
          $FixUrl = "#";
        }else{
          $FixUrl = $url;
        }
        $data = [
          'img' => $img,
          'url' => $FixUrl
        ];
        $this->db->insert('preview_section', $data);
        return true;
      }
    }
    public function editDescription(){
        $desc = $this->input->post('desc', true);
        $this->db->set('short_desc', $desc);
        $this->db->update('settings');
    }

    public function addRekening(){
        $bank = $this->input->post('rekening', true);
        $name = $this->input->post('name', true);
        $number = $this->input->post('number', true);
        $data = [
            'rekening' => $bank,
            'name' => $name,
            'number' => $number
        ];
        $this->db->insert('rekening', $data);
    }

    public function editRekening($id){
        $bank = $this->input->post('rekening', true);
        $name = $this->input->post('name', true);
        $number = $this->input->post('number', true);
        $data = [
            'rekening' => $bank,
            'name' => $name,
            'number' => $number
        ];
        $this->db->where('id', $id);
        $this->db->update('rekening', $data);
    }

    public function addSosmed(){
      $name = $this->input->post('name', true);
      $icon = $this->input->post('icon', true);
      $link = $this->input->post('link', true);
      $data = [
          'name' => $name,
          'icon' => $icon,
          'link' => $link
      ];
      $this->db->insert('sosmed', $data);
    }

    public function editSosmed($id){
      $name = $this->input->post('name', true);
      $icon = $this->input->post('icon', true);
      $link = $this->input->post('link', true);
      $data = [
          'name' => $name,
          'icon' => $icon,
          'link' => $link
      ];
      $this->db->where('id', $id);
      $this->db->update('sosmed', $data);
    }

    public function editHome(){
      $section_category_first = $this->input->post('category_first', true);
      $is_preorder_section = $this->input->post('is_preorder', true);
      $title_preorder = $this->input->post('title_preorder', true);
      $caption_preorder = $this->input->post('caption_preorder', true);
      $url_prod_preorder = $this->input->post('url_prod_preorder', true);
      $thumbnail_preorder = $this->input->post('thumbnail_preorder', true);
      $section_category_two = $this->input->post('section_category_two', true);
      $section_category_three = $this->input->post('section_category_three', true);
      $bottom_title_section = $this->input->post('bottom_title_section', true);
      $data = [
          'section_category_first' => $section_category_first,
          'is_preorder_section' => $is_preorder_section,
          'title_preorder' => $title_preorder,
          'caption_preorder' => $caption_preorder,
          'url_prod_preorder' => $url_prod_preorder,
          'thumbnail_preorder' => $thumbnail_preorder,
          'section_category_two' => $section_category_two,
          'section_category_three' => $section_category_three,
          'bottom_title_section' => $bottom_title_section
      ];
      $this->db->update('home_setting', $data);
    }

    public function editAddress(){
      $address = $this->input->post('address', true);
      $regency = $this->input->post('settingSelectRegency', true);
      $data = [
          'address' => $address,
          'regency_id' => $regency
      ];
      $this->db->update('settings', $data);
    }

    public function addDelivery(){
      $destination = $this->input->post('destination', true);
      $price = $this->input->post('price', true);
      $data = [
          'destination' => $destination,
          'price' => $price
      ];
      $this->db->insert('cost_delivery', $data);
    }

    public function addCOD(){
      $destination = $this->input->post('destination', true);
      $data = [
          'regency_id' => $destination
      ];
      $this->db->insert('cod', $data);
    }

    public function editDelivery($id){
      $destination = $this->input->post('destination', true);
      $price = $this->input->post('price', true);
      $data = [
          'destination' => $destination,
          'price' => $price
      ];
      $this->db->where('id', $id);
      $this->db->update('cost_delivery', $data);
    }

    public function addFooter(){
      $page = $this->input->post('page');
      $type = $this->input->post('type');
      $data = [
        'page' => intval($page),
        'type' => intval($type)
      ];
      $this->db->insert('footer', $data);
    }

}
