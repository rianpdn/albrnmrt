<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    public function getCity($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/city?province=".$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: ". $this->config->item('api_rajaongkir')
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response =  json_decode($response, true);
            // echo json_encode($response);
            // exit;
            return $response['rajaongkir']['results'];
        }
    }

    public function getProvinces(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/province",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: ". $this->config->item('api_rajaongkir')
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response =  json_decode($response, true);
            return $response['rajaongkir']['results'];
        }
    }

    public function getService($kurir){
        $dbSetting = $this->db->get('settings')->row_array();
        $origin = $dbSetting['regency_id'];
        $destination = $this->input->post('destination');

        $getcart = $this->db->get_where('cart', ['user' => $this->session->userdata('id')]);
        $weight = 0;
        foreach ($getcart->result_array() as $key) {
            $weight += (intval($key['weight']) * intval($key['qty']));
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "originType=city&origin=".$origin."&destination=".$destination."&weight=".$weight."&courier=".$kurir."&destinationType=city",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: ". $this->config->item('api_rajaongkir')
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            // $response = json_decode($response, true);
            // return $response;
            return "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            // echo json_encode($response);
            // exit;
            return $response['rajaongkir']['results'][0]['costs'];
        }
    }
    public function gen_urut_kodeunik()   {

        // $nama = substr($namaLanding, 0, 2);
        $this->db->select('RIGHT(id,4) as kode', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('invoice');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
         //jika kode ternyata sudah ada.      
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
        }
        else {      
         //jika kode belum ada      
         $kode = 1;    
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        // $kodejadi = $nama.$idLanding."-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
        return $kodemax;  
    }
    public function succesfully(){
        $getuser = $this->db->get_where('user', ['id' => $this->session->userdata('id')])->row_array();
        $invoice = $getuser['id'] .  substr(rand(),0,5) . substr(time(),7);
        $label = $this->input->post('label', true);
        $name = $this->input->post('name', true);
        $email = $getuser['email'];
        $telp = $this->input->post('telp', true);
        $province = $this->input->post('paymentSelectProvinces', true);
        $regency = $this->input->post('paymentSelectRegencies', true);
        $district = $this->input->post('district', true);
        $village = $this->input->post('village', true);
        $zipcode = $this->input->post('zipcode', true);
        $address = $this->input->post('address', true);
        $courier = $this->input->post('paymentSelectKurir', true);
        $service1 = explode("-", $courier);
        $service2 = $service1[2];

        $voucher_code = $this->input->post('kode_voucher', true);
        $voucher_nominal = $this->input->post('nominal_voucher', true);

      
       

        $kurir = $service1[1];
        $dateInput = date('Y-m-d H:i:s');
        
        $getcart = $this->db->get_where('cart', ['user' => $this->session->userdata('id')]);

        if ($getcart->num_rows() != 0) {
            foreach ($getcart->result_array() as $key) {
                $price += (intval($key['price']) * intval($key['qty']));
                $realprice += (intval($key['real_price']) * intval($key['qty']));
            }
            
            
            
            $qgetVoucher = $this->db->get_where('voucher', ['kode' => $voucher_code]);
            $totalPrices = 0;
            $ongkir = 0;
            if ($qgetVoucher->num_rows() != 0) {
                $v = $qgetVoucher->row_array();
                
                if ($v['type'] == 1) {
                    $totalPricesss = $price - $v['nominal'];
                    $totalPrices = $totalPricesss;
                    $ongkir = $service1[0];
                }
                if ($v['type'] == 2) {
                    $ongkirs = $service1[0] - $v['nominal'];
                    $ongkir = $ongkirs;
                    $totalPrices = $price;
                }
                
            }else{
                $ongkir = $service1[0];
                $totalPrices = $price;
            }
            // echo $ongkir;
            // exit;
    
            $total_realprice = $realprice;
            // $kodeunik = $this->gen_urut_kodeunik();
    
            $number = (string) $this->gen_urut_kodeunik();
            if (strlen($number) > 3) {
                $number = substr($number, strlen($number) - 3);
            }
            $number = (int) $number;

            $cekpg = $this->config->item('status_payment_gateway');
            $totalPrice = 0;
            if ($cekpg != 'AKTIF') {
                $totalPrice = $totalPrices - $number;
            }else{
                $totalPrice = $totalPrices;
            }
           
    
            // $totalAll = 0;
    
            // $voucer = strlen((string)$voucher_nominal);
            // if ($voucer != 0) {
            $totalAll = intval($ongkir) + intval($totalPrice);
            // }else{
                // $totalAll = intval($ongkir) + intval($totalPrice) - intVal($voucher_nominal);
            // }
    
            if($service2 == 'cod'){
                $numOne = 1;
            }else{
                $numOne = 0;
            }
    
            $data = [
                'user' => $this->session->userdata('id'),
                'invoice_code' => $invoice,
                'label' => $label,
                'name' => $name,
                'email' => $email,
                'telp' => $telp,
                'province' => $province,
                'regency' => $regency,
                'district' => $district,
                'village' => $village,
                'zipcode' => $zipcode,
                'address' => $address,
                'courier' => $service2,
                'courier_service' => $kurir,
                'ongkir' => $ongkir,
                'total_price' => $totalPrice,
                'total_all' => $totalAll,
                'date_input' => $dateInput,
                'resi' => '0',
                'total_realprice' => $total_realprice,
                'voucher_code' => $voucher_code,
                'voucher_nominal' => $voucher_nominal,
            ];
            $this->db->insert('invoice', $data);
            $id_invo = $this->db->insert_id();
    
            $getLastInvo = $this->db->where('id',$id_invo)
                                        ->get('invoice')->row_array();
            // echo "asdasd".$getLastInvo['invoice_code'];
            // exit;
            foreach($getcart->result_array() as $c){
                $data = [
                    'id_invoice' =>  $getLastInvo['invoice_code'],
                    'user' => $this->session->userdata('id'),
                    'product_name' => $c['product_name'],
                    'price' => $c['price'],
                    'qty' => $c['qty'],
                    'slug' => $c['slug'],
                    'ket' => $c['ket'],
                    'img' => $c['img'],
                    'real_price' => $c['real_price']
                ];
                $this->db->insert('transaction', $data);
            }
    
            return ['invoice' =>$getLastInvo['invoice_code'], 'total' => $totalAll,'success' => true,'date' => $dateInput,'name' => $name,'totalall' => $totalAll,'email' => $email,'courier' => $service2,'telp' => $telp];
        }else{
            return ['success' => false];
        }
        
        // $this->db->where('user', $this->session->userdata('id'));
        // $this->db->delete('cart');
        
        // $this->load->library('email');

        // $config['charset'] = 'utf-8';
        // $config['useragent'] = $this->config->item('app_name');
        // $config['smtp_crypto'] = $this->config->item('smtp_crypto');
        // $config['protocol'] = 'smtp';
        // $config['mailtype'] = 'html';
        // $config['smtp_host'] = $this->config->item('host_mail');
        // $config['smtp_port'] = $this->config->item('port_mail');
        // $config['smtp_timeout'] = '5';
        // $config['smtp_user'] = $this->config->item('mail_account');
        // $config['smtp_pass'] = $this->config->item('pass_mail');
        // $config['crlf'] = "\r\n";
        // $config['newline'] = "\r\n";
        // $config['wordwrap'] = TRUE;

        // $table = '';
        // foreach ($getcart->result_array() as $c) {
        //     $table .= '<tr><td>'.$c['product_name'].'</td><td style="text-align: center">'.$c['qty'].'</td><td>Rp'.number_format($c['price'], 0, ',', '.').'</td><td>Rp'.number_format($c['price'] * $c['qty'], 0, ',', '.').'</td></tr>';
        // };

        // $rek = $this->db->get('rekening');
        // $rekening = '';
        // foreach($rek->result_array() as $r){
        //     $rekening .= '<p><strong>'.$r['rekening'].'</strong><br/>
        //     Atas Nama : '.$r['name'].'<br/>
        //     No Rekening : '.$r['number'].'</p>';
        // }


        
        // $this->email->initialize($config);
        // $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
        // $this->email->to($email);
        // $this->email->subject('Konfirmasi Pesanan '.$getLastInvo['invoice_code']);
        // $this->email->message(
        //     '<p><strong>Halo '.$name.'</strong><br>
        //     Terima Kasih telah melakukan pemesanan di Albiruni Mart, mohon untuk melakukan pembayaran.<br/>
        //     Jika sudah melakukan pembayaran, silakan melakukan konfirmasi pembayaran <a href="'.base_url().'payment/confirmation">dengan klik disini</a> serta bisa melalui Whatsapp '.$this->config->item('whatsapp').' atau <a href="https://wa.me/'.$this->config->item('whatsappv2').'">klik disini</a> dengan format sebagai berikut:</p>
        //     <table>
        //         <tr>
        //             <td>1. Kode Pesanan</td>
        //         </tr>
        //         <tr>
        //             <td>2. Transfer Dari Bank</td>
        //         </tr>
        //         <tr>
        //             <td>3. Transfer Ke Bank</td>
        //         </tr>
        //         <tr>
        //             <td>*Sertakan juga bukti transfer</td>
        //         </tr>
        //     </table>
        //     <br/>
        //     <table>
        //         <tr>
        //             <td>Kode Pesanan</td>
        //             <td><strong>'.$getLastInvo['invoice_code'].'</strong></td>
        //         </tr>
        //         <tr>
        //             <td style="padding-right:20px;">Tanggal Pesanan</td>
        //             <td>'.$dateInput.'</td>
        //         </tr>
        //     </table>
        //     <br/>
        //     <table border="1" style="border-collapse: collapse;">
        //         <tr>
        //             <th>Produk</th>
        //             <th>Jumlah</th>
        //             <th>Harga</th>
        //             <th>Total</th>
        //         </tr>
        //     '.$table.'
        //     </table>
        //     <br/>
        //     <table>
        //         <tr>
        //             <td>Total Harga</td>
        //             <td>Rp'.number_format($totalPrice, 0, ',', '.').'</td>
        //         </tr>
        //         <tr>
        //             <td>Biaya Pengiriman</td>
        //             <td>Rp'.number_format($ongkir, 0, ',', '.').'</td>
        //         </tr>
        //         <tr>
        //             <td style="padding-right:20px;"><strong>Total Keseluruhan</strong></td>
        //             <td><strong>Rp'.number_format($totalAll, 0, ',', '.').'</strong></td>
        //         </tr>
        //     </table>
        //     <p>Silakan pilih metode pembayaran yang tersedia dibawah ini:</p>
        //     '.$rekening.'
        //     <br/>
        //     </p>Pesanan akan dikirim setelah kami menerima pembayaran Anda. <br/>
        //     Terima Kasih</p>
        //     ');
        // $this->email->send();

        // $config['charset'] = 'utf-8';
        // $config['useragent'] = $this->config->item('app_name');
        // $config['smtp_crypto'] = $this->config->item('smtp_crypto');
        // $config['protocol'] = 'smtp';
        // $config['mailtype'] = 'html';
        // $config['smtp_host'] = $this->config->item('host_mail');
        // $config['smtp_port'] = $this->config->item('port_mail');
        // $config['smtp_timeout'] = '5';
        // $config['smtp_user'] = $this->config->item('mail_account');
        // $config['smtp_pass'] = $this->config->item('pass_mail');
        // $config['crlf'] = "\r\n";
        // $config['newline'] = "\r\n";
        // $config['wordwrap'] = TRUE;

        // if($service2 == "cod"){
        //     $metpeng = "COD/Cash of Delivery";
        // }else{
        //     $metpeng = "pengiriman ". strtoupper($service2);
        // }

        // $this->email->initialize($config);
        // $this->email->from($this->config->item('mail_account'), $this->config->item('app_name'));
        // $this->email->to($this->config->item('email_contact'));
        // $this->email->subject('Pesanan Masuk '.$getLastInvo['invoice_code']);
        // $this->email->message(
        //     '<p>Halo admin, <br/>Telah masuk pesanan dengan ORDER ID '.$invoice.' menggunakan metode '.$metpeng.', total belanjaan Rp'. number_format($totalAll,0,",",".").'. Silakan cek pesanan dengan <a href="'.base_url().'administrator/order/'.$getLastInvo['invoice_code'].'" target="_blank">klik disini</a> </p>
        //     ');
        // $this->email->send();

        // $produknya = array(); 
        // $getProduk = $this->db->where('id_invoice', $getLastInvo['invoice_code'])->get('transaction')->result();
        // foreach ($getProduk as $s) {
        //     $produknya[] = $s->product_name;
        // }
        // $textWa ="Halo ".$name.""."\xA"."
        // Terima Kasih telah melakukan pemesanan di Albiruni Mart, mohon untuk melakukan pembayaran."."\xA"."
        // Jika sudah melakukan pembayaran, silakan melakukan konfirmasi pembayaran ". base_url()."payment/confirmation serta bisa melalui Whatsapp ".$this->config->item('whatsapp')." dengan format sebagai berikut:"."\xA"."
        // 1. Kode Pesanan"."\xA"."
        // 2. Transfer Dari Bank"."\xA"."
        // 3. Transfer Ke Bank"."\xA"."
        // *Sertakan juga bukti transfer."."\xA"."
        // =============================================="."\xA"."
        // Produk : *".implode(",",$produknya)."*"."\xA"."
        // Kode Pesanan : *".$getLastInvo['invoice_code']."*"."\xA"."
        // Tanggal Pesanan : ".$dateInput.""."\xA"."
        // Total Keseluruhan yang harus ditransfer : *Rp".number_format($totalAll, 0, ',', '.')."*"."\xA"."
        // Pesanan akan dikirim setelah kami menerima pembayaran Anda."."\xA"."
        // Terima Kasih";
        // kirimWa($telp,$textWa);

       
      
       
        // return ['invoice' =>$getLastInvo['invoice_code'], 'total' => $totalAll,'success' => true];
    }

}
