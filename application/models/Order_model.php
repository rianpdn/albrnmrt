<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function getLapJualan()
    {
        $qGetTrx = $this->db->query('SELECT p.kode_buku,p.title,p.price AS harga,SUM(t.qty) as QTY,p.price * SUM(t.qty) AS bruto FROM `transaction` t JOIN products p on t.slug=p.slug GROUP BY t.slug');
        return $qGetTrx;
    } 
    public function getLapJualanPeriodic($start,$end)
    {
        $qGetTrx = $this->db->query('SELECT p.kode_buku,p.title,p.price AS harga,SUM(t.qty) as QTY,p.price * SUM(t.qty) AS bruto FROM `transaction` t JOIN products p on t.slug=p.slug JOIN invoice i ON t.id_invoice=i.invoice_code WHERE date(date_input) BETWEEN "'.$start.'" AND "'.$end.'" GROUP BY t.slug');
        return $qGetTrx;
    }
    public function getAllLapJualanPeriodic($start,$end)
    {
        $qGetTrx = $this->db->query("SELECT i.name as namapembeli,i.district as daerah,p.kode_buku as kodebuku,p.title as judulbuku,p.price as hargabuku,t.qty as jumlahbuku,p.price * t.qty AS bruto,concat(round(100-( p.promo_price/p.price * 100 ),0),'') AS diskon,i.ongkir as ongkir, date_submit FROM `transaction` t JOIN invoice i ON t.id_invoice=i.invoice_code JOIN products p on t.slug=p.slug WHERE date(date_input) BETWEEN '".$start."' AND '".$end."' ");
        return $qGetTrx;
    }
    
    public function getCartUser(){
        $id = $this->session->userdata('id');
        return $this->db->get_where('cart', ['user' => $id]);
    }

    public function getOrders($number,$offset){
        $this->db->join("user", "invoice.user=user.id");
        $this->db->order_by('id', 'desc');
        return $this->db->get('invoice',$number,$offset);
    }
    public function getOrdersByStatus($status){
        // $this->db->join("user", "invoice.user=user.id");
        $this->db->where('status', $status);
        $this->db->order_by('id', 'desc');
        return $this->db->get('invoice');
    }
    public function getOrderByInvoice($id){
        return $this->db->get_where('transaction', ['id_invoice' => $id]);
    }
    public function getDataInvoiceId($id){
        return $this->db->get_where('invoice', ['invoice_code' => $id]);
    }

    public function getDataInvoice($id){
        return $this->db->get_where('invoice', ['invoice_code' => $id])->row_array();
    }

    public function uploadFile(){
        $config['upload_path'] = './assets/images/confirmation/';
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size'] = '2048';
        $config['file_name'] = round(microtime(true)*1000);

        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')){
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function insertPaymentConfirmation($upload){
        $invoice = $this->input->post('invoice', true);
        $file = $upload['file']['file_name'];
        $data = [
            'invoice' => $invoice,
            'file' => $file
        ];
        $this->db->insert('payment_proof', $data);
    }

}